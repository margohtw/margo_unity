using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class UserProject
{

    // These fields have to be public to use the UnityEngine.JsonUtility interface
    public string title;
    public string projectFilename;
    public string savedAt;
    public Image thumbnail;
    public List<WEAModel> placedWeaModels;

    public override string ToString()
    { 
        return title + ", " + projectFilename + ", " + savedAt + ", " + placedWeaModels.Count + " platzierte WEAs";
    }
}
