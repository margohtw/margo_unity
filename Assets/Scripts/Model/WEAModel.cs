using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WEAModel
{
    private static float INITIAL_HUBHEIGHT = 143;
    private static float INITIAL_ROTORDIAM = 134;

    // These fields have to be public to use the UnityEngine.JsonUtility interface
    public string id;
    public string modelURL;
    public string textureURL;
    public Vector2d position;
    public float elevation;
    public float scaleFactor = 1;
    public int rotationDegrees = 0;

}
