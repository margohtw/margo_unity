using System;
using System.Collections;
using System.Collections.Generic;
using Mapbox.Utils;
using UnityEngine;

public class UserLocationProvider : MonoBehaviour
{
    private static UserLocationProvider instance;
    
    [SerializeField] private InitialLoadingScreenController loadingScreenController;
    
    [SerializeField] private bool debugLatLngOverride;
    
    [SerializeField] private float overrideLat;
    [SerializeField] private float overrideLng;

    private float currentLatitude;
    private float currentLongitude;

    private bool isUpdating;
    private bool initialUpdateComplete;
    private bool stopLocationUpdates;

    int updateAttempts;

    private void Start()
    {
        if (instance == null) instance = this;
        if (instance != null == instance != this) this.Destroy();
    }

    public static UserLocationProvider GetInstance()
    {
        return instance;
    }

    private void Update()
    {
        if (!stopLocationUpdates)
        {
            if (!isUpdating)
            {
                StartCoroutine(GetLocation());
                isUpdating = true;
            }
        }
    }

    IEnumerator GetLocation()
    {
        Debug.LogFormat("Start location coroutine ...");

        // Ask for required permissions
        if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.FineLocation))
        {
            UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.FineLocation);
            UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.CoarseLocation);
        }

        // Check if user accepted / location service is enabled
        if (!Input.location.isEnabledByUser)
            yield return new WaitForSeconds(3);

        // Start service happens here
        Input.location.Start();

        // Wait service initialization
        int maxWait = 15;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service did not initialize in 20 seconds
        if (maxWait < 1)
        {
            // TODO Failure
            Debug.LogFormat("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.LogFormat("Unable to determine device location");
            yield break;
        }
        else
        {
            // Location service successful
            Debug.LogFormat("Location service live. status {0}", Input.location.status);
            // Access granted and location value could be retrieved
            Debug.LogFormat("Location: "
                + Input.location.lastData.latitude + " "
                + Input.location.lastData.longitude + " "
                + Input.location.lastData.altitude + " "
                + Input.location.lastData.horizontalAccuracy + " "
                + Input.location.lastData.timestamp);

            // Set it here so it can be accessed by using GetLatitude and GetLongitude
            currentLatitude = Input.location.lastData.latitude;
            currentLongitude = Input.location.lastData.longitude;

            updateAttempts++;

            //as long as the initial update isnt complete, the initial loading screen still needs to get a status update
            if (!initialUpdateComplete)
            {
                //if we're manually overriding just use those values
                if (debugLatLngOverride)
                {
                    loadingScreenController.UpdateInfoText(true, false, updateAttempts);
                    initialUpdateComplete = true;
                }
                //the current location is occasionally determined to be 0, 0
                if (currentLatitude == 0 && currentLongitude == 0)
                {
                    loadingScreenController.UpdateInfoText(false, false, updateAttempts);
                    if (updateAttempts == 3)
                    {
                        initialUpdateComplete = true;
                    }
                }
                //success! a nonzero lat/lng value pair was determined
                if (currentLatitude != 0 && currentLongitude != 0)
                {
                    loadingScreenController.UpdateInfoText(false, true, updateAttempts);
                    initialUpdateComplete = true;
                }
            }
        }

        stopLocationUpdates = true;
        isUpdating = false;
        Input.location.Stop();
    }

    public float GetCurrentLatitude()
    {
        if (debugLatLngOverride) return overrideLat;
        return currentLatitude;
    }

    public float GetCurrentLongitude()
    {
        if (debugLatLngOverride) return overrideLng;
        return currentLongitude;
    }

    public bool IsInitialUpdateComplete()
    {
        return initialUpdateComplete;
    }

    public void StopLocationUpdates()
    {
        stopLocationUpdates = true;
    }

    public void ManualUserLocationUpdate(Vector2d newLatLng)
    {
        if (!isUpdating)
        {
            if (debugLatLngOverride)
            {
                overrideLat = (float)newLatLng[0];
                overrideLng = (float)newLatLng[1];
            }
            else
            {
                currentLatitude = (float)newLatLng[0];
                currentLongitude = (float)newLatLng[1];
            }
        }
    }
}
