using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ChangeToLandscape : MonoBehaviour
{
    //Variables
    //Public

    [Header("Manually switch landscape Mode: (bool can be used as button)")]
    
    public bool landscapeDebug = false;

    [Header("Visual Tree Assets (uxml-Stylesheets)")]
    [Space]

    //All the needed VisualTreeAssets
    //0
    public VisualTreeAsset initialLoadingScreen_portrait;
    public VisualTreeAsset initialLoadingScreen_landscape;
    [Space]
    //1
    public VisualTreeAsset modeSelectionScreen_portrait;
    public VisualTreeAsset modeSelectionScreen_landscape;
    [Space]
    //2
    public VisualTreeAsset homeWeaScreen_portrait;
    public VisualTreeAsset homeWeaScreen_landscape;
    [Space]
    //3
    public VisualTreeAsset videoRecord_portrait;
    public VisualTreeAsset videoRecord_landscape;
    [Space]
    //4
    public VisualTreeAsset saveProject_portrait;
    public VisualTreeAsset saveProject_landscape;
    [Space]
    //5
    public VisualTreeAsset loadProject_portrait;
    public VisualTreeAsset loadProject_landscape;
    [Space]
    //6
    public VisualTreeAsset arScreen_portrait;
    public VisualTreeAsset arScreen_landscape;
    [Space]
    //7
    public VisualTreeAsset calibTileScreen_portrait;
    public VisualTreeAsset calibTileScreen_landscape;
    [Space]
    //8
    public VisualTreeAsset changeWEAModelScreen_portrait;
    public VisualTreeAsset changeWEAModelScreen_landscape;

    [Space]
    [Header("Scene Objects")]
    [Space]

    public GameObject InitialLoadingScreen;
    public GameObject ModeSelectionScreen;
    public GameObject HomeWEAScreen;
    public GameObject VideoRecord;
    public GameObject SaveProject;
    public GameObject LoadProject;
    public GameObject MainArScreen;
    public GameObject CalibTileScreen;
    public GameObject ChangeWEAModelScreen;

    List<GameObject> uiElements = new List<GameObject>();

    //Private

    private bool isDeviceLandscape = false;

    private bool isCurrentUiLandscapeMode = false;

    private int countSwitchOrientationExecuted = 0;

    //Logik

    private void Awake()
    {
        uiElements.Add(InitialLoadingScreen);
        uiElements.Add(ModeSelectionScreen);
        uiElements.Add(HomeWEAScreen);
        uiElements.Add(VideoRecord);
        uiElements.Add(SaveProject);
        uiElements.Add(LoadProject);
        uiElements.Add(MainArScreen);
        uiElements.Add(CalibTileScreen);
        uiElements.Add(ChangeWEAModelScreen);
    }


    private void Update()
    {
        ListenForOrientationChange();
        ListenForChangeInEditorDebug();

        //Debug.Log("Aktuelle Orientation: " + Input.deviceOrientation);

        //Performs Stylsheet Update once change occured
        if (isCurrentUiLandscapeMode != isDeviceLandscape)
        {
            isCurrentUiLandscapeMode = isDeviceLandscape;
            SwitchBetweenOrientation();
        }

    }

    private void ListenForOrientationChange()
    {
        //Only works on real device, not in Editor (returns Unknown)
        if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            isDeviceLandscape = true;
        }
        else if (Input.deviceOrientation == DeviceOrientation.FaceUp || Input.deviceOrientation == DeviceOrientation.FaceDown
            || Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
        {
            isDeviceLandscape = false;
        }
    }

    private void ListenForChangeInEditorDebug()
    {
    #if UNITY_EDITOR
        //Bool view in Inspector can be used as button to simulate orientation Change
        isDeviceLandscape = landscapeDebug;
    #endif
    }

    /// <summary>
    /// Loops through all UI-Objects and replaces the VisualTreeAsset (uxml) with the correctly orientated one
    /// </summary>
    void SwitchBetweenOrientation()
    {
        for (int i = 0; i < uiElements.Count; i++)
        {
            GameObject screen = uiElements[i];

            // change the uxml
            screen.GetComponent<UIDocument>().visualTreeAsset = ReturnAppropriateVisualTreeAsset(screen.transform.name);
        }

        ReInitializeCurrentScreenController();
    }

    private void ReInitializeCurrentScreenController()
    {
        // re-init the controller of the current screen so that ui listeners work again
        GameObject activeScreen = CanvasManager.GetInstance().GetGameObjectOfActiveCanvas();
        IScreenController screenController = activeScreen.GetComponent<IScreenController>();
        Debug.Log("InitController for " + activeScreen.name + " executed!");
        screenController.InitController();
        
    }

    /// <summary>
    /// Returns correct VisualTreeAsset (uxml) based on Object and 
    /// </summary>
    /// <param name="UIName">Name of GameObject whoms Uxml is to be replaced</param>
    /// <returns></returns>
    private VisualTreeAsset ReturnAppropriateVisualTreeAsset(string UIName)
    {
        switch (UIName)
        {
            case "InitialLoadingScreen":
                if (!isDeviceLandscape)
                {
                    return initialLoadingScreen_portrait;
                }
                else
                {
                    return initialLoadingScreen_landscape;
                }

            case "ModeSelectionScreen":


                if (!isDeviceLandscape)
                {
                    return modeSelectionScreen_portrait;
                }
                else
                {
                    return modeSelectionScreen_landscape;
                }
                
            case "HomeWEAScreen":
                if (!isDeviceLandscape)
                {
                    return homeWeaScreen_portrait;
                }
                else
                {
                    return homeWeaScreen_landscape;
                }

            case "VideoRecord":

                if (!isDeviceLandscape)
                {
                    return videoRecord_portrait;
                }
                else
                {
                    return videoRecord_landscape;
                }

            case "SaveProject":

                if (!isDeviceLandscape)
                {
                    return saveProject_portrait;
                }
                else
                {
                    return saveProject_landscape;
                }

            case "LoadProject":

                if (!isDeviceLandscape)
                {
                    return loadProject_portrait;
                }
                else
                {
                    return loadProject_landscape;
                }

            case "MainARScreen":

                if (!isDeviceLandscape)
                {
                    return arScreen_portrait;
                }
                else
                {
                    return arScreen_landscape;
                }

            case "CalibTileScreen":

                if (!isDeviceLandscape)
                {
                    return calibTileScreen_portrait;
                }
                else
                {
                    return calibTileScreen_landscape;
                }

            case "ChangeWEAModelScreen":

                if (!isDeviceLandscape)
                {
                    return changeWEAModelScreen_portrait;
                }
                else
                {
                    return changeWEAModelScreen_landscape;
                }

            default:
                Debug.LogError("Portrait/Landscape Error occured with " + UIName + " Object. Null was returned. Error Occured in ReturnAppropriateVisualTreeAsset()");
                return null;
        }

    }

    public bool IsDeviceLandscapeMode()
    {
        return isDeviceLandscape;
    }

}
