using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class LoadProjectScreenController : MonoBehaviour, IScreenController
{
    [SerializeField] UIDocument uIDocument;
    private Button cancelButton;
    private Button loadButton;
    private ListView projectList;

    public void InitController()
    {
        OnEnable();
    }

    private void OnEnable()
    {
        var rootVisualElement = GetComponent<UIDocument>().rootVisualElement;

        cancelButton = rootVisualElement.Q<Button>("cancel-button");
        loadButton = rootVisualElement.Q<Button>("load-button");

        loadButton.clicked += () =>
        {
            Loading();
        };

        cancelButton.clicked += () =>
        {
            Canceling();
        };
    }

    private void Loading()
    {
        Debug.Log("Selected File will be loaded");
        // Adding loading logic here
    }

    private void Canceling()
    {
        Debug.Log("Saving canceled");
        CanvasManager.GetInstance().SwitchToScreen(CanvasType.ArScreen);
    }
}
