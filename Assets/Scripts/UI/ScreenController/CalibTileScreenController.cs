using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CalibTileScreenController : MonoBehaviour, IScreenController
{
    [SerializeField] MapViewSizeController mapSizeController;
    [SerializeField] CalibTileHandlerFacade calibTileManager;

    private Button btnDGM, btnCityModel, btnCityModelRgb, btnDOM, btnDOMRgb, btnCancel, btnAccept;

    private Label lblDOMGesamt, lblDOMTexGesamt, lblDGMGesamt, lblCityModelGesamt, lblCityModelTexGesamt;

    private VisualElement rootVisualElement;

    // Holds all registered visual elements on the screen in a list
    private List<VisualElement> visualElementsOnScreen;

    public void InitController()
    {
        OnEnable();
    }

    private void Update()
    {

        if (!calibTileManager.IsInitialized())
        {
            return;
        }

        UpdateSelectedTileCounts();
    }


    private void UpdateSelectedTileCounts()
    {
        List<CalibTileWrapper> allTilesList = calibTileManager.GetAllTiles();
        int selectedCityModelDownload = 0;
        int selectedCityModelTexDownload = 0;
        int selectedDOMDownload = 0;
        int selectedDOMTexDownload = 0;
        int selectedDGMDownload = 0;

        foreach (var tile in allTilesList)
        {
            if (tile.IsSelected())
            {
                if (tile.GetCalibTileType().Equals(TileType.cityModel)) { selectedCityModelDownload++; }
                if (tile.GetCalibTileType().Equals(TileType.cityModel_rgb)) { selectedCityModelTexDownload++; }
                if (tile.GetCalibTileType().Equals(TileType.dom)) { selectedDOMDownload++; }
                if (tile.GetCalibTileType().Equals(TileType.dom_rgb)) { selectedDOMTexDownload++; }
                if (tile.GetCalibTileType().Equals(TileType.dgm)) { selectedDGMDownload++; }

            }
        }
        UpdateTileCountLabels(selectedCityModelDownload, selectedCityModelTexDownload, selectedDOMDownload, selectedDOMTexDownload, selectedDGMDownload);
    }

    private void UpdateTileCountLabels(int selectedCityModelDownload, int selectedCityModelTexDownload, int selectedDOMDownload, int selectedDOMTexDownload, int selectedDGMDownload)
    {
        lblCityModelGesamt.text = selectedCityModelDownload + "/" + calibTileManager.GetCountAvailableTiles(TileType.cityModel) + " Kacheln gewählt";
        lblCityModelTexGesamt.text = selectedCityModelTexDownload + "/" + calibTileManager.GetCountAvailableTiles(TileType.cityModel_rgb) + " Kacheln gewählt";
        lblDOMGesamt.text = selectedDOMDownload + "/" + calibTileManager.GetCountAvailableTiles(TileType.dom) + " Kacheln gewählt";
        lblDOMTexGesamt.text = selectedDOMTexDownload + "/" + calibTileManager.GetCountAvailableTiles(TileType.dom_rgb) + " Kacheln gewählt";
        lblDGMGesamt.text = selectedDGMDownload + "/" + calibTileManager.GetCountAvailableTiles(TileType.dgm) + " Kacheln gewählt";
    }

    void OnEnable()
    {
        AppModeManager.GetInstance().SetAppMode(AppMode.calibscreen);
        InitGuiElements();
        mapSizeController.OpenMapCamFullscreen();
        calibTileManager.ShowTiles();
        mapSizeController.HideMapButtons();
        HighlightSelectedCalibTypeButton(btnCityModel);
        calibTileManager.ShowTilesByTileType(TileType.cityModel);
    }

    public void InitGuiElements()
    {
        ReferenceGuiElements();
        RegisterOnClickListeners();

        HighlightSelectedCalibTypeButton(btnCityModel);
        calibTileManager.ShowTilesByTileType(TileType.cityModel);
    }


    private void ReferenceGuiElements()
    {
        

        rootVisualElement = GetComponent<UIDocument>().rootVisualElement;
        visualElementsOnScreen =  new List<VisualElement>();

        btnDGM = rootVisualElement.Q<Button>("btnGelaende");
        btnCityModel = rootVisualElement.Q<Button>("btnGebaeude");
        btnDOM = rootVisualElement.Q<Button>("btnOberflaeche");
        btnCityModelRgb = rootVisualElement.Q<Button>("btnGebaeudeRgb");
        btnDOMRgb = rootVisualElement.Q<Button>("btnOberflaecheRgb");

        btnCancel = rootVisualElement.Q<Button>("btnCancel");
        btnAccept = rootVisualElement.Q<Button>("btnAccept");

        lblCityModelTexGesamt = rootVisualElement.Q<Label>("lblSelectedCityModelTex");

        lblCityModelGesamt = rootVisualElement.Q<Label>("lblSelectedCityModel");
        lblCityModelTexGesamt = rootVisualElement.Q<Label>("lblSelectedCityModelTex");
        lblDOMGesamt = rootVisualElement.Q<Label>("lblSelectedDOM");
        lblDOMTexGesamt = rootVisualElement.Q<Label>("lblSelectedDOMTex");
        lblDGMGesamt = rootVisualElement.Q<Label>("lblSelectedDGM");

        visualElementsOnScreen.Add(btnDGM);
        visualElementsOnScreen.Add(btnCityModel);
        visualElementsOnScreen.Add(btnDOM);
        visualElementsOnScreen.Add(btnCityModelRgb);
        visualElementsOnScreen.Add(btnDOMRgb);

        //Debug.Log("ReferenceGuiElements: " + visualElementsOnScreen.Count);
    }

    
    private void RegisterOnClickListeners()
    {
        btnDGM.clicked += () =>
        {

            HighlightSelectedCalibTypeButton(btnDGM);
            calibTileManager.ShowTilesByTileType(TileType.dgm);
        };

        btnCityModel.clicked += () =>
        {
            HighlightSelectedCalibTypeButton(btnCityModel);
            calibTileManager.ShowTilesByTileType(TileType.cityModel);
        };

        btnCityModelRgb.clicked += () =>
        {
            HighlightSelectedCalibTypeButton(btnCityModelRgb);
            calibTileManager.ShowTilesByTileType(TileType.cityModel_rgb);
        };

        btnDOM.clicked += () =>
        {
            HighlightSelectedCalibTypeButton(btnDOM);
            calibTileManager.ShowTilesByTileType(TileType.dom);
        };

        btnDOMRgb.clicked += () =>
        {
            HighlightSelectedCalibTypeButton(btnDOMRgb);
            calibTileManager.ShowTilesByTileType(TileType.dom_rgb);
        };

        btnCancel.clicked += () =>
        {
            calibTileManager.ToggleTileControllerSelectedTiles();
            CanvasManager.GetInstance().SwitchBackToLastScreen();
        };

        btnAccept.clicked += () =>
        {
            calibTileManager.WriteCurrentlySelectedTilesToJson();
            CanvasManager.GetInstance().SwitchBackToLastScreen();
        };
    }

    private void HighlightSelectedCalibTypeButton(Button calibTypeButton)
    {
        UnHighlightCalibTypeButton(btnCityModel);
        UnHighlightCalibTypeButton(btnDGM);
        UnHighlightCalibTypeButton(btnDOM);
        UnHighlightCalibTypeButton(btnCityModelRgb);
        UnHighlightCalibTypeButton(btnDOMRgb);

        calibTypeButton.style.width = 150;
        calibTypeButton.style.height = 150;
        calibTypeButton.style.unityBackgroundImageTintColor = Color.grey;
    }

    private void UnHighlightCalibTypeButton(Button calibTypeButton)
    {
        calibTypeButton.style.width = 120;
        calibTypeButton.style.height = 120;
        calibTypeButton.style.unityBackgroundImageTintColor = new Color(255, 255, 255);
    }

    void OnDisable()
    {
        AppModeManager.GetInstance().PopAppModeStack();
        calibTileManager.HideTiles();
        mapSizeController.CloseMapCam();
        mapSizeController.ShowMapButtons();
    }

    public bool IsPointerOverUI(Vector2 screenPos)
    {

        foreach (var visualElement in visualElementsOnScreen)
        {
            //Debug.Log("isOverUI: " + visualElement + " " + visualElementsOnScreen.Count);

            var touchPos = new Vector2(screenPos.x / Screen.width, screenPos.y / Screen.height);
            var flippedPosition = new Vector2(touchPos.x, 1 - touchPos.y);
            var adjustedPosition = flippedPosition * visualElement.panel.visualTree.layout.size;

            
            if (visualElement.ContainsPoint(visualElement.WorldToLocal(adjustedPosition)))
            {
                return true;
            }
        }
        return false;
    }
}
