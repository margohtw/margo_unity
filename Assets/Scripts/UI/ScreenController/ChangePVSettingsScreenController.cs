using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

enum Submenus
{
    Ausrichtung,
    Abstand,
    Modultyp,
    Umrandung
}

public class ChangePVSettingsScreenController : MonoBehaviour, IScreenController
{
    [SerializeField] private UIDocument uIDocument;

    private Submenus currentStatus = Submenus.Ausrichtung;
    private PolygonController polygonController;

    private Button btn_reihenabstand, btn_ausrichtung, btn_modultyp, btn_umrandung, btn_bestaetigen;
    private Slider slider_ausrichtung, slider_reihenabstand, slider_modulabstand, slider_umrandung_hoehe;
    private VisualElement main_buttons_holder, ausrichtung_elements_holder, abstand_elements_holder, modultyp_elements_holder, umrandung_elements_holder;
    private Toggle toggle_modultyp_a, toggle_modultyp_b, toggle_modultyp_c, toggle_zaun, toggle_hecke;
    private VisualElement modultyp_image, umrandung_image;
    private Label lbl_ausrichtung_wert, lbl_reihenabstand_wert, lbl_modulabstand_wert, lbl_umrandung_hoehe_wert;


    private List<VisualElement> submenuList;
    private List<VisualElement> btnList;
    private List<Toggle> toggleListModulType, toggleListUmrandungsType;
    
    public void InitController()
    {
        OnEnable();
    }

    private void OnEnable()
    {
        ReferenceGuiElements();
        RegisterInteractionListeners();  
        currentStatus = Submenus.Ausrichtung;
        SwitchToSubmenu(Submenus.Ausrichtung);
        MapViewSizeController.GetInstance().OpenMapCamFullscreen();
    }

    private void ReferenceGuiElements()
    {
        var rootVisualElement = GetComponent<UIDocument>().rootVisualElement;

        btn_reihenabstand = rootVisualElement.Q<Button>("btn_reihenabstand");
        btn_ausrichtung = rootVisualElement.Q<Button>("btn_ausrichtung");
        btn_modultyp = rootVisualElement.Q<Button>("btn_modultyp");
        btn_umrandung = rootVisualElement.Q<Button>("btn_umrandung");
        btn_bestaetigen = rootVisualElement.Q<Button>("btn_bestaetigen");

        main_buttons_holder = rootVisualElement.Q<VisualElement>("main_buttons_holder");
        ausrichtung_elements_holder = rootVisualElement.Q<VisualElement>("ausrichtung_elements_holder");
        abstand_elements_holder = rootVisualElement.Q<VisualElement>("abstand_elements_holder");
        modultyp_elements_holder = rootVisualElement.Q<VisualElement>("modultyp_elements_holder");
        umrandung_elements_holder = rootVisualElement.Q<VisualElement>("umrandung_elements_holder");

        slider_ausrichtung = rootVisualElement.Q<Slider>("slider_ausrichtung");
        slider_reihenabstand = rootVisualElement.Q<Slider>("slider_reihenabstand");
        slider_modulabstand = rootVisualElement.Q<Slider>("slider_modulabstand");
        slider_umrandung_hoehe = rootVisualElement.Q<Slider>("slider_umrandung_hoehe");

        lbl_ausrichtung_wert = rootVisualElement.Q<Label>("label_ausrichtung_wert");
        lbl_ausrichtung_wert.text = "0° (S)";
        lbl_reihenabstand_wert = rootVisualElement.Q<Label>("label_reihenabstand_wert");
        lbl_modulabstand_wert = rootVisualElement.Q<Label>("label_modulabstand_wert");
        lbl_umrandung_hoehe_wert = rootVisualElement.Q<Label>("label_umrandung_hoehe_wert");

        toggle_modultyp_a = rootVisualElement.Q<Toggle>("toggle_modultyp_a");
        toggle_modultyp_b = rootVisualElement.Q<Toggle>("toggle_modultyp_b");
        toggle_modultyp_c = rootVisualElement.Q<Toggle>("toggle_modultyp_c");

        toggle_modultyp_a.SetEnabled(true);

        toggle_zaun = rootVisualElement.Q<Toggle>("toggle_zaun");
        toggle_hecke = rootVisualElement.Q<Toggle>("toggle_hecke");

        toggle_zaun.SetEnabled(true);

        modultyp_image = rootVisualElement.Q<VisualElement>("modultyp_image");
        umrandung_image = rootVisualElement.Q<VisualElement>("umrandung_image");

        toggleListModulType = new List<Toggle>();
        toggleListModulType.Add(toggle_modultyp_a);
        toggleListModulType.Add(toggle_modultyp_b);
        toggleListModulType.Add(toggle_modultyp_c);

        toggleListUmrandungsType = new List<Toggle>();
        toggleListUmrandungsType.Add(toggle_zaun);
        toggleListUmrandungsType.Add(toggle_hecke);

        submenuList = new List<VisualElement>();
        submenuList.Add(ausrichtung_elements_holder);
        submenuList.Add(abstand_elements_holder);
        submenuList.Add(modultyp_elements_holder);
        submenuList.Add(umrandung_elements_holder);

        btnList = new List<VisualElement>();
        btnList.Add(btn_reihenabstand);
        btnList.Add(btn_ausrichtung);
        btnList.Add(btn_modultyp);
        btnList.Add(btn_umrandung);

        List<VisualElement> interactionBlockers = new List<VisualElement>();
        interactionBlockers = getInteractionBlockers(interactionBlockers, rootVisualElement);

        CanvasManager.GetInstance().GetGameObjectOfActiveCanvas().GetComponent<MainArUIManager>().RegisterMouseOverListenersPvSettings(interactionBlockers);
    }

    private List<VisualElement> getInteractionBlockers(List<VisualElement> ibList, VisualElement rootVisualElement)
    {
        foreach (VisualElement el in rootVisualElement.Children())
        {
            ibList.Add(el);
            getInteractionBlockers(ibList, el);
        }
        return ibList;
    }

    private void RegisterInteractionListeners()
    {
        btn_ausrichtung.clicked += () =>
        {
            SwitchToSubmenu(Submenus.Ausrichtung);
        };

        btn_reihenabstand.clicked += () =>
        {
            SwitchToSubmenu(Submenus.Abstand);
        };

        btn_modultyp.clicked += () =>
        {
            SwitchToSubmenu(Submenus.Modultyp);
        };

        btn_umrandung.clicked += () =>
        {
            SwitchToSubmenu(Submenus.Umrandung);
        };

        btn_bestaetigen.clicked += () =>
        {
            OnConfirm();
        };

        toggle_modultyp_a.RegisterValueChangedCallback(ToggleModultypDefault);
        toggle_modultyp_b.RegisterValueChangedCallback(ToggleModultypAgri);

        toggle_zaun.RegisterValueChangedCallback(ToggleUmrandungZaun);
        toggle_hecke.RegisterValueChangedCallback(ToggleUmrandungHecke);

        slider_ausrichtung.RegisterValueChangedCallback(AusrichtungSliderChangeCallback);
        slider_reihenabstand.RegisterValueChangedCallback(ReihenabstandSliderChangeCallback);
        slider_modulabstand.RegisterValueChangedCallback(ModulabstandSliderChangeCallback);
        slider_umrandung_hoehe.RegisterValueChangedCallback(UmrandungsHoeheSliderChangeCallback);
    }

    private void ToggleModultypDefault(ChangeEvent<bool> changeEvent)
    {
        SetOtherTogglesFalse(toggle_modultyp_a, toggleListModulType);
        polygonController.SwitchPvModels(PvModelType.Default);
        modultyp_image.style.backgroundImage = new StyleBackground(PvModelManager.GetInstance().GetPvModel(PvModelType.Default).GetImage());
    }

    private void ToggleModultypAgri(ChangeEvent<bool> changeEvent)
    {
        SetOtherTogglesFalse(toggle_modultyp_b, toggleListModulType);
        polygonController.SwitchPvModels(PvModelType.Agri);
        modultyp_image.style.backgroundImage = new StyleBackground(PvModelManager.GetInstance().GetPvModel(PvModelType.Agri).GetImage());
    }

    private void ToggleUmrandungZaun(ChangeEvent<bool> changeEvent)
    {
        SetOtherTogglesFalse(toggle_zaun, toggleListUmrandungsType);
        polygonController.SwitchUmrandungsModel(UmrandungsType.Zaun);
        umrandung_image.style.backgroundImage = new StyleBackground(UmrandungsModelManager.GetInstance().GetUmrandungsModel(UmrandungsType.Zaun).GetModelImage());
    }
    
    private void ToggleUmrandungHecke(ChangeEvent<bool> changeEvent)
    {
        SetOtherTogglesFalse(toggle_hecke, toggleListUmrandungsType);
        polygonController.SwitchUmrandungsModel(UmrandungsType.Hecke);
        umrandung_image.style.backgroundImage = new StyleBackground(UmrandungsModelManager.GetInstance().GetUmrandungsModel(UmrandungsType.Hecke).GetModelImage());
    }

    private void SetOtherTogglesFalse(Toggle activeToggle, List<Toggle> toggleList)
    {
        foreach (Toggle toggle in toggleList)
        {
            if (toggle != activeToggle) toggle.SetValueWithoutNotify(false);
        }
    }

    private void AusrichtungSliderChangeCallback(ChangeEvent<float> changeEvent)
    {
        float value = changeEvent.newValue - 90f;
        string direction = "";
        if (value < -30) direction = "(SO)";
        else if (value >= -30 && value <= 30) direction = "(S)";
        else direction = "SW";
        int valueRounded = (int) value;
        valueRounded = Math.Abs(valueRounded);
        lbl_ausrichtung_wert.text = valueRounded + "° " + direction;
        polygonController.UpdateRotation(value);
    }

    private void ReihenabstandSliderChangeCallback(ChangeEvent<float> changeEvent)
    {
        string result = Math.Round(polygonController.UpdateRowDistance(changeEvent.newValue), 2) + "m";
        lbl_reihenabstand_wert.text = result;
    }

    private void ModulabstandSliderChangeCallback(ChangeEvent<float> changeEvent)
    {
        float result = polygonController.UpdateModuleDistance(changeEvent.newValue);
        string labelText = Math.Round(result, 2) + "m";
        lbl_modulabstand_wert.text = labelText;
    }

    private void UmrandungsHoeheSliderChangeCallback(ChangeEvent<float> changeEvent)
    { 
        float result = polygonController.UpdateUmrandungsModelHeight(changeEvent.newValue);
        string labelText = Math.Round(result, 2) + "m";
        lbl_umrandung_hoehe_wert.text = labelText;
    }

    private void OnConfirm()
    {
        CanvasManager.GetInstance().SwitchToScreen(CanvasType.ArScreen);
    }

    private void SwitchToSubmenu(Submenus target)
    {
        VisualElement newSubmenu;
        VisualElement newActiveBtn;
        switch (target)
        {
            case Submenus.Ausrichtung:
                newSubmenu = ausrichtung_elements_holder;
                newActiveBtn = btn_ausrichtung;
                break;
            case Submenus.Abstand:
                newSubmenu = abstand_elements_holder;
                newActiveBtn = btn_reihenabstand;
                break;
            case Submenus.Modultyp:
                newSubmenu = modultyp_elements_holder;
                newActiveBtn = btn_modultyp;
                break;
            case Submenus.Umrandung:
                newSubmenu = umrandung_elements_holder;
                newActiveBtn = btn_umrandung;
                break;
            default:
                newSubmenu = ausrichtung_elements_holder;
                newActiveBtn = btn_ausrichtung;
                break;
        }

        foreach (var btn in btnList)
        {
            if (btn == newActiveBtn) btn.style.backgroundColor = new StyleColor(new Color(1f, 1f, 1f));
            else btn.style.backgroundColor = new StyleColor(new Color(0.72f, 0.72f, 0.72f));
        }

        foreach (var submenu in submenuList)
        {
            if (submenu == newSubmenu)
            {
                RecursivelySetVisible(submenu, true);
            }
            else
            {
                RecursivelySetVisible(submenu, false);
            }
        }

        currentStatus = target;
    }

    private void RecursivelySetVisible(VisualElement visualElement, bool visible)
    {
        if (visible) visualElement.style.display = DisplayStyle.Flex;
        else visualElement.style.display = DisplayStyle.None;

        if (visualElement.childCount == 0) return;

        foreach (var child in visualElement.Children())
        {
            RecursivelySetVisible(child, visible);
        }
    }

    public void SetPolygonController(PolygonController polygonController)
    {
        this.polygonController = polygonController;
    }
}

