using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UIElements;

public class InitialLoadingScreenController : MonoBehaviour, IScreenController
{
    [SerializeField]
    UIDocument uIDocument;

    [SerializeField] private float rotateSpeed;
    [SerializeField] private float fadeoutTime;

    [SerializeField] private ArOutputTextureScaler arOutputTextureScaler;

    private GameObject arCamera;

    private VisualElement loadingCircle;
    private Label infoText;

    private bool isEnabled;
    private float elapsedTime;
    private bool isUpdating;

    public void InitController()
    {
        Start();
    }

    void Start()
    {
        ReferenceGuiElements();

        AdjustArOutputTextureRenderer();

        arCamera = GameObject.Find("AR Camera");

        isEnabled = true;
        isUpdating = true;

        infoText.text = "Locating device coordinates...";
    }

    void Update()
    {
        if (isEnabled)
        {
            StartCoroutine(RotateLoadingCircle());
        }
    }

    private void ReferenceGuiElements()
    {
        var rootVisualElement = GetComponent<UIDocument>().rootVisualElement;
        loadingCircle = rootVisualElement.Q<VisualElement>("loadingCircle");
        infoText = (Label)rootVisualElement.Q<VisualElement>("infoText");
    }

    private void AdjustArOutputTextureRenderer()
    {
        arOutputTextureScaler.AdjustTextureRendererInitial();
    }


    IEnumerator RotateLoadingCircle()
    {
        // this is necessary because UIElements don't allow the setting of pivots
        // the pivot is always at the top left corner of the element

        float targetRotationDeg = loadingCircle.transform.rotation.eulerAngles.z + rotateSpeed * Time.deltaTime;

        // Get the x/y location of the center note - these are constant unless the parent rescales; i.e., they don't change with rotation.
        float x0 = loadingCircle.contentRect.center.x;
        float y0 = loadingCircle.contentRect.center.y;

        // Convert Cartesian to Polar
        float r = Mathf.Sqrt(x0 * x0 + y0 * y0);
        float theta0 = Mathf.Atan2(y0, x0);

        // Calculate the location of the center of the VisualElement after rotating
        // Note: The rotation you want is *in addition* to the "default" polar angle from origin to the center
        float x = r * Mathf.Cos(theta0 + (Mathf.Deg2Rad * targetRotationDeg));
        float y = r * Mathf.Sin(theta0 + (Mathf.Deg2Rad * targetRotationDeg));

        // Actually do the requested rotation
        loadingCircle.transform.rotation = Quaternion.Euler(0f, 0f, targetRotationDeg);

        // Finally, rotation happens about the upper-left corner of the VisualElement, so you need to shift the position
        // to get the rotated center to be coincident with the un-rotated center.
        float xDelta = x0 - x;
        float yDelta = y0 - y;
        loadingCircle.transform.position = new Vector3(xDelta, yDelta, 0f);

        yield return null;
    }

    IEnumerator FadeOut()
    {
        float progress = 0;
        while (elapsedTime < fadeoutTime)
        {
            elapsedTime += Time.deltaTime;
            progress = elapsedTime / fadeoutTime;
            float alpha = 1 - progress;
            infoText.style.color = new StyleColor(new Color(196f, 196f, 196f, alpha));
            yield return null;
        }
        if (progress >= 1) GetComponentInParent<CanvasManager>().InitializeApplication();
        yield return null;
    }

    public void UpdateInfoText(bool overrideMode, bool success, int requestCounter)
    {
        if (!isUpdating) return;
        if (overrideMode)
        {
            isUpdating = false;
            FinalizeDeterminePosition(true, "Using override latitutde/longitude values.");
        }
        else if (success)
        {
            isUpdating = false;
            FinalizeDeterminePosition(true, "Device located!\nStarting application...");
        }
        else  
        {
            if (requestCounter >= 3)
            {
                isUpdating = false;
                FinalizeDeterminePosition(false, "Device could not be located!\n");
            }
            else infoText.text = "Device location unsuccessful, attempt " + requestCounter + "/3";
        }
    }

    private void FinalizeDeterminePosition(bool success, string message)
    {
        infoText.text = message;
        if (success)
        {
            // TODO initialize things here?
        }
        else
        {
            //TODO give opportunity to try again?
        }

        StartCoroutine(FadeOut());
    }
}
