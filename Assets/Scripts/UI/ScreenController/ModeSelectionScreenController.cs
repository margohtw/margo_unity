using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ModeSelectionScreenController : MonoBehaviour, IScreenController
{
    [SerializeField] private UIDocument uIDocument;

    private Button btnWea;
    private Button btnPv;

    public void InitController()
    {
        OnEnable();
    }

    void OnEnable()
    {
        ReferenceGuiElements();
        RegisterOnClickListeners();
        AppModeManager.GetInstance().PopAppModeStack();
    }

    private void ReferenceGuiElements()
    {
        var rootVisualElement = GetComponent<UIDocument>().rootVisualElement;
        btnWea = rootVisualElement.Q<Button>("btnWEA");
        btnPv = rootVisualElement.Q<Button>("btnPVA");
    }

    private void RegisterOnClickListeners()
    {
        btnWea.clicked += () =>
        {
            ShowHomeWeaScreen();
        };

        btnPv.clicked += () =>
        {
            ShowHomePvScreen();
        };
    }

    private void ShowHomeWeaScreen()
    {
        Debug.Log("Klick - Wechsle zu Home-WEA-Screen ...");
        AppModeManager.GetInstance().SetAppMode(AppMode.wea);
        CanvasManager.GetInstance().SwitchToScreen(CanvasType.HomeWeaScreen);
    }

    private void ShowHomePvScreen()
    {
        Debug.Log("Klick - Wechsle zu Home-PV-Screen ...");
        AppModeManager.GetInstance().SetAppMode(AppMode.pv);
        CanvasManager.GetInstance().SwitchToScreen(CanvasType.HomeWeaScreen);
    }

}
