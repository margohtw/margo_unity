using System.Collections.Generic;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UIElements.Button;
using Slider = UnityEngine.UIElements.Slider;

public class MainArUIManager : MonoBehaviour
{
    public enum ArUiMode
    {
        None,
        InitialGpsPositioning,
        Stabilization,
        LoadingCalibModels,
        Calibration,
        RegularUse
    }

    private ArUiMode currentArUiMode = ArUiMode.None;

    private MainArScreenController mainArScreenController;

    [SerializeField] private UnityEngine.UI.Image progressCircle;
    [SerializeField] private GameObject RawArImage;
    [SerializeField] private UnityEngine.UI.Slider progressBar;
    [SerializeField] private Text progressBarInfoText;
    [SerializeField] private ChangeToLandscape changeToLandscape;
    [SerializeField] private GameObject weaModelHolder;
    [SerializeField] private GameObject pvModelHolder;
    [SerializeField] private GameObject userLocationMarker;
    [SerializeField] private AbstractMap map;

    private Button btnMap;
    private Button btnCalib;
    private Button btnCenter;
    private Button btnAccept;
    private Button btnMenu;
    private Button btnMenuCalibTypeSelection;
    private Button btnExit;

    private Slider cameraZoomSlider;
    private Slider distanceSlider;
  
    //Floating Side Menu
    private Button btnVideo;
    private Button btnSave;
    private Button btnLoad;
    private Button btnPOI;
    
    private VisualElement bottomNavBar;
    private VisualElement rootVisualElement;

    //check calib mode
    private bool isCalibModeActive;
    
    //once calibration is done, dont switch back
    private bool initialized;

    // Holds all registered visual elements on the screen in a list
    private List<VisualElement> visualElementsOnScreen = new List<VisualElement>();

    // Information if the mouse is currently over an UI toolkit element
    public static bool IsMouseOverUI;
    
    //if the user has been here before, skip the stabilization+initialization
    private bool initComplete;

    //Debugging
    LatLngUTMConverter utmConverter = new LatLngUTMConverter("ETRS89");

    int i = 0;

    private void OnEnable()
    {
        RawArImage.SetActive(true);
    }

    private void OnDisable()
    {
        RawArImage.SetActive(false);
    }

    void Update()
    {
        /*//DEBUG
        Vector3 camPos = arCamera.transform.position;
        Vector3 camAngles = arCamera.transform.rotation.eulerAngles;
        Vector3 gyroAngles = Input.gyro.attitude.eulerAngles;
        float x = Mathf.Round(camPos.x * 100) / 100;
        float y = Mathf.Round(camPos.y * 100) / 100;
        float z = Mathf.Round(camPos.z * 100) / 100;
        string camDebugText = "Cam Pos: " + x + ", " + y + ", " + z +
                              "\nCam Angles: " + (int) camAngles.x + ", " + (int) camAngles.y + ", " +
                              (int) camAngles.z +
                              "\nGyro Values: " + (int) gyroAngles.x + ", " + (int) gyroAngles.y + ", " +
                              (int) gyroAngles.z +
                              "\nMagnetic Heading: " + Input.compass.trueHeading +
                              "\nAndroid Heading: " + WorldToUnitySpaceConverter.GetInstance().GetAndroidAngleToNorth() +
                              "\nOffset:" +
                              WorldToUnitySpaceConverter.GetInstance().GetCalculatedOffset();

                              SetCamDebugText(camDebugText);
        //DEBUG*/
    }

    public void Initialize()
    {
        InitGuiElements();

        CalibLogger.GetInstance().startCalibLogger();
        CalibLogger.GetInstance().addText("MAP POSITIONING: " + mainArScreenController.isMapBasedCalibrationEnabled());

        if (!initComplete && mainArScreenController.isMapBasedCalibrationEnabled())
        {
            SwitchUiMode(ArUiMode.InitialGpsPositioning);
            
        }
        else if (initComplete) SwitchUiMode(ArUiMode.RegularUse);
        else SwitchUiMode(ArUiMode.Stabilization);
    }

    private void InitGuiElements()
    {

        mainArScreenController = GetComponent<MainArScreenController>();

        ReferenceGuiElements();
        RegisterMouseOverListeners();
        RegisterOnClickListeners();
        MakeUiScreenSettings();
    }

    private void MakeUiScreenSettings()
    {
        // Rotate the progress bar by 90� (this is not possible through ui builder)
        cameraZoomSlider.transform.rotation *= Quaternion.Euler(0f, 0f, -90f);
    }

    private void ReferenceGuiElements()
    {

        rootVisualElement = GetComponent<UIDocument>().rootVisualElement;
        //Debug.LogFormat("ReferenceGuiElements ...");

        btnMap = rootVisualElement.Q<Button>("btnMap");
        btnCalib = rootVisualElement.Q<Button>("btnCalib");
        btnCenter = rootVisualElement.Q<Button>("btnCenter");
        btnAccept = rootVisualElement.Q<Button>("btnAccept");
        btnMenu = rootVisualElement.Q<Button>("btnMenu");
        btnMenuCalibTypeSelection = rootVisualElement.Q<Button>("btnMenuCalibTypeSelection");
        btnVideo = rootVisualElement.Q<Button>("btnVideo");
        btnSave = rootVisualElement.Q<Button>("btnSave");
        btnLoad = rootVisualElement.Q<Button>("btnLoad");
        btnPOI = rootVisualElement.Q<Button>("btnPOI");

        btnExit = rootVisualElement.Q<Button>("btnExit");

        cameraZoomSlider = rootVisualElement.Q<Slider>("sliderCameraZoom");

        bottomNavBar = rootVisualElement.Q<VisualElement>("BottomNavBar");

        visualElementsOnScreen.Add(btnMap);
        visualElementsOnScreen.Add(btnCalib);
        visualElementsOnScreen.Add(btnCenter);
        visualElementsOnScreen.Add(btnMenu);
        visualElementsOnScreen.Add(btnVideo);
        visualElementsOnScreen.Add(btnSave);
        visualElementsOnScreen.Add(btnLoad);
        visualElementsOnScreen.Add(btnPOI);
        visualElementsOnScreen.Add(cameraZoomSlider);
    }

    private void RegisterMouseOverListeners()
    {
        // This is used to fix the click through bug (ui toolkit event system)
        foreach (var visualElement in visualElementsOnScreen)
        {
            visualElement.RegisterCallback<PointerOverEvent>((evt) =>
            {
                Debug.Log("Enter");
                // mouse is over ui toolkit gui
                IsMouseOverUI = true;
            });
            visualElement.RegisterCallback<PointerOutEvent>((evt) =>
            {
                Debug.Log("Leave");
                // mouse left the ui toolkit gui
                IsMouseOverUI = false;
            });
        }
    }

    public void RegisterMouseOverListenersPvSettings(List<VisualElement> elements) 
    {
        foreach (var visualElement in elements)
        {
            visualElement.RegisterCallback<PointerOverEvent>((evt) =>
            {
                Debug.Log("Enter");
                // mouse is over ui toolkit gui
                IsMouseOverUI = true;
            });
            visualElement.RegisterCallback<PointerOutEvent>((evt) =>
            {
                Debug.Log("Leave");
                // mouse left the ui toolkit gui
                IsMouseOverUI = false;
            });
        }
    }

    private void RegisterOnClickListeners()
    {
        distanceSlider.RegisterValueChangedCallback(mainArScreenController.OnDistanceSliderChanged);
        cameraZoomSlider.RegisterValueChangedCallback(mainArScreenController.OnCameraZoomSliderChanged);

        btnMap.clicked += () =>
        {
            mainArScreenController.ToggleMapVisibility();
        };

        btnCalib.clicked += () =>
        {
            SwitchUiMode(ArUiMode.Calibration);
        };

        btnCenter.clicked += () =>
        {
            mainArScreenController.PlaceWEAModel();
        };

        btnMenu.clicked += () =>
        {
            ToggleFloatingSideMenu();
        };

        btnMenuCalibTypeSelection.clicked += () =>
        {
            OpenMenuCalibTypeSelection();
        };

        btnAccept.clicked += () =>
        {
            mainArScreenController.EndCalibration();
            SwitchUiMode(ArUiMode.RegularUse);
        };

        btnVideo.clicked += () =>
        {
            mainArScreenController.OpenVideo();
        };

        btnSave.clicked += () =>
        {
            mainArScreenController.OpenSave();
        };

        btnLoad.clicked += () =>
        {
            mainArScreenController.OpenLoad();
        };

        btnPOI.clicked += () =>
        {
            mainArScreenController.OpenPOI();
        };
    }

    public void SwitchUiMode(ArUiMode mode)
    {
        currentArUiMode = mode;
        switch (mode)
        {
            case ArUiMode.InitialGpsPositioning:
                MapViewSizeController.GetInstance().ShowUIInitialGpsPositioning();
                ShowUIInitialGpsPositioningMode();

                // LOGGING
                LatLngUTMConverter.UTMResult utmPos = utmConverter.convertLatLngToUtm(UserLocationProvider.GetInstance().GetCurrentLatitude(), UserLocationProvider.GetInstance().GetCurrentLongitude());
                CalibLogger.GetInstance().addText("Enter Initial Positioning - UTM Pos: " + utmPos.Easting + " / " + utmPos.Northing);

                /*Do we need this?
                 *WorldToUnitySpaceConverter.GetInstance().StartInitialGpsPositioning(new Vector2d( UserLocationProvider.GetInstance().GetCurrentLatitude(), UserLocationProvider.GetInstance().GetCurrentLongitude()));*///userLocationMarker.SetActive(false);
                break;
            case ArUiMode.Stabilization:
                WorldToUnitySpaceConverter.GetInstance().EndInitialGpsPositioning(map.WorldToGeoPosition(map.gameObject.transform.position));
                MapViewSizeController.GetInstance().HideUIInitialGpsPositioning();

                // LOGGING
                LatLngUTMConverter.UTMResult utmPos2 = utmConverter.convertLatLngToUtm(UserLocationProvider.GetInstance().GetCurrentLatitude(), UserLocationProvider.GetInstance().GetCurrentLongitude());
                CalibLogger.GetInstance().addText("Leave Initial Positioning - UTM Pos: " + utmPos2.Easting + " / " + utmPos2.Northing);

                
                WorldToUnitySpaceConverter.GetInstance().Init();
                ShowUIStabilizationMode();
                break;
            case ArUiMode.LoadingCalibModels:
                StartLoadingCalibModelsMode();
                break;
            case ArUiMode.Calibration:
                StartCalibrationMode();
                break;
            case ArUiMode.RegularUse:
                StartRegularUseMode();
                break;
        }
    }

    private void ShowUIInitialGpsPositioningMode()
    {
        userLocationMarker.SetActive(true); // control here if user location marker should be visible in the beginning of InitGpsPositioningMode
        progressBar.gameObject.SetActive(false);
        btnCenter.style.visibility = Visibility.Hidden;
        bottomNavBar.style.visibility = Visibility.Hidden;
        btnAccept.style.visibility = Visibility.Hidden;
        btnMenu.style.visibility = Visibility.Hidden;
        HideCameraZoomSlider();
        btnMenuCalibTypeSelection.style.visibility = Visibility.Hidden;
        RecenterMapOnInitGpsPosition();
    }

    private void ShowUIStabilizationMode()
    {
        userLocationMarker.SetActive(true);
        progressBarInfoText.text = "Keep Phone Stable";
        progressBar.gameObject.SetActive(true);
        btnCenter.style.visibility = Visibility.Hidden;
        bottomNavBar.style.visibility = Visibility.Hidden;
        btnAccept.style.visibility = Visibility.Hidden;
        btnMenu.style.visibility = Visibility.Hidden;
        //ShowCameraZoomSlider();
        btnMenuCalibTypeSelection.style.visibility = Visibility.Hidden;
        HideCameraZoomSlider();
    }

    private void StartLoadingCalibModelsMode()
    {
        if (!CalibModelHandler.GetInstance().AreTilesLoaded())
        {
            progressBar.SetValueWithoutNotify(0f);
            progressBarInfoText.text = "Loading Calib Models";
            CalibModelHandler.GetInstance().BeginLoadingTiles();
        }
        else SwitchUiMode(ArUiMode.Calibration);
    }

    private void StartCalibrationMode()
    {
        if (!initComplete)
        {
            UserLocationProvider.GetInstance().StopLocationUpdates();
            CalibModelHandler.GetInstance().SetInitialCalibTileRotation();
        }
        ShowUICalibrationMode();
        isCalibModeActive = true;
    }

    private void ShowUICalibrationMode()
    {
        pvModelHolder.SetActive(false);
        //weaModelHolder.SetActive(false);
        mainArScreenController.EnterCalibMode();
        CalibModelHandler.GetInstance().InitializeTypeVisibility();
        progressBar.gameObject.SetActive(false);
        btnCenter.style.visibility = Visibility.Hidden;
        bottomNavBar.style.visibility = Visibility.Hidden;
        //btnMenuCalibTypeSelection.style.visibility = Visibility.Visible;
        btnMenuCalibTypeSelection.style.visibility = Visibility.Hidden;
        btnMenu.style.visibility = Visibility.Hidden;
        btnAccept.style.visibility = Visibility.Visible;
    }

    private void StartRegularUseMode()
    {
        initComplete = true;
        pvModelHolder.SetActive(true);
        weaModelHolder.SetActive(true);
        bottomNavBar.style.visibility = Visibility.Visible;
        btnAccept.style.visibility = Visibility.Hidden;
        btnMenuCalibTypeSelection.style.visibility = Visibility.Hidden;
        btnMenu.style.visibility = Visibility.Visible;
        btnCenter.style.visibility = Visibility.Visible;
        //ShowCameraZoomSlider();
        HideCameraZoomSlider(); //hide zoom slider always for now
        isCalibModeActive = false;
    }

    public void ToggleFloatingSideMenu()
    {
        if (btnVideo.style.visibility == Visibility.Visible)
        {
            btnVideo.style.visibility = Visibility.Hidden;
            btnSave.style.visibility = Visibility.Hidden;
            btnLoad.style.visibility = Visibility.Hidden;
            btnPOI.style.visibility = Visibility.Hidden;
        }
        else
        {
            btnVideo.style.visibility = Visibility.Visible;
            btnSave.style.visibility = Visibility.Visible;
            btnLoad.style.visibility = Visibility.Visible;
            btnPOI.style.visibility = Visibility.Visible;
        }
    }

    public void OpenMenuCalibTypeSelection()
    {
        //CanvasManager.GetInstance().SwitchToScreen(CanvasType.SelectCalibTileTypeScreen);
        rootVisualElement = GetComponent<UIDocument>().rootVisualElement;
        VisualElement window = rootVisualElement.Q<VisualElement>("SelectCalibTileTypeScreen");
        if (changeToLandscape.IsDeviceLandscapeMode())
        {
            // Change appearance of SelectCalibTileTypeScreen in landscape mode (vertical split)
            VisualElement selectCalibTileTypeScreen = rootVisualElement.Q<VisualElement>("SelectCalibTileTypeScreen");
            selectCalibTileTypeScreen.style.bottom = new StyleLength(new Length(0, LengthUnit.Pixel));;
            selectCalibTileTypeScreen.style.height = new StyleLength(new Length(70, LengthUnit.Percent));
            selectCalibTileTypeScreen.style.width = new StyleLength(new Length(50, LengthUnit.Percent));
        }
        window.style.display = DisplayStyle.Flex;
    
    }

    public bool IsCalibrationModeActive()
    {
        return isCalibModeActive;
    }

    public void SetLoadingBarProgress(float progress)
    {
        progressBar.SetValueWithoutNotify(progress);
        //progressCircle.fillAmount = progress;
    }

    public void HideCameraZoomSlider()
    {
        cameraZoomSlider.style.display = DisplayStyle.None;
    }

    public void ShowCameraZoomSlider()
    {
        cameraZoomSlider.style.display = DisplayStyle.Flex;
    }

    public bool IsInGpsInitMode()
    {
        if (currentArUiMode == ArUiMode.InitialGpsPositioning) return true;
        return false;
    }

    public void RecenterMapOnInitGpsPosition()
    {
        MapInteraction mapInteraction = map.GetComponent<MapInteraction>();
        mapInteraction.RecenterMapOnUserLocation();
    }
    public bool isMapBasedCalibrationEnabled()
    {
        return mainArScreenController.isMapBasedCalibrationEnabled();
    }
}
