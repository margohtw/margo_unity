using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UIElements;

public class SaveProjectScreenController : MonoBehaviour, ISaveable, IScreenController
{
    [SerializeField] private UIDocument uIDocument;
    [SerializeField] WeaMapHandler weaMapHandler;

    private Button cancelButton;
    private Button saveButton;
    private TextField projectName;

    private Button feedbackOkButton;
    private VisualElement feedbackWindow;
    private Label feedbackLabel;

    private bool isSuccessfullySaved = false;

    public void InitController()
    {
        OnEnable();
    }
    private void OnEnable()
    {
        var rootVisualElement = GetComponent<UIDocument>().rootVisualElement;

        cancelButton = rootVisualElement.Q<Button>("cancel-button");
        saveButton = rootVisualElement.Q<Button>("save-button");
        projectName = rootVisualElement.Q<TextField>("input");

        feedbackWindow = rootVisualElement.Q<VisualElement>("feedback-window");
        feedbackOkButton = rootVisualElement.Q<Button>("feedback-window-ok-button");
        feedbackLabel = rootVisualElement.Q<Label>("feedback-window-lbl-text");

        saveButton.clicked += () =>
        {
            isSuccessfullySaved = ProjectDataSaver.SaveJsonData(this);
            if (isSuccessfullySaved)
            {
                feedbackLabel.text = "Projekt wurde erfolgreich gespeichert.";
                feedbackWindow.style.visibility = Visibility.Visible;
            }
            else
            {
                feedbackLabel.text = "Fehler beim Speichern des Projektes!\nBitte pr�fen, ob ein Projekt mit gleichem Projektnamen schon existiert.\n";
                feedbackWindow.style.visibility = Visibility.Visible;
            }
        };

        feedbackOkButton.clicked += () =>
        {
            feedbackWindow.style.visibility = Visibility.Hidden;
            if (isSuccessfullySaved)
            {
                CanvasManager.GetInstance().SwitchToScreen(CanvasType.HomeWeaScreen);
            }
        };

    }

    public void PopulateSaveData(SaveData projectData)
    {
        List<WEAModel> placedWeaModels = weaMapHandler.GetCurrentlyPlacedWeaModels();
        Debug.Log("File to save: " + placedWeaModels.Count + " wurden platziert ...");

        foreach(WEAModel model in placedWeaModels)
        {
            Debug.Log("SaveDebug: WEA wird gespeichert..., geopos:" + model.position);
            // y pos when saving missing (always 0)
            Debug.Log("SaveDebug: WEA wird gespeichert. WorldPos: " + weaMapHandler.TestGetWorldPos(model.position));
        }

        projectData.userProject.title = projectName.text;
        projectData.userProject.projectFilename = projectName.text + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) + ".margo";
        projectData.userProject.savedAt = DateTime.Now.ToString("g", CultureInfo.CreateSpecificCulture("de-DE")); ;
        projectData.userProject.placedWeaModels = placedWeaModels;

        // TODO implement thumbnail

        Debug.Log("Projekt mit dem Titel '" + projectData.userProject.title + "' wird mit dem Dateinamen '" + projectData.userProject.projectFilename + "' gespeichert");
    }

    public void LoadFromSaveData(SaveData projectData)
    {
        // No action here, this method is not called but required for the interface ISaveable
    }
}
