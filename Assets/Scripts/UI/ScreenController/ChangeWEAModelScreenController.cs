using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ChangeWEAModelScreenController : MonoBehaviour, IScreenController
{
    [SerializeField] UIDocument uIDocument;
    [SerializeField] RawArCameraOutputController rawArCameraOutputController;

    private Button saveButton;
    private Label lblDrehung, lblSliderSize;
    private Slider sliderSize, sliderDrehung;

    public void InitController()
    {
        OnEnable();
    }

    private void OnEnable()
    {
        ReferenceGuiElements();
        RegisterOnClickListeners();
    }

    private void ReferenceGuiElements()
    {
        var rootVisualElement = GetComponent<UIDocument>().rootVisualElement;

        saveButton = rootVisualElement.Q<Button>("save-button");
        sliderSize = rootVisualElement.Q<Slider>("sliderSize");
        sliderDrehung = rootVisualElement.Q<Slider>("sliderDrehung");
        lblDrehung = rootVisualElement.Q<Label>("lblDrehung");
        lblSliderSize = rootVisualElement.Q<Label>("lblSliderSize");
    }


    private void RegisterOnClickListeners()
    {
        sliderDrehung.RegisterValueChangedCallback(x => { DrehungSliderChanged(); });
        sliderSize.RegisterValueChangedCallback(x => { SizeSliderChanged(); });

        saveButton.clicked += () =>
        {
            CanvasManager.GetInstance().SwitchBackToLastScreen();
        };
    }

    private void SizeSliderChanged()
    {
        double roundedScaleSliderValue = System.Math.Round((sliderSize.value / 100), 1);
        rawArCameraOutputController.ChangeScaleWeaModel((float)roundedScaleSliderValue);
        lblSliderSize.text = "Faktor: " + roundedScaleSliderValue;
    }


    private void DrehungSliderChanged()
    {
        rawArCameraOutputController.RotateLastClickedWeaModel((int)sliderDrehung.value);
        lblDrehung.text = (int)sliderDrehung.value + "�";
    }

}

