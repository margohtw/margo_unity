using System;
using System.Collections.Generic;
using Mapbox.Utils;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UIElements;

/*
 * This controller is responsible for MainArScreen, it receives the user input and calls further logic 
 */
public class MainArScreenController : MonoBehaviour, IScreenController
{
    private MainArUIManager mainArUIManager;

    [SerializeField] private Camera arCamera;
    [SerializeField] GameObject rawArCameraOuput;
    [SerializeField] private GameObject arModelsHolder;
    
    [SerializeField] ArInteractionWeaModels arInteractionWeaModels;
    [SerializeField] WeaMapHandler weaMapHandler;
    
    [SerializeField] MapViewSizeController mapSizeController;
    
    [SerializeField] ProjectsListViewCreator projectsListViewCreator;
    [SerializeField] HomeWEAScreenController homeScreenController;

    private bool isCalibrated = false;

    private float userElevation;

    public void InitController()
    {
       OnEnable();
    }

    void OnEnable()
    {
        //InitLoadedUserProject();
        arInteractionWeaModels.DestroyAllWeaModels();
        mapSizeController.CloseMapCam();
        mainArUIManager = GetComponent<MainArUIManager>();
        mainArUIManager.Initialize();
    }

    private void LoadWEAsFromUserProject()
    {
        UserProject currentUserProject = projectsListViewCreator.GetCurrentUserProject();
        // first check if it is necessary (otherwise its a new project)
        if (currentUserProject != null)
        {
            Debug.Log("Userproject wird geladen: " + currentUserProject.ToString());

            weaMapHandler.PlaceWeaModels(currentUserProject.placedWeaModels);
        }
    }


    public void EnterCalibMode()
    {
        

        CalibLogger.GetInstance().addText("Enter Calib Mode Init Pose: " + arModelsHolder.transform.position + " " + arModelsHolder.transform.eulerAngles + " arcamera: " + arCamera.transform.position + " " + arCamera.transform.eulerAngles);
        CalibLogger.GetInstance().addText("UnityToWorldYOffset: " + WorldToUnitySpaceConverter.GetInstance().GetUnityToWorldYOffset());
        double[] utmPos = WorldToUnitySpaceConverter.GetInstance().UnitySpaceToUTM(arModelsHolder.transform.position);
        CalibLogger.GetInstance().addText("UTM Pos: " + utmPos[0] + " " + utmPos[1]);

        if (!isCalibrated) //calib-mode entered for first time
        {
            CalibLogger.GetInstance().addText("first calib");
            InitializeUserElevation();
        }
        else //calib mode is re-entered
        {
            CalibLogger.GetInstance().addText("re-enter calib mode");
        }
        
    }

    public void EndCalibration()
    {
    
        
        CalibLogger.GetInstance().addText("end pose: " + arModelsHolder.transform.position + " " + arModelsHolder.transform.eulerAngles + " arcamera: " + arCamera.transform.position + " " + arCamera.transform.eulerAngles);
        double[] utmPos = WorldToUnitySpaceConverter.GetInstance().UnitySpaceToUTM(arModelsHolder.transform.position);
        CalibLogger.GetInstance().addText("UTM Pos: " + utmPos[0] + " " + utmPos[1]);
        CalibLogger.GetInstance().endCalibLogger();
        CalibModelHandler.GetInstance().EndCalibration();
        if (!isCalibrated)
        {
            LoadWEAsFromUserProject();
        }
        isCalibrated = true;

    }
    
    public void PlaceWEAModel()
    {
        arInteractionWeaModels.InstantiateWea();
    }

    public void ToggleMapVisibility()
    {
        mapSizeController.ToggleMapVisibility();
        //ChangeVisibilityCameraZoomSlider();
    }

    private void ChangeVisibilityCameraZoomSlider()
    {
        if (mapSizeController.IsMapOpen())
        {
            mainArUIManager.HideCameraZoomSlider();
        }
        else
        {
            mainArUIManager.ShowCameraZoomSlider();
        }
    }

    public void OnDistanceSliderChanged(ChangeEvent<float> value)
    {
        CalibModelHandler.GetInstance().SetMatDistance(value.newValue);
    }

    public void OnCameraZoomSliderChanged(ChangeEvent<float> value)
    {
        rawArCameraOuput.transform.localScale = new Vector3(1, 1, 1) * value.newValue;
    }
    
    public void InitializeUserElevation()
    { 
        CalibModelHandler.GetInstance().EnableDgmForElevation();
        SetUserElevation();
    }

    private void SetUserElevation()
    {
        userElevation = -(ElevationRaycaster.GetInstance().GetElevation(arCamera.transform.position) + 1.6f);
        arModelsHolder.transform.position += new Vector3(0f, userElevation, 0f);
    }

    public void OpenVideo()
    {
        CanvasManager.GetInstance().SwitchToScreen(CanvasType.VideoRecord);
    }

    public void OpenLoad()
    {
        CanvasManager.GetInstance().SwitchToScreen(CanvasType.LoadProject);
    }

    public void OpenSave()
    {
        CanvasManager.GetInstance().SwitchToScreen(CanvasType.SaveProject);
        rawArCameraOuput.SetActive(true); // we need to reactivate rawArCameraOutput game object, AR screen stops working otherwise
    }

    public void OpenPOI()
    {
        //CanvasManager.GetInstance().SwitchToScreen(CanvasType.XXX);
    }

    public bool isMapBasedCalibrationEnabled()
    {
        return homeScreenController.isMapBasedCalibrationEnabled();
    }

    public float GetInitUserElevation()
    {
        return userElevation;
    }
}