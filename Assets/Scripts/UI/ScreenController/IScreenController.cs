using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Every ScreenController needs to implement this interface, so that switching between screen modes (landscape / portrait) works correctly
 * because gui elements and event listeners need to be re-registered (different uxml). Implement InitController() method and call behaviour to initialize 
 * the UI like registering gui elements and onClickListeners (typically the things which are done in onEnable())
 * The method is called in script ChangeToLandscape.cs when changing the screen mode to landscape/portrait
 */
public interface IScreenController
{
    void InitController();
}