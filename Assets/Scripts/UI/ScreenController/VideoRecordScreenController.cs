using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class VideoRecordScreenController : MonoBehaviour, IScreenController
{
    [SerializeField] UIDocument uIDocument;
    [SerializeField] AndroidUtils androidVideoRecordUtils;

    private Button recordButton;
    private Button cancelRecordButton;
    private Button quitScreenButton;
    private Label lblSecondsCurrentRecoding;

    private bool isRecording = false;
    private float secondsCurrentRecording, minuteCurrentRecording = 0;

    public void InitController()
    {
        OnEnable();
    }

    void Update()
    {
        if (isRecording)
        {
            UpdateTimerUI();
        }
    }

    private void UpdateTimerUI()
    {
        secondsCurrentRecording += Time.deltaTime;
        lblSecondsCurrentRecoding.text = GetValueWithLeadingNullIfNeeded((int)minuteCurrentRecording) + ":" + GetValueWithLeadingNullIfNeeded((int)secondsCurrentRecording);
        if (secondsCurrentRecording >= 60)
        {
            minuteCurrentRecording++;
            secondsCurrentRecording %= 60;
        }
    }

    private string GetValueWithLeadingNullIfNeeded(int value)
    {
        if (value < 10)
        {
            return 0 + value.ToString();
        }
        return value.ToString();
    }

    void OnEnable()
    {
        ReferenceGuiElements();
        RegisterOnClickListeners();
    }

    public void ReferenceGuiElements()
    {
        var rootVisualElement = GetComponent<UIDocument>().rootVisualElement;
        recordButton = rootVisualElement.Q<Button>("record-button");
        cancelRecordButton = rootVisualElement.Q<Button>("cancel-record-button");
        quitScreenButton = rootVisualElement.Q<Button>("quit-screen-button");
        lblSecondsCurrentRecoding = rootVisualElement.Q<Label>("lbl-recording-seconds");
    }

    public void RegisterOnClickListeners()
    {
        recordButton.clicked += () =>
        {
            StartScreenRecording();
        };

        cancelRecordButton.clicked += () =>
        {
            CancelScreenRecording();
        };

        quitScreenButton.clicked += () =>
        {
            CanvasManager.GetInstance().SwitchToScreen(CanvasType.ArScreen);
        };
    }

    private void StartScreenRecording()
    {
        Debug.Log("Starting video record ...");
        secondsCurrentRecording = 0;
        isRecording = true;
        androidVideoRecordUtils.StartRecording();
        lblSecondsCurrentRecoding.style.visibility = Visibility.Visible;
        recordButton.style.visibility = Visibility.Hidden;
        cancelRecordButton.style.visibility = Visibility.Visible;
    }

    private void CancelScreenRecording()
    {
        Debug.Log("Cancel video record ...");
        secondsCurrentRecording = 0;
        isRecording = false;
        androidVideoRecordUtils.StopRecording();
        lblSecondsCurrentRecoding.style.visibility = Visibility.Hidden;
        recordButton.style.visibility = Visibility.Visible;
        cancelRecordButton.style.visibility = Visibility.Hidden;
    }
 
}
