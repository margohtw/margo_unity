using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

/*
 * This controller is responsible for HomeWEAScreen, it receives the user input and calls further logic 
 */
public class HomeWEAScreenController : MonoBehaviour, IScreenController
{
    [SerializeField] private UIDocument uIDocument;

    private ProjectsListViewCreator projectsListViewCreator;

    private Button btnNew;
    private Button btnSettings;
    private Button btnLoad;


    private Toggle checkOberflaeche, checkGelaende, checkGebaeude, checkOberflaecheRgb, checkGebaeudeRgb;
    private Toggle checkMapPositioning;
    private List<Tuple<TileType, CalibModelTypeDataRow>> tuplesTileTypeDataRow;
    TileType[] types = { TileType.dgm, TileType.dom, TileType.cityModel, TileType.dom_rgb, TileType.cityModel_rgb };
    private CalibSettingsJsonFileHandler calibModelJsonFileHandler;


    private Label lblNoProjectsFound;

    private ListView projectsListView;

    public void InitController()
    {
        OnEnable();
    }


   
    void OnEnable()
    {

        tuplesTileTypeDataRow = new List<Tuple<TileType, CalibModelTypeDataRow>>();
        calibModelJsonFileHandler = GetComponent<CalibSettingsJsonFileHandler>();

        GetComponents();
        ReferenceGuiElements();
        RegisterOnClickListeners();
        RegisterCheckBoxListeners();
        LoadProjects();

        InitDataRowsCalibTypes();
        UpdateCheckBoxValues();
        Update3DCalibModelVisibility();
    }

    private void GetComponents()
    {
        projectsListViewCreator = GetComponent<ProjectsListViewCreator>();
    }

    private void ReferenceGuiElements()
    {
        var rootVisualElement = GetComponent<UIDocument>().rootVisualElement;
        btnSettings = rootVisualElement.Q<Button>("btnSettings");
        btnLoad = rootVisualElement.Q<Button>("LadenButton");
        btnNew = rootVisualElement.Q<Button>("NeuButton");

        lblNoProjectsFound = rootVisualElement.Q<Label>("lblNoProjectsFound");
        projectsListView = rootVisualElement.Q<ListView>();

        checkOberflaeche = rootVisualElement.Q<Toggle>("toggleDOM");
        checkGelaende = rootVisualElement.Q<Toggle>("toggleDGM");
        checkGebaeude = rootVisualElement.Q<Toggle>("toggleCityModel");
        checkOberflaecheRgb = rootVisualElement.Q<Toggle>("toggleDomRGB");
        checkGebaeudeRgb = rootVisualElement.Q<Toggle>("toggleCityModelRGB");

        checkMapPositioning = rootVisualElement.Q<Toggle>("toggleMapPositioning");
    }

    private void RegisterOnClickListeners()
    {

        btnNew.clicked += () =>
        {
            projectsListViewCreator.UnselectProjects();
            SwitchScreen(CanvasType.ArScreen);
        };

        btnSettings.clicked += () =>
        {
            SwitchScreen(CanvasType.CalibTileScreen);
        };

        

        btnLoad.clicked += () =>
        {
            if (projectsListViewCreator.GetCurrentUserProject() != null)
            {
                SwitchScreen(CanvasType.ArScreen);
            }
            else
            {
                // Message -> first select a project item
            }
        };
    }

    private void RegisterCheckBoxListeners()
    {
        checkGelaende.RegisterValueChangedCallback(x =>
        {
            Debug.Log("change checkbox dgm " + x.newValue);
            UnselectAllCheckboxes();
            checkGelaende.SetValueWithoutNotify(true);
            GetDataRow(TileType.dgm).isSelected = true;
            CalibModelHandler.GetInstance().MakeCalibModelsInvisible();
            CalibModelHandler.GetInstance().ChangeCalibModelVisibility(TileType.dgm, true);
        });

        checkOberflaeche.RegisterValueChangedCallback(x =>
        {
            Debug.Log("click checkbox dom " + x.newValue);
            UnselectAllCheckboxes();
            checkOberflaeche.SetValueWithoutNotify(true);
            GetDataRow(TileType.dom).isSelected = true;
            CalibModelHandler.GetInstance().MakeCalibModelsInvisible();
            CalibModelHandler.GetInstance().ChangeCalibModelVisibility(TileType.dom, true);
        });

        checkGebaeude.RegisterValueChangedCallback(x =>
        {
            Debug.Log("change checkbox citymodel " + x.newValue);
            UnselectAllCheckboxes();
            checkGebaeude.SetValueWithoutNotify(true);
            GetDataRow(TileType.cityModel).isSelected = true;
            CalibModelHandler.GetInstance().MakeCalibModelsInvisible();
            CalibModelHandler.GetInstance().ChangeCalibModelVisibility(TileType.cityModel, true);
        });

        checkGebaeudeRgb.RegisterValueChangedCallback(x =>
        {
            Debug.Log("change checkbox citymodel rgb " + x.newValue);
            UnselectAllCheckboxes();
            checkGebaeudeRgb.SetValueWithoutNotify(true);
            GetDataRow(TileType.cityModel_rgb).isSelected = true;
            CalibModelHandler.GetInstance().MakeCalibModelsInvisible();
            CalibModelHandler.GetInstance().ChangeCalibModelVisibility(TileType.cityModel_rgb, true);
        });

        checkOberflaecheRgb.RegisterValueChangedCallback(x =>
        {
            Debug.Log("change dom rgb " + x.newValue);
            UnselectAllCheckboxes();
            checkOberflaecheRgb.SetValueWithoutNotify(true);
            GetDataRow(TileType.dom_rgb).isSelected = true;
            CalibModelHandler.GetInstance().MakeCalibModelsInvisible();
            CalibModelHandler.GetInstance().ChangeCalibModelVisibility(TileType.dom_rgb, true);
        });
    }

    

    private void UnselectAllCheckboxes()
    {
        
        checkGelaende.SetValueWithoutNotify(false);
        checkOberflaeche.SetValueWithoutNotify(false);
        checkGebaeude.SetValueWithoutNotify(false);
        checkOberflaecheRgb.SetValueWithoutNotify(false);
        checkGebaeudeRgb.SetValueWithoutNotify(false);
        GetDataRow(TileType.cityModel).isSelected = false;
        GetDataRow(TileType.cityModel_rgb).isSelected = false;
        GetDataRow(TileType.dgm).isSelected = false;
        GetDataRow(TileType.dom).isSelected = false;
        GetDataRow(TileType.dom_rgb).isSelected = false;
    }

    private void SwitchScreen(CanvasType type)
    {
        switch (type)
        {
            case CanvasType.ArScreen:
                Debug.Log("Klick - Wechsle zu AR-Screen ...");

                //Store calib type visibility settings
                List<CalibModelTypeDataRow> calibModelTypeDataRows = new List<CalibModelTypeDataRow>();
                foreach (var tuple in tuplesTileTypeDataRow)
                {
                    calibModelTypeDataRows.Add(tuple.Item2);
                }
                calibModelJsonFileHandler.WriteCalibSettings(calibModelTypeDataRows, checkMapPositioning.value);
                break;
            case CanvasType.CalibTileScreen:
                Debug.Log("Klick - Wechsle zu Calib-Tile-Selection-Screen ...");
                break;
            default:
                Debug.Log("Klick - Wechsle zu unbekanntem Screen ...");
                break;
        }
        CanvasManager.GetInstance().SwitchToScreen(type);
    }

    private void LoadProjects()
    {
        projectsListViewCreator.UnselectProjects();

        List<UserProject> allProjects = ProjectDataSaver.getAllSavedProjects();
        if (allProjects.Count > 0)
        {
            projectsListViewCreator.RenderProjectsListView(projectsListView, allProjects);
        }
        else
        {
            //TODO add label, currently produces nullpointerexception
            //lblNoProjectsFound.style.visibility = Visibility.Visible;
        }

    }

    private void InitDataRowsCalibTypes()
    {
        CalibSettingsSaveData savedData = calibModelJsonFileHandler.ReadCalibSettings();
        List<CalibModelTypeDataRow> loadedCalibModelTypeDataRows = savedData.calibModelTypeDataList;
        if (loadedCalibModelTypeDataRows.Count == 0)
        {
            InitEmptyDataRows();
        }
        else
        {
            CreateDataRowsFromSaveData(loadedCalibModelTypeDataRows);
        }
        checkMapPositioning.SetValueWithoutNotify(savedData.mapPositioning);
    }

    private void InitEmptyDataRows()
    {
        foreach (var type in types)
        {
            CalibModelTypeDataRow newRow = new CalibModelTypeDataRow();
            switch (type)
            {
                case TileType.dgm:
                case TileType.dom:
                case TileType.cityModel:
                    newRow.isSelected = true;
                    newRow.selectedShaderPos = 0;
                    break;
                case TileType.dom_rgb:
                    newRow.transparency = 0.85f;
                    break;
                case TileType.cityModel_rgb:
                    newRow.isSelected = false;
                    newRow.transparency = 0.85f;
                    break;
            }
            newRow.type = type;
            tuplesTileTypeDataRow.Add(new Tuple<TileType, CalibModelTypeDataRow>(type, newRow));
        }
    }

    private void CreateDataRowsFromSaveData(List<CalibModelTypeDataRow> rows)
    {
        foreach (var row in rows)
        {
            tuplesTileTypeDataRow.Add(new Tuple<TileType, CalibModelTypeDataRow>(row.type, row));
        }
    }

    private void UpdateCheckBoxValues()
    {
        checkGelaende.value = GetDataRow(TileType.dgm).isSelected;
        checkOberflaeche.value = GetDataRow(TileType.dom).isSelected;
        checkGebaeude.value = GetDataRow(TileType.cityModel).isSelected;
        checkOberflaecheRgb.value = GetDataRow(TileType.dom_rgb).isSelected;
        checkGebaeudeRgb.value = GetDataRow(TileType.cityModel_rgb).isSelected;
    }

    private void Update3DCalibModelVisibility()
    {
        CalibModelHandler.GetInstance().ChangeCalibModelVisibility(TileType.dgm, GetDataRow(TileType.dgm).isSelected);
        CalibModelHandler.GetInstance().ChangeCalibModelVisibility(TileType.dom, GetDataRow(TileType.dom).isSelected);
        CalibModelHandler.GetInstance().ChangeCalibModelVisibility(TileType.cityModel, GetDataRow(TileType.cityModel).isSelected);
        CalibModelHandler.GetInstance().ChangeCalibModelVisibility(TileType.dom_rgb, GetDataRow(TileType.dom_rgb).isSelected);
        CalibModelHandler.GetInstance().ChangeCalibModelVisibility(TileType.cityModel_rgb, GetDataRow(TileType.cityModel_rgb).isSelected);
    }

    public bool IsCalibTypeSelected(TileType tileType)
    {
        CalibModelTypeDataRow row = GetDataRow(tileType);
        if (row == null)
        {
            return false;
        }
        return (row.isSelected);
    }

    public CalibModelTypeDataRow GetDataRow(TileType tileType)
    {
        foreach (var tuple in tuplesTileTypeDataRow)
        {
            if (tuple.Item1 == tileType) return tuple.Item2;
        }
        return null;
    }

    public bool isMapBasedCalibrationEnabled()
    {
        return checkMapPositioning.value;
    }
}

[Serializable]
public class CalibModelTypeDataRow
{
    public TileType type;
    public bool isSelected;
    public int selectedShaderPos;
    public float transparency;
}

