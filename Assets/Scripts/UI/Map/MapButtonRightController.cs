using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapButtonRightController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] MapViewSizeController controller;
    [SerializeField] CalibTileHandlerFacade calibTileManager;

    public void OnPointerClick(PointerEventData eventData)
    {
        switch (CanvasManager.GetInstance().GetActiveCanvasType())
        {
            case CanvasType.ArScreen:
            case CanvasType.ChangePVModelScreen:
                controller.ToggleMapFullScreen();
                break;
            case CanvasType.CalibTileScreen:
                calibTileManager.WriteCurrentlySelectedTilesToJson();
                CanvasManager.GetInstance().SwitchBackToLastScreen();
                break;
        }
    }
}
