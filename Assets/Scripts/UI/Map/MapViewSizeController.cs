
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapViewSizeController : MonoBehaviour
{
    private static MapViewSizeController instance;

    [SerializeField] Camera mapCam;
    [SerializeField] private GameObject mapUi;
    [SerializeField] RawImage rawArCameraOutput;
    [SerializeField] ArOutputTextureScaler arOutputTextureScaler;
    [SerializeField] private float fullscreenScaleFactor;
    [SerializeField] private float fullSize;
    [SerializeField] private float halfSize;
    [SerializeField] ChangeToLandscape landscapeChanger;
    [SerializeField] private List<GameObject> uiElements;

    private bool isMapFullscreen = false;
    private bool isMapOpen = false;

    private readonly Rect halfWidth = new Rect(0, 0, 0.5f, 1);
    private readonly Rect halfHeight = new Rect(0, 0, 1, 0.5f);
    private readonly Rect fullHeight = new Rect(0, 0, 1, 1f);
    private readonly Rect gone = new Rect(0, 0, 0, 0);

    private MapInteraction mapInteraction;

    public static MapViewSizeController GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            mapInteraction = GetComponent<MapInteraction>();
            instance = this;
        }
    }

    public void ToggleMapFullScreen()
    {
        if (!isMapOpen) return;

        if (isMapFullscreen)
        {
            SetMapHalfHeight();
        }
        else
        {
            SetMapFullHeight();
        }
    }

    public bool IsMapFullscreen()
    {
        return isMapFullscreen;
    }

    public bool IsMapOpen()
    {
        return isMapOpen;
    }

    public void ToggleMapVisibility()
    {
        if (isMapOpen)
        {
            CloseMapCam();
        }
        else
        {
            OpenMapCam();
        }
        Debug.Log(mapCam.rect);
    }

    public void OpenMapCamFullscreen()
    {

        SetMapFullHeight();
        arOutputTextureScaler.MakeArViewGone();
        isMapOpen = true;
    }

    private void OpenMapCam()
    {
        arOutputTextureScaler.MakeArViewHalfScreen();
        SetMapHalfHeight();
        mapInteraction.RecenterMapOnUserLocation();
        isMapOpen = true;
    }

    public void ShowUIInitialGpsPositioning()
    {
        uiElements[0].SetActive(false);
        uiElements[1].SetActive(false);
        uiElements[2].SetActive(false);
        uiElements[3].SetActive(true);
        uiElements[4].SetActive(true);
        uiElements[5].SetActive(true);
        OpenMapCamFullscreen();
    }

    public void HideUIInitialGpsPositioning()
    {
        uiElements[0].SetActive(true);
        uiElements[1].SetActive(true);
        uiElements[2].SetActive(true);
        uiElements[3].SetActive(false);
        uiElements[4].SetActive(false);
        uiElements[5].SetActive(false);
        CloseMapCam();
    }

    public void CloseMapCam()
    {
        if (isMapOpen)
        {
            arOutputTextureScaler.MakeArViewFullScreen();
        }
        mapCam.rect = gone;
        isMapOpen = false;
        isMapFullscreen = false;
        mapInteraction.ResetAllInputs();
    }

    private void SetMapHalfHeight()
    {
        Debug.Log("SetzeMapHalfHeight...");
        arOutputTextureScaler.MakeArViewHalfScreen();
        mapCam.rect = halfHeight;
        if (landscapeChanger.IsDeviceLandscapeMode())
        {
            mapCam.rect = halfWidth;
        }
        isMapFullscreen = false;
        isMapOpen = true;
        mapCam.orthographicSize = halfSize;
        mapInteraction.AdjustSymbols();
    }

    private void SetMapFullHeight()
    {
        Debug.Log("SetzeMapFullHeight...");
        mapCam.rect = fullHeight;
        arOutputTextureScaler.MakeArViewGone();
        isMapFullscreen = true;
        isMapOpen = true;
        mapCam.orthographicSize = fullSize;
        mapInteraction.AdjustSymbols();
    }

    public void HideMapButtons()
    {
        mapUi.SetActive(false);
    }

    public void ShowMapButtons()
    {
        mapUi.SetActive(true);
    }
}
