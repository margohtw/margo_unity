using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MapButtonLeftController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Sprite inactiveSprite;
    [SerializeField] private Sprite activeSprite;
    [SerializeField] private UserLocationController userLocationController;

    private bool manualGpsPositioning = false;

    public void OnPointerClick(PointerEventData eventData)
    {
        manualGpsPositioning = !manualGpsPositioning;
        ToggleButtonImage();
        ToggleUserLocationButtonDraggable();
    }

    private void ToggleUserLocationButtonDraggable()
    {
        userLocationController.SetManualAdjustment(manualGpsPositioning);
    }

    private void ToggleButtonImage()
    {
        if (manualGpsPositioning)
        {
            GetComponent<Image>().sprite = activeSprite;
        }
        else
        {
            GetComponent<Image>().sprite = inactiveSprite;
        }
    }
}
