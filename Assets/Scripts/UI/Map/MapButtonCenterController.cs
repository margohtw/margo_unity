using Mapbox.Unity.Map;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapButtonCenterController : MonoBehaviour, IPointerClickHandler
{

    [SerializeField] AbstractMap map;
    [SerializeField] UserLocationProvider locationProvider;
    [SerializeField] CalibTileHandlerFacade calibTileManager;
    [SerializeField] private MapInteraction mapInteraction;

    public void OnPointerClick(PointerEventData eventData)
    {
        switch (CanvasManager.GetInstance().GetActiveCanvasType())
        {
            case CanvasType.ArScreen:
                // Center map section to current user location
                mapInteraction.RecenterMapOnUserLocation();
                break;
            case CanvasType.CalibTileScreen:
                calibTileManager.ToggleTileControllerSelectedTiles();
                CanvasManager.GetInstance().SwitchBackToLastScreen();
                break;
        }
    }
}
