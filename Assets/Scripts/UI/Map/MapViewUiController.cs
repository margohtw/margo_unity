using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapViewUiController : MonoBehaviour, IPointerClickHandler
{
    Canvas canvas;
    Rect halfHeight = new Rect(0, 0, 1, 0.5f);
    Rect fullHeight = new Rect(0, 0, 1, 1f);
    Rect gone = new Rect(0, 0, 0, 0);

    private void Awake()
    {
        canvas = gameObject.GetComponentInParent<Canvas>();
        canvas.worldCamera.rect = gone;
    }

    private void SetHalfHeight()
    {
        canvas.worldCamera.rect = halfHeight;
    }

    private void SetFullHeight()
    {
        canvas.worldCamera.rect = fullHeight;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        
    }
}
