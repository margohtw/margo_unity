using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraDepthManager : MonoBehaviour
{
    public int depth;
    void Awake()
    {
        GetComponent<Camera>().depth = depth;
    }
}
