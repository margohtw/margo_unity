using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

/*
 * Enum values contain all screens (canvas) of the app
*/
public enum CanvasType
{
    InitialLoadingScreen,
    ModeSelectionScreen,
    HomeWeaScreen,
    VideoRecord,
    SaveProject,
    LoadProject,
    ArScreen,
    Map,
    CalibTileScreen,
    ChangeWEAModelScreen,
    ChangePVModelScreen
}

/*
 * This class handles switching between different screens in the app
 */
public class CanvasManager : MonoBehaviour
{
    private static CanvasManager _instance;

    [SerializeField] private List<CanvasController> _allAvailableCanvas;

    /*
     * Holds the sequence of the displayed screens in the app 
    */
    private Stack<CanvasController> _canvasAppSequenceStack;

    public static CanvasManager GetInstance()
    {
        return _instance;
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        _canvasAppSequenceStack = new Stack<CanvasController>();

        ShowInitialLoadingScreen();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GetInstance().SwitchBackToLastScreen();
        }
    }

    private void ShowInitialLoadingScreen()
    {
        CanvasController startCanvas = _allAvailableCanvas.Find(x => x.GetCanvasType() == CanvasType.InitialLoadingScreen);
        startCanvas.gameObject.SetActive(true);
    }

    public void InitializeApplication()
    {
        //stop the initial loading screen
        CanvasController initialLoadingScreen = _allAvailableCanvas.Find(x => x.GetCanvasType() == CanvasType.InitialLoadingScreen);
        initialLoadingScreen.gameObject.SetActive(false);

        CanvasController startCanvas = _allAvailableCanvas.Find(x => x.GetCanvasType() == CanvasType.ModeSelectionScreen);
        AppModeManager.GetInstance().SetAppMode(AppMode.wea);
        startCanvas.gameObject.SetActive(true);
        _canvasAppSequenceStack.Push(startCanvas);
    }

    /*
     * Call this method, if you want to switch to another screen, the currently active screen gets disabled
     */
    public void SwitchToScreen(CanvasType canvasType)
    {
        CanvasController newCanvas = _allAvailableCanvas.Find(x => x.GetCanvasType() == canvasType);
        getActiveCanvas().gameObject.SetActive(false);
        newCanvas.gameObject.SetActive(true);
        _canvasAppSequenceStack.Push(newCanvas);
        SwitchEventSystem();
        Debug.Log(getActiveCanvas() + " is active screen now!");
        Debug.Log("Aktuelle ScreenStack-Gr??e:" + _canvasAppSequenceStack.Count());
    }

    private void SwitchEventSystem()
    {
        // We need to switch the event system between native unity ui event system and ui toolkit event system
        // dependent on which screen is currently active
        if (!IsMapScreenActive())
        {
            // disable native unity ui event system
            //GameObject.Find("EventSystem").GetComponent<EventSystem>().enabled = false;
        }
        else
        {
            GameObject.Find("EventSystem").GetComponent<EventSystem>().enabled = true;
        }

    }

    public void SwitchBackToLastScreen()
    {
        if (_canvasAppSequenceStack.Count > 1)
        {
            getActiveCanvas().gameObject.SetActive(false);
            _canvasAppSequenceStack.Pop();
            _canvasAppSequenceStack.Peek().gameObject.SetActive(true);
            Debug.Log(getActiveCanvas() + " is active screen now!");
            Debug.Log("Aktuelle ScreenStack-Gr??e:" + _canvasAppSequenceStack.Count());
        }
    }

    public GameObject GetGameObjectOfActiveCanvas()
    {
        CanvasType activeCanvasType = GetActiveCanvasType();
        CanvasController activeCanvasController = _allAvailableCanvas.Find(x => x.GetCanvasType() == activeCanvasType);
        return activeCanvasController.gameObject;
    }

    /*
     * Returns if the currently displayed screen is the AR-Screen
     */
    public Boolean IsMapScreenActive()
    {
        return (getActiveCanvas().GetCanvasType().Equals(CanvasType.ArScreen) || getActiveCanvas().GetCanvasType().Equals(CanvasType.CalibTileScreen));
    }

    private CanvasController getActiveCanvas()
    {
        return _canvasAppSequenceStack.Peek();
    }

    public CanvasType GetActiveCanvasType()
    {
        return _canvasAppSequenceStack.Peek().GetCanvasType();
    }

    public GameObject GetScreenOfType(CanvasType canvasType)
    {
        return _allAvailableCanvas.Find(x => x.GetCanvasType() == canvasType).gameObject;
    }
}
