using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
* Add this class as a script to each Canvas (Screen), so it will be registered as a new screen and it is possible to 
* switch to it via CanvasManager.SwitchToScreen(). Don't forget to add a new CanvasType in CanvasManager class and reference 
* it in Unity!
*/
public class CanvasController : MonoBehaviour
{

    [SerializeField]
    private CanvasType _canvasType;

    public CanvasType GetCanvasType()
    {
        return _canvasType;
    }
}
