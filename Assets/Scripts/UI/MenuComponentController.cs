using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuComponentController : MonoBehaviour
{
    public int position;
    public float travelTime;
    private bool isMoving;
    private RectTransform rectTransform;
    private Vector3 origin;
    private Vector3 target;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        origin = rectTransform.anchoredPosition;
        float height = rectTransform.rect.height;
        float targetY = origin.y + (height * position);
        target = new Vector3(origin.x, targetY, origin.z);
    }

    public void Hide()
    {
        gameObject.transform.Translate(origin);
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
        //LerpObject();
        StartCoroutine(LerpObject());
    }

    IEnumerator LerpObject()
    {
        if (isMoving) yield break;
        isMoving = true;
        float currentTime = 0;
        float normalizedTime;
        while (currentTime <= travelTime)
        {
            currentTime += Time.deltaTime;
            normalizedTime = currentTime / travelTime;
            rectTransform.anchoredPosition = Vector3.Lerp(origin, target, normalizedTime);
            yield return null;
        }
        isMoving = false;
    }
}
