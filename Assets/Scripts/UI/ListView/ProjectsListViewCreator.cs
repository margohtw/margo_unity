using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

public class ProjectsListViewCreator : MonoBehaviour
{

    private UserProject currentSelectedUserProject;

    public void RenderProjectsListView(ListView projectsListView, List<UserProject> allProjects)
    {
        OrderListBySaveDate(allProjects);

        var listItem = Resources.Load<VisualTreeAsset>("UI/UXMLS/SampleListView");
        Func<VisualElement> makeItem = () => listItem.Instantiate();
        
        Action<VisualElement, int> bindItem = (e, i) =>
        {
            var lblProjectName = e.Q<Label>("lblProjectName");
            lblProjectName.text = allProjects[i].title;

            var lblDate = e.Q<Label>("lblDate");
            lblDate.text = allProjects[i].savedAt;

        };

        projectsListView.makeItem = makeItem;
        projectsListView.bindItem = bindItem;
        projectsListView.itemsSource = allProjects;
        projectsListView.selectionType = SelectionType.Single;

        projectsListView.onSelectionChange += objects =>
        {
            if (projectsListView.selectedItem == null)
            {
                return;
            }
            Debug.Log("ProjectListView: Aktuell selektiertes Projekt: " + projectsListView.selectedItem);
            currentSelectedUserProject = (UserProject)projectsListView.selectedItem;

            // we change the background of the selected container to mark it as selected
            projectsListView.Query<VisualElement>("item-container").ForEach((item) => {
                var lblProjectName = item.Q<Label>("lblProjectName");
                var lblDate = item.Q<Label>("lblDate");

                if (lblProjectName.text.Equals(currentSelectedUserProject.title) && lblDate.text.Equals(currentSelectedUserProject.savedAt))
                {
                    Debug.Log("ProjectListView: Item Grey: " + currentSelectedUserProject.title + " / " + currentSelectedUserProject.savedAt);
                    item.style.backgroundColor = Color.grey;
                }
                else
                {
                    Debug.Log("ProjectListView: Item White: " + currentSelectedUserProject.title);
                    item.style.backgroundColor = new Color(226,226,226);
                 
                }
            }
            );

        };
        // more info: https://forum.unity.com/threads/how-to-make-an-item-in-a-listview-from-a-uxml-template.1100638/
    }



    private void OrderListBySaveDate(List<UserProject> allProjects)
    {
        allProjects.Sort(delegate (UserProject x, UserProject y)
        {
            if (x.savedAt == null && y.savedAt == null) return 0;
            else if (x.savedAt == null) return -1;
            else if (y.savedAt == null) return 1;
            else return x.savedAt.CompareTo(y.savedAt);
        });
    }

    public UserProject GetCurrentUserProject()
    {
        return currentSelectedUserProject;
    }

    public void UnselectProjects()
    {
        currentSelectedUserProject = null;
    }

}
