using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MenuComponentManager : MonoBehaviour
{
    private static MenuComponentManager instance;
    private List<MenuComponentController> controllerList;
    private bool isMenuOpen = false;
    private void Awake()
    {
        //initialize the instance of the singleton
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        controllerList = GetComponentsInChildren<MenuComponentController>().ToList();
    }

    public void ShowAll()
    {
        /*for (int i = 0; i < controllerList.Count; i++)
        {
            while (controllerList[i].GetComponent<RectTransform>().rectTransform.Overlaps(controllerList[i + 1].GetComponent<RectTransform>().rectTransform))
            {
                for (int j = i + 1; j < controllerList.Count; j++)
                {
                    controllerList[j].Move();
                }
            }
        }*/
        controllerList.ForEach(x => x.Show());
    }

    public void HideAll()
    {
        controllerList.ForEach(x => x.Hide());
    }

    public void ToggleMenu()
    {
        if (isMenuOpen)
        {
            isMenuOpen = false;
            HideAll();
        }
        else
        {
            isMenuOpen = true;
            ShowAll();
        }
    }

    private void OnEnable()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
    }

    public static MenuComponentManager GetInstance()
    {
        return instance;
    }
}
