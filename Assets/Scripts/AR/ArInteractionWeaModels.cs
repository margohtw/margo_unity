using System;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using Mapbox.Unity.Map;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using System.Linq;

/*
 * This class handels all interaction with the AR 3D-WEA models;
 * Note, that some logic is moved into RawArCameraOutputController because of click-through bugs
 */

public interface IOnMapSymbolMovedListener
{
    public void OnMapSymbolMoved(GameObject mapSymbol);
}

[RequireComponent(typeof(Camera))]
public class ArInteractionWeaModels : MonoBehaviour, IOnMapSymbolMovedListener
{
    [SerializeField] GameObject weaPrefab;
    [SerializeField] GameObject weaModelHolder;

    [SerializeField] WeaMapHandler weaMapHandler;
    [SerializeField] UserLocationProvider userLocationProvider;
    [SerializeField] private MainArUIManager arUiManager;
    [SerializeField] MainArScreenController arScreenController;

    [SerializeField] private AbstractMap map;

    private ARCameraManager cameraManager;
    private Camera arCamera;

    private String weaModelGameObjectNamePattern = "WeaModel_Prefab(Clone)";

    //first is map symbol, second is instantiated wea model
    private List<Tuple<GameObject, GameObject>> weaTupleList = new List<Tuple<GameObject, GameObject>>();

    void Start()
    {
        if (arCamera == null)
        {
            arCamera = GetComponent<Camera>();
        }
        if (cameraManager == null)
        {
            cameraManager = gameObject.GetComponent<ARCameraManager>();
        }
    }

    public void OnMapSymbolMoved(GameObject mapSymbol)
    {
        foreach (var tuple in weaTupleList)
        {
            if (tuple.Item1 == mapSymbol)
            {
                Vector2d newGeoPosLatLng = map.WorldToGeoPosition(tuple.Item1.transform.position);
                Debug.Log("SaveDebug: MOVED geopos map symbol: " + newGeoPosLatLng);

                // calculate unity world pos for 3d model
                Vector3 newWorldPos = WorldToUnitySpaceConverter.GetInstance().LatLngToUnitySpace(newGeoPosLatLng);
                Debug.Log("SaveDebug: MOVED worldpos map symbol: " + newWorldPos);
                newWorldPos.y = ElevationRaycaster.GetInstance().GetElevation(newWorldPos);
                tuple.Item2.transform.position = newWorldPos;
                return;
            }
        }
    }

    public GameObject InstantiateWea(Vector3 unityWorldPosWeaModel, GameObject mapSymbol)
    {
        //instantiate the 3d model
        GameObject newObject = Instantiate(weaPrefab, weaModelHolder.transform, true);
        //TODO: Fix WEA elevation, e.g. enabling DGM Holder to make elevation raycast work;
        //Quick fix for evaluation: disable Raycast

        float initUserElevation = arScreenController.GetInitUserElevation();
        float elevationDiff = weaModelHolder.transform.parent.position.y + initUserElevation;
        float absDiff = Math.Abs(weaModelHolder.transform.parent.position.y) - Math.Abs(initUserElevation);
        //Debug.Log("TestElevation: unityWorldPosWeaModel" + unityWorldPosWeaModel);
        //Debug.Log("TestElevation: absDiff" + absDiff);

        unityWorldPosWeaModel.y -= absDiff;

        Debug.Log("TestElevation: " + initUserElevation + " " + weaModelHolder.transform.parent.position.y + " " + elevationDiff + " / " + unityWorldPosWeaModel.y + " " + weaModelHolder.transform.position);

        //unityWorldPosWeaModel.y = ElevationRaycaster.GetInstance().GetElevation(unityWorldPosWeaModel);
        //Debug.Log("TestWEA: InstantiateWea - ElevationRaycaster: " + unityWorldPosWeaModel.y);


        newObject.transform.position = unityWorldPosWeaModel;
        

        // add BoxCollider to WEA model so it is clickable
        BoxCollider boxCollider = newObject.AddComponent<BoxCollider>();
        boxCollider.center = new Vector3(0, 100, 0);
        boxCollider.size = new Vector3(50, 200, 80);
        Rigidbody2D gameObjectsRigidBody = newObject.gameObject.AddComponent<Rigidbody2D>();

        weaTupleList.Add(new Tuple<GameObject, GameObject>(mapSymbol, newObject));

        //Vector2d geoPos = map.WorldToGeoPosition(newObject.transform.position);
        Vector2d geoPos = WorldToUnitySpaceConverter.GetInstance().UnitySpaceToLatLng(newObject.transform.position);
        Debug.Log("SaveDebug: New WEA MODEL added, worldpos:" + newObject.transform.position);
        Debug.Log("SaveDebug: Test -> placed MODEL in unity space umgwandelt in geopos:" + geoPos);

        return newObject;
    }

    public GameObject InstantiateWea()
    {
        //instantiate the 3d model in the direction that the camera is looking
        Vector3 unadjustedLookDir = arCamera.transform.eulerAngles;
        Quaternion lookDir = Quaternion.Euler(0.0f, unadjustedLookDir.y, 0.0f);
        Vector3 unityWorldPos = lookDir * Vector3.forward;
        unityWorldPos = Vector3.Normalize(unityWorldPos);
        unityWorldPos *= 500;

        GameObject newObject = Instantiate(weaPrefab, weaModelHolder.transform, true);

        newObject.transform.position = new Vector3(
            unityWorldPos.x,
            ElevationRaycaster.GetInstance().GetElevation(unityWorldPos),
            unityWorldPos.z);

        // add BoxCollider to WEA model so it is clickable
        BoxCollider boxCollider = newObject.AddComponent<BoxCollider>();
        boxCollider.center = new Vector3(0, 100, 0);
        boxCollider.size = new Vector3(50, 200, 80);
        Rigidbody2D gameObjectsRigidBody = newObject.gameObject.AddComponent<Rigidbody2D>();

        // create a map symbol
        GameObject mapSymbol = weaMapHandler.AddMapSymbol(unityWorldPos);

        weaTupleList.Add(new Tuple<GameObject, GameObject>(mapSymbol, newObject));

        Vector2d geoPos = WorldToUnitySpaceConverter.GetInstance().UnitySpaceToLatLng(newObject.transform.position);
        Debug.Log("SaveDebug: New WEA Model added, worldpos:" + newObject.transform.position);
        Debug.Log("SaveDebug: New WEA Model added, geopos:" + geoPos);

        return newObject;
    }

    public void DestroyAllWeaModels()
    {
        var weaModels = Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name == weaModelGameObjectNamePattern);

        foreach (GameObject weaModel in weaModels)
        {
            Destroy(weaModel);
        }
    }

    public List<GameObject> GetAllWeaModels()
    {
        List<GameObject> weaModelList = new List<GameObject>();
        var weaModels =  Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name == weaModelGameObjectNamePattern);
        foreach (GameObject weaModel in weaModels)
        {
            weaModelList.Add(weaModel);
        }
        return weaModelList;
    }

    public List<Tuple<GameObject, GameObject>> GetWeaSymbolTupleList()
    {
        return weaTupleList;
    }

}
