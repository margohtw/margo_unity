using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.UI;

/*
* This class is used for scaling the AR camera output based on the screen resolution of the device.
* This is necessary because of the work-around of the implemented zoom functionality.
* The AR camera output is rendered into a RawImage game object with a RenderTexture.
* We need to scale this AR render texture resolution based on the screen aspect ratio or when changing the map mode (fullscreen / half screen).
* Through the manipulation of rendertexture properties we can divide the screen and switch between map/ar mode.
*/
public class ArOutputTextureScaler : MonoBehaviour
{
    [SerializeField] RenderTexture arCameraOutputTexture;
    [SerializeField] RawImage rawArCameraOutput;
    [SerializeField] Camera arCamera;
    [SerializeField] ChangeToLandscape landscapeChanger;
    [SerializeField] Canvas arCanvas;

    private int previousHeight;
    private int previousWidth;

    void Start()
    {
        previousHeight = Screen.height;
        previousWidth = Screen.width;
    }

    void Update()
    {
        if (Screen.height != previousHeight || Screen.width != previousWidth)
        {
            AdjustTextureRendererInitial();
            previousHeight = Screen.height;
            previousWidth = Screen.width;
        }
    }

    /**
     * Called whenever the screen resolution changes and once at app start
     */
    public void AdjustTextureRendererInitial()
    {
        Debug.Log("Adjust RenderTexture resolution: " + Screen.height + " / " + Screen.width);
        arCameraOutputTexture.Release();
        arCameraOutputTexture = new RenderTexture(Screen.width, Screen.height, 24, GraphicsFormat.R8G8B8A8_UNorm);
        arCameraOutputTexture.width = Screen.width;
        arCameraOutputTexture.height = Screen.height;
        arCameraOutputTexture.Create();
        arCamera.targetTexture = arCameraOutputTexture;
        rawArCameraOutput.texture = arCameraOutputTexture;
    }

    public void MakeArViewHalfScreen()
    {
        Debug.Log("Halbiere Screen (RawArCameraOutput-Image) - MAP / AR ...");
        if (landscapeChanger.IsDeviceLandscapeMode())
        {
            MakeArHalfScreenLandscape();
        }
        else
        {
            MakeArHalfScreenPortrait();
        }
        ResetRawImageScale();
    }

    private void MakeArHalfScreenLandscape()
    {
        arCanvas.GetComponent<CanvasScaler>().referenceResolution = new Vector2(2400f, 1080f);
        rawArCameraOutput.rectTransform.anchorMin = new Vector2(0.5f, 0);
        rawArCameraOutput.rectTransform.anchorMax = new Vector2(0.5f, 1);

        rawArCameraOutput.rectTransform.offsetMin = Vector2.zero;
        rawArCameraOutput.rectTransform.offsetMax = Vector2.zero;

        float newXPos = 1200;
        float newWidth = 2400;
        rawArCameraOutput.rectTransform.anchoredPosition = new Vector2(newXPos, rawArCameraOutput.rectTransform.anchoredPosition.y);
        rawArCameraOutput.rectTransform.sizeDelta = new Vector2(newWidth, rawArCameraOutput.rectTransform.sizeDelta.y);

        rawArCameraOutput.uvRect = new Rect(0.5f, 0, 1, 1);

    }

    private  void MakeArHalfScreenPortrait()
    {
        float newHeight = 1200;
        float newYPos = -600;
        rawArCameraOutput.uvRect = new Rect(0, 0.5f, 1, 0.5f);
        rawArCameraOutput.rectTransform.sizeDelta = new Vector2(rawArCameraOutput.rectTransform.sizeDelta.x, newHeight);
        rawArCameraOutput.rectTransform.anchoredPosition = new Vector2(rawArCameraOutput.rectTransform.anchoredPosition.x, newYPos);
    }


    /*
     * We need to reset the scale when changing to map mode
     */
    private void ResetRawImageScale()
    {
        rawArCameraOutput.transform.localScale = new Vector3(1, 1, 1);
    }


    public void MakeArViewFullScreen()
    {
        Debug.Log("Zeige Full-AR-Screen ...");
        if (landscapeChanger.IsDeviceLandscapeMode())
        {

            MakeArFullScreenLandscape();
        }
        else
        {
            MakeArFullScreenPortrait();
        }

        ResetRawImageScale();
    }

    private void MakeArFullScreenLandscape()
    {
        arCanvas.GetComponent<CanvasScaler>().referenceResolution = new Vector2(1080f, 2400f);

        rawArCameraOutput.rectTransform.anchorMin = new Vector2(0, 1);
        rawArCameraOutput.rectTransform.anchorMax = new Vector2(1, 1);


        float newHeight = 2400;
        float newYPos = -1200;
        rawArCameraOutput.uvRect = new Rect(0, 0, 1, 1);
        rawArCameraOutput.rectTransform.sizeDelta = new Vector2(rawArCameraOutput.rectTransform.sizeDelta.x, newHeight);
        rawArCameraOutput.rectTransform.anchoredPosition = new Vector2(rawArCameraOutput.rectTransform.anchoredPosition.x, newYPos);

        rawArCameraOutput.rectTransform.offsetMin = new Vector2(0, rawArCameraOutput.rectTransform.offsetMin.y);
        rawArCameraOutput.rectTransform.offsetMax = new Vector2(0, rawArCameraOutput.rectTransform.offsetMax.y);

        rawArCameraOutput.uvRect = new Rect(0, 0, 1, 1);
    }

    private void MakeArFullScreenPortrait()
    {
        float newHeight = 2400;
        float newYPos = -1200;
        rawArCameraOutput.uvRect = new Rect(0, 0, 1, 1);
        rawArCameraOutput.rectTransform.sizeDelta = new Vector2(rawArCameraOutput.rectTransform.sizeDelta.x, newHeight);
        rawArCameraOutput.rectTransform.anchoredPosition = new Vector2(rawArCameraOutput.rectTransform.anchoredPosition.x, newYPos);
    }

    public void MakeArViewGone()
    {
        Debug.Log("Hide AR Screen ...");
        float newHeight = 0;
        float newYPos = 0;
        rawArCameraOutput.rectTransform.sizeDelta = new Vector2(rawArCameraOutput.rectTransform.sizeDelta.x, newHeight);
        rawArCameraOutput.rectTransform.anchoredPosition = new Vector2(rawArCameraOutput.rectTransform.anchoredPosition.x, newYPos);
        if (landscapeChanger.IsDeviceLandscapeMode())
        {
            // adjust xPos only in landscape mode
            float newXPos = 2400;
            rawArCameraOutput.rectTransform.anchoredPosition = new Vector2(newXPos, rawArCameraOutput.rectTransform.anchoredPosition.y);
        }
    }

}
