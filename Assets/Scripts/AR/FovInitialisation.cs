using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class FovInitialisation : MonoBehaviour
{
    public bool hasFOV = false;
    
    private float hFOV, vFOV;

    public float HFOV { get => hFOV; set => hFOV = value; }
    public float VFOV { get => vFOV; set => vFOV = value; }

    public void InitFov(ARCameraManager cameraManager, Camera arCamera)
    {
        if (cameraManager.subsystem == null) return;

        if (hasFOV)
        {
            return;
        }
        
        Input.gyro.enabled = true;
        XRCameraParams cameraParams = new XRCameraParams
        {
            zNear = arCamera.nearClipPlane,
            zFar = arCamera.farClipPlane,
            screenWidth = Screen.width,
            screenHeight = Screen.height,
            screenOrientation = Screen.orientation
        };

        XRCameraFrame cameraFrame;

        Debug.Log("Init FOV " + Application.platform);

        switch (Application.platform)
        {
            case (RuntimePlatform.Android):
                if (cameraManager.subsystem.TryGetLatestFrame(cameraParams, out cameraFrame))
                {
                    float t = cameraFrame.projectionMatrix.m11;
                    vFOV = Mathf.Atan(1.0f / t) * 2.0f * Mathf.Rad2Deg;
                    float aspectRatio = (float)arCamera.pixelHeight / arCamera.pixelWidth;
                    hFOV = Camera.VerticalToHorizontalFieldOfView(vFOV, 1.0f / aspectRatio);
                    Debug.Log("Calib Camera FOV: " + arCamera.aspect + " " + vFOV + " " + hFOV + " " + ARSession.state);
                    hasFOV = true;
                }
                break;
            case (RuntimePlatform.OSXEditor):
                if (cameraManager.subsystem.TryGetLatestFrame(cameraParams, out cameraFrame))
                {
                    float t = cameraFrame.projectionMatrix.m11;
                    vFOV = Mathf.Atan(1.0f / t) * 2.0f * Mathf.Rad2Deg;
                    float aspectRatio = (float)arCamera.pixelHeight / arCamera.pixelWidth;
                    hFOV = Camera.VerticalToHorizontalFieldOfView(vFOV, 1.0f / aspectRatio);
                    Debug.Log("Calib Camera FOV: " + arCamera.aspect + " " + vFOV + " " + hFOV + " " + ARSession.state);
                    hasFOV = true;
                }
                else
                {
                    vFOV = arCamera.fieldOfView;
                    hFOV = Camera.VerticalToHorizontalFieldOfView(vFOV, 1.0f / arCamera.aspect);
                    Debug.Log("Calib Camera FOV: " + arCamera.aspect + " " + vFOV + " " + hFOV + " " + ARSession.state);
                    hasFOV = true;
                }
                break;
            default:
                vFOV = arCamera.fieldOfView;
                hFOV = Camera.VerticalToHorizontalFieldOfView(vFOV, 1.0f / arCamera.aspect);
                Debug.Log("Calib Camera FOV: " + arCamera.aspect + " " + vFOV + " " + hFOV + " " + ARSession.state);
                hasFOV = true;
                break;

        }

    }

    
}
