using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Mapbox.Map;


/*
 * This class controls all interaction of the user with the AR screen
 * Is attatched to the AR Camera (has access to position and rotation of the user via transform component)
*/

[RequireComponent(typeof(Camera))]
public class ArInteractionCalibModels : MonoBehaviour
{
    [SerializeField] GameObject dgmPrefab;

    [SerializeField] GameObject arModelsHolder;
    //[SerializeField] private GameObject weaModelHolder;
    //[SerializeField] private GameObject pvModelHolder;
    [SerializeField] MainArUIManager uiManager;
     
    private FovInitialisation fovInitialisation;

    private ARCameraManager cameraManager;
    private Camera arCamera;

 
    void Start() 
    {
        //Debug.Log("Start CalibInteraction");
        fovInitialisation = GetComponent<FovInitialisation>();

        if (arCamera == null)
        {
            arCamera = GetComponent<Camera>();
        }
        if (cameraManager == null)
        {
            cameraManager = gameObject.GetComponent<ARCameraManager>();
        }
        //fovInitialisation.InitFov(cameraManager, arCamera);
    }



    void Update()
    {
        if (cameraManager != null)
        {
            fovInitialisation.InitFov(cameraManager, arCamera);
        }
        CheckARCalibModelGestures();
    }


    private void CheckARCalibModelGestures()
    {

        if (!uiManager.IsCalibrationModeActive())
        {
            return;
        }

        switch (Application.platform)
        {
            case RuntimePlatform.Android:
            case RuntimePlatform.IPhonePlayer:
                if (Input.touchCount == 1)
                {

                    if (Input.GetTouch(0).tapCount == 2 && Input.GetTouch(0).phase == TouchPhase.Began)
                    {
                        ProcessDoubleTap();
                        return;
                    }
                    else
                    {
                        DragCalibModel();
                    }

                }
                else if (Input.touchCount == 2)
                {
                    ZoomCalibModel();
                }

                break;
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
                if (Input.GetMouseButton(0))
                {

                    /*if (Input.GetTouch(0).tapCount == 2 && Input.GetTouch(0).phase == TouchPhase.Began)
                    {
                        ProcessDoubleTap();
                        return;
                    }
                    else
                    {
                        DragCalibModel();
                    }*/

                }
                else if (Input.GetKey(KeyCode.Z))
                {
                    CustomWindowsZoom();
                }

                break;

        }

        
    }

    private void ProcessDoubleTap()
    {
        Debug.Log("DoubleTap");
        straightenTerrain();

    }

    private void CustomWindowsZoom()
    {
        Debug.Log("ZOOM IN ");
        arModelsHolder.transform.Translate(arCamera.transform.rotation * Vector3.forward, Space.World);
    }

    private void DragCalibModel()
    {
        Touch touch = Input.GetTouch(0);
        Debug.Log("Calib: " + touch.phase + " " + fovInitialisation.HFOV);

        if (touch.phase == TouchPhase.Moved)
        {
           
            Transform transform = GetComponent<Transform>();
            float dAngleH = fovInitialisation.HFOV / (Screen.width / touch.deltaPosition.x);
            dAngleH *= 0.5f;

            float dAngleV = fovInitialisation.VFOV / (Screen.height / touch.deltaPosition.y);
            Vector3 rotatedXAxis = Quaternion.AngleAxis(transform.eulerAngles.y, Vector3.up) * Vector3.right;
            dAngleV *= 0.5f;
            
            arModelsHolder.transform.RotateAround(transform.position, Vector3.up, dAngleH);
            arModelsHolder.transform.RotateAround(transform.position, rotatedXAxis, -dAngleV);

            //Debug.Log("Calib: " + touch.deltaPosition + " " + dAngleH + " " + dAngleV + " " + fovInitialisation.HFOV + " " + fovInitialisation.VFOV);

            CalibLogger.GetInstance().addText("DRAG: " + dAngleH + " " + dAngleV + " " + arModelsHolder.transform.eulerAngles.ToString());
            straightenTerrain();
        }
    }

    private void straightenTerrain()
    {
        Transform transform = GetComponent<Transform>();

        //calculate how far ground plane looks rotated in current camera view (around look at axis)
        Vector3 rotatedXAxis = Quaternion.AngleAxis(transform.eulerAngles.y, Vector3.up) * Vector3.right;
        Vector3 pt1 = Vector3.ProjectOnPlane(transform.position, arModelsHolder.transform.up);
        Vector3 pt2 = Vector3.ProjectOnPlane(transform.position + rotatedXAxis, arModelsHolder.transform.up);
        Vector3 vectorOnGroundPlane = pt2 - pt1;


        float rotationAroundLookAt = Vector3.SignedAngle(vectorOnGroundPlane, rotatedXAxis, transform.forward);
        float rotationAroundLookAt2 = Vector3.SignedAngle(vectorOnGroundPlane, rotatedXAxis, Vector3.up);
        float rotationAroundLookAt3 = Vector3.SignedAngle(vectorOnGroundPlane, rotatedXAxis, Vector3.forward);

        Debug.Log("Calib Transform: " + rotationAroundLookAt + " " + rotationAroundLookAt2 + " " + rotationAroundLookAt3);

        //un-rotate calibmodels
        arModelsHolder.transform.RotateAround(transform.position, transform.forward, rotationAroundLookAt);

    }

    private void ZoomCalibModel()
    {
        if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved)
        {
            float threshold = 5.0f;

            //Debug.Log("two fingers moving");
            Vector2 dPos0 = Input.touches[0].deltaPosition;
            Vector2 dPos1 = Input.touches[1].deltaPosition;
            //do not continue if movement is to small
            if (dPos0.magnitude < threshold && dPos1.magnitude < threshold)
            {
                return;
            }

            //check if two finger moves parallel to each other
            float dDotProduct = Vector2.Dot(dPos0.normalized, dPos1.normalized);
            if (dDotProduct > 0.8)
            {
                //yes, two fingers move parallel to each other
                //check if movement is towards top or bottom
                float dYNorm = (dPos0.normalized.y + dPos1.normalized.y) / 2f;
                if (dYNorm > 0.95f)
                {
                    //two fingers moving up
                    //Debug.Log("Elevation UP");

                    arModelsHolder.transform.Translate(new Vector3(0, 0.2f, 0), Space.World);
                    CalibLogger.GetInstance().addText("ELEVATION UP: " + arModelsHolder.transform.position);
                }
                else if (dYNorm < -0.95f)
                {
                    //two fingers moving down
                    //Debug.Log("Elevation DOWN");
                    arModelsHolder.transform.Translate(new Vector3(0, -0.2f, 0), Space.World);
                    CalibLogger.GetInstance().addText("ELEVATION DOWN: " + arModelsHolder.transform.position);
                }
                return;
            }

            //if map based positioning is enabled, do not enabled zooming of calib models
            if(uiManager.isMapBasedCalibrationEnabled())
            {
                return;
            }

            //else: continue with Zoom detection

            Vector2 refVec = (Input.touches[0].position - Input.touches[1].position).normalized;

            Vector3 camLookDir = transform.rotation * Vector3.forward;
            camLookDir.Normalize();
            camLookDir.y = 0; //only translate along x-z-plane!
            camLookDir *= 1.0f;

            if (Vector2.Dot(dPos0.normalized, refVec) < 0 &&
                Vector2.Dot(dPos1.normalized, refVec) > 0)
            {

                //theyre moving towards each other!
                Debug.Log("ZOOM IN " + dPos0.magnitude + " " + dPos1.magnitude);
                arModelsHolder.transform.Translate(camLookDir, Space.World);
                CalibLogger.GetInstance().addText("ZOOM IN: " + arModelsHolder.transform.position);
            }
            else
            {
                //theyre moving away from each other!
                Debug.Log("ZOOM OUT " + dPos0.magnitude + " " + dPos1.magnitude);
                arModelsHolder.transform.Translate(-camLookDir, Space.World);
                CalibLogger.GetInstance().addText("ZOOM OUT: " + arModelsHolder.transform.position);
            }
            //gameObject.transform.Translate(deltaVector);
        }
        
    }

    


}
