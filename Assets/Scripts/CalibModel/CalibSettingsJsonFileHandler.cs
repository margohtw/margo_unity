using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/*
 * The calib settings (model visibility, map positioning) is stored in json files.
 */
public class CalibSettingsJsonFileHandler : MonoBehaviour
{

    private readonly string SAVE_FILE_PATH = "/CalibModelData/selectedSettings";

    public CalibSettingsSaveData ReadCalibSettings()
    {
        CalibSettingsSaveData saveDataFile = new CalibSettingsSaveData();

        if (File.Exists(Application.persistentDataPath + SAVE_FILE_PATH))
        {
            string saveFileContents = File.ReadAllText(Application.persistentDataPath + SAVE_FILE_PATH);
            saveDataFile = JsonUtility.FromJson<CalibSettingsSaveData>(saveFileContents);
        }
        Debug.Log("calib settings read successfull! Data: " + saveDataFile.ToString());
        return saveDataFile;
    }

    public void WriteCalibSettings(List<CalibModelTypeDataRow> calibModelTypeDataList, bool isMapPositioningEnabled)
    {
        CreateDirectoryIfNotExists();
        CalibSettingsSaveData saveDataFile = new CalibSettingsSaveData();
        saveDataFile.calibModelTypeDataList = calibModelTypeDataList;
        saveDataFile.mapPositioning = isMapPositioningEnabled;
        string json = JsonUtility.ToJson(saveDataFile);
        File.WriteAllText(Application.persistentDataPath + SAVE_FILE_PATH, json);
        Debug.Log("calib settings write successfull! Data: " + json.ToString());
        CalibLogger.GetInstance().addText("Calib-Settings: " + json.ToString());
    }

    private void CreateDirectoryIfNotExists()
    {
        FileInfo file = new System.IO.FileInfo(Application.persistentDataPath + SAVE_FILE_PATH);
        file.Directory.Create();
    }
}


[Serializable]
public class CalibSettingsSaveData
{
    public List<CalibModelTypeDataRow> calibModelTypeDataList = new List<CalibModelTypeDataRow>();
    public bool mapPositioning;

}
