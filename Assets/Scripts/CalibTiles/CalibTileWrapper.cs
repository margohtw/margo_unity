using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalibTileWrapper : MonoBehaviour
{
    //contains info from the json
    private CalibTile calibTile;
    //map symbol
    private TileController tileController;
    //map game object
    private GameObject tileGameObject;

    private int utmZone;

    private double[][] utmCoords = new double[4][];
    private double[][] wgsCoords = new double[4][];

    private TileType calibTileType;
    private bool isSelected = false;

    public static CalibTileWrapper CreateCalibTileWrapper(GameObject go, CalibTile tile, TileType type)
    {
        CalibTileWrapper wrapper = go.AddComponent<CalibTileWrapper>();
        wrapper.calibTile = tile;
        wrapper.calibTileType = type;
        wrapper.CalculateUtmCoords();
        wrapper.CalculateWgsCoords();
        return wrapper;
    }

    private void CalculateUtmCoords()
    {
        utmCoords[0] = new double[] { calibTile.x_sw, calibTile.y_sw };
        utmCoords[1] = new double[] { calibTile.x_sw + calibTile.size_x, calibTile.y_sw };
        utmCoords[2] = new double[] { calibTile.x_sw, calibTile.y_sw + calibTile.size_y };
        utmCoords[3] = new double[] { calibTile.x_sw + calibTile.size_x, calibTile.y_sw +calibTile.size_y };
        if (calibTile.epsg == 25833) utmZone = 33;
        if (calibTile.epsg == 25832) utmZone = 32;
    }

    private void CalculateWgsCoords()
    {
        LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
        for (int i = 0; i < 4; i++)
        {
            double x = utmCoords[i][0];
            double y = utmCoords[i][1];

            LatLngUTMConverter.LatLng latlng = converter.convertUtmToLatLng(x, y, utmZone, "N");

            wgsCoords[i] = new [] { latlng.Lat, latlng.Lng };
        }
    }

    public double[][] GetWgsCoordinates()
    {
        // position [0][] is the SW corner!
        // [][0] is lat!
        // [][1] is lng!
        return wgsCoords;
    }

    public double[][] GetUtmCoordinates()
    {
        // position [0][] is the SW corner!
        // [][0] is easting!
        // [][1] is northing!
        return utmCoords;
    }

    public double GetOffsetX()
    {
        return calibTile.offset_x;
    }

    public double GetOffsetY()
    {
        return calibTile.offset_y;
    }

    public TileType GetCalibTileType()
    {
        return calibTileType;
    }

    public void SetTileGameObject(GameObject go)
    {
        tileGameObject = go;
        tileController = tileGameObject.GetComponent<TileController>();
        tileController.SetWrapper(this);
    }

    public GameObject GetTileGameObject()
    {
        return tileGameObject;
    }

    public TileController GetTileController()
    {
        return tileController;
    }

    public void SetSelected (bool selected)
    {
        isSelected = selected;
    } 

    public bool IsSelected()
    {
        return isSelected;
    }

    public string GetFilename()
    {
        return calibTile.filename;
    }

    public CalibTile GetCalibTile()
    {
        return calibTile;
    }
}
