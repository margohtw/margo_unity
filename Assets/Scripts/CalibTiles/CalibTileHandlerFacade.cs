using System;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Mapbox.Map;
using UnityEngine;

/*
 * This facade provides an uniform interface for the client to handle all logic for the calib tiles
 * - to make visual changes
 * - to work with the persisted json files (selected tiles are saved there)
 * - responsible to get the calib tiles data from a data source (filesystem / api)
 */
public class CalibTileHandlerFacade : MonoBehaviour
{
    // Subsystems
    private CalibTileJsonFileHandler calibTileJsonFileHandler;
    private CalibTileVisibilityHandler calibTileVisibilityHandler;
    private IGetCalibTilesStrategy iGetCalibTilesStrategy;

    private bool isInitialized;
    
    List<CalibTileWrapper> allCalibTileWrappers = new List<CalibTileWrapper>();

    private void Start()
    {
        calibTileJsonFileHandler = gameObject.GetComponent<CalibTileJsonFileHandler>();
        calibTileVisibilityHandler = gameObject.GetComponent<CalibTileVisibilityHandler>();

        // Adjust strategy here, comment in to get the tiles from the API
        //iGetCalibTilesStrategy = gameObject.GetComponent<GetCalibTilesFromApiStrategy>();
    }

    public void Initialize()
    {
        iGetCalibTilesStrategy = new GetCalibTilesFromFilesystemStrategy();
        iGetCalibTilesStrategy.RequestAllTilesList(OnGetAllCalibTilesResponseCallback);
    }

    private void OnGetAllCalibTilesResponseCallback(List<CalibTile> allCalibTiles)
    {
        // Complete the initialization
        InstantiateTiles(allCalibTiles);
        calibTileJsonFileHandler.Initialize(allCalibTileWrappers);
        isInitialized = true;
    }

    private void InstantiateTiles(List<CalibTile> allCalibTiles)
    {
        List<string> previouslySelectedTiles = calibTileJsonFileHandler.ReadSelectedTilesFromJson();
        allCalibTileWrappers = calibTileVisibilityHandler.InstantiateTiles(previouslySelectedTiles, allCalibTiles);
        calibTileVisibilityHandler.HideTiles();
    }
    
    public void ShowTiles()
    {
        calibTileVisibilityHandler.ShowTiles();
    }

    public void HideTiles()
    {
        calibTileVisibilityHandler.HideTiles();
    }

    public void ShowTilesByTileType(TileType typeToShow)
    {
        calibTileVisibilityHandler.ShowTilesByTileType(typeToShow);
    }

    public int GetCountAvailableTiles(TileType typeToShow)
    {
        return calibTileVisibilityHandler.GetCountAvailableTiles(typeToShow);
    }

    public List<CalibTileWrapper> GetAllTiles()
    {
        return allCalibTileWrappers;
    }

    public List<CalibTileWrapper> GetSelectedTiles()
    {
        return calibTileJsonFileHandler.GetSelectedTiles();
    }

    public void WriteCurrentlySelectedTilesToJson()
    {
        calibTileJsonFileHandler.WriteSelectedTilesToJson();
    }

    public void ToggleTileControllerSelectedTiles()
    {
        calibTileJsonFileHandler.ToggleTileControllerSelectedTiles();
    }

    public bool IsInitialized()
    {
        return isInitialized;
    }

}

public enum TileType
{
    cityModel,
    dom,
    dgm,
    cityModel_rgb,
    dom_rgb
}
