using System;

[Serializable]
public class CalibTile
{
    public int epsg;
    public string filename;
    public int offset_x;
    public int offset_y;
    public float size_x;
    public float size_y;
    public string type;
    public float x_sw;
    public float y_sw;
}
