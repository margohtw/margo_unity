using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GetCalibTilesFromFilesystemStrategy : IGetCalibTilesStrategy
{
    private List<CalibTile> allTiles = new List<CalibTile>();

    private string[] filePaths = { "/CalibTileData/cityModel", "/CalibTileData/dgm", "/CalibTileData/dom", "/CalibTileData/cityModel_rgb", "/CalibTileData/dom_rgb" };


    public void RequestAllTilesList(Action<List<CalibTile>> responseCallback)
    {
        allTiles = new List<CalibTile>();
        List<string> filePathList = GetFilePathsAllTiles();
        foreach (var path in filePathList)
        {
            StreamReader sr = new StreamReader(path);
            string metaFileJson = sr.ReadToEnd();
            CalibTile tile = JsonUtility.FromJson<CalibTile>(metaFileJson);
            allTiles.Add(tile);
        }
        responseCallback(allTiles);
    }



    private List<string> GetFilePathsAllTiles()
    {
        List<string> filePathList = new List<string>();

        foreach (var path in filePaths)
        {
            string[] metaPaths = Directory.GetFiles(Application.persistentDataPath + path, "*meta*");
            filePathList.AddRange(metaPaths);
        }
        return filePathList;
    }

    public List<CalibTile> GetAllTilesList()
    {
        return allTiles;
    }
}

