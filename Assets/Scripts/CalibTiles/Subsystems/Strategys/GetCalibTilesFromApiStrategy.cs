using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Not finished yet
public class GetCalibTilesFromApiStrategy : MonoBehaviour, IGetCalibTilesStrategy
{
    private GeoDataApiClient geoDataApiClient;

    private List<CalibTile> allTiles = new List<CalibTile>();

    private Action<List<CalibTile>> responseCallback;

    private void Start()
    {
        geoDataApiClient = gameObject.GetComponent<GeoDataApiClient>();
    }

    public void RequestAllTilesList(Action<List<CalibTile>> responseCallback)
    {
        this.responseCallback = responseCallback;
        geoDataApiClient.RequestCalibTiles(OnResponseCallback);
    }

    public void OnResponseCallback(List<CalibTile> tilesFromApiResponse)
    {
        allTiles = tilesFromApiResponse;
        responseCallback(allTiles);
    }

    public List<CalibTile> GetAllTilesList()
    {
        return allTiles;
    }


}
