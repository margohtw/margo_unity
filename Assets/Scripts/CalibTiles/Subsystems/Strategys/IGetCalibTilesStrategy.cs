using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IGetCalibTilesStrategy
{
    /*
     * Get the calib tile list data from any data source (e.g. file system or api) and return the list 
     */
    void RequestAllTilesList(Action<List<CalibTile>> responseCallback);

    /*
     * Returns the requested list (with no extra request)
     */
    List<CalibTile> GetAllTilesList();
}
