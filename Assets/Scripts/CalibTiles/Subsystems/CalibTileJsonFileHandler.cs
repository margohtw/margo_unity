using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/*
 * The information which tiles the user has selected is stored in json files.
 * This class handles all logic to read, write and change the persisted json files for the selected calib tiles
 */
public class CalibTileJsonFileHandler : MonoBehaviour
{

    private string saveFilePath = "/CalibTileData/selectedTiles";

    private List<CalibTileWrapper> allTiles = new List<CalibTileWrapper>();
    private UserLocationProvider locationProvider;

    void Start()
    {
        locationProvider = GetComponent<UserLocationProvider>();
    }

    public void Initialize(List<CalibTileWrapper> allTilesToHandle)
    {
        allTiles = allTilesToHandle;
    }

    public List<CalibTileWrapper> GetSelectedTiles()
    {
        List<CalibTileWrapper> selectedTiles = new List<CalibTileWrapper>();
        List<string> savedFileNames = ReadSelectedTilesFromJson();
        foreach (CalibTileWrapper tile in allTiles)
        {
            if 
            (   
                //either its selected
                savedFileNames.Contains(tile.GetFilename()) || 
                // or it needs to be specifically of DGM type and also less than 5 km away from the user
                tile.GetCalibTileType() == TileType.dgm && IsTileCloseToUser(tile)
            ) 
            {
                selectedTiles.Add(tile);
            }
        }
        return selectedTiles;
    }

    private bool IsTileCloseToUser(CalibTileWrapper wrapper)
    {
        double[] tileUtmCoords = wrapper.GetUtmCoordinates()[0];
        LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
        LatLngUTMConverter.UTMResult utm = converter.convertLatLngToUtm(locationProvider.GetCurrentLatitude(), locationProvider.GetCurrentLongitude());
        float distance = Mathf.Sqrt((Mathf.Pow((float)(tileUtmCoords[0] - utm.Easting), 2)) + Mathf.Pow((float)(tileUtmCoords[1] - utm.Northing), 2));
        return distance < 5000f;
    }

    public List<string> ReadSelectedTilesFromJson()
    {
        CalibTileSaveData saveDataFile = new CalibTileSaveData();
        if (File.Exists(Application.persistentDataPath + saveFilePath))
        {
            string saveFileContents = File.ReadAllText(Application.persistentDataPath + saveFilePath);
            saveDataFile = JsonUtility.FromJson<CalibTileSaveData>(saveFileContents);
        }

        List<string> fileNameList = new List<string>();
        foreach (string calibTileFileName in saveDataFile.savedCalibTileFilenames)
        {
            fileNameList.Add(calibTileFileName);
        }
        return fileNameList;
    }

    public void WriteSelectedTilesToJson()
    {
        CalibTileSaveData savedFileNames = new CalibTileSaveData();
        foreach (CalibTileWrapper tile in allTiles)
        {
            if (tile.IsSelected())
            {
                savedFileNames.savedCalibTileFilenames.Add(tile.GetFilename());
            }
        }
        string json = JsonUtility.ToJson(savedFileNames);
        File.WriteAllText(Application.persistentDataPath + saveFilePath, json);
    }

    public void ToggleTileControllerSelectedTiles()
    {
        List<string> previouslySelectedTiles = ReadSelectedTilesFromJson();
        foreach (CalibTileWrapper tileWrapper in allTiles)
        {
            if (previouslySelectedTiles.Contains(tileWrapper.GetFilename())) tileWrapper.GetTileController().SetEnabled(true);
            else tileWrapper.GetTileController().SetEnabled(false);
        }
    }
}

[Serializable]
public class CalibTileSaveData
{
    public List<string> savedCalibTileFilenames = new List<string>();
}




