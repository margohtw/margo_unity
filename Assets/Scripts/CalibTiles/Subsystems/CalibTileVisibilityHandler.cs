using Mapbox.Unity.Map;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles all logic to change the visibility of the calib tiles in the UI
 */
public class CalibTileVisibilityHandler : MonoBehaviour
{
    [SerializeField] AbstractMap map;
    [SerializeField] GameObject tileHolder;
    [SerializeField] GameObject tilePrefab;

    private List<GameObject> instancedGameObjects = new List<GameObject>();
    
    private List<CalibTileWrapper> allTileWrappers = new List<CalibTileWrapper>();

    public List<CalibTileWrapper> InstantiateTiles(List<string> previouslySelectedTiles, List<CalibTile> allTiles)
    {
        foreach (var calibTile in allTiles)
        {
            GameObject tileGameObject = Instantiate(tilePrefab, tileHolder.transform, true);
            CalibTileWrapper wrapper = InstantiateWrapperClass(calibTile, tileGameObject);
            tileGameObject.name = wrapper.GetFilename();
            tileGameObject.GetComponent<TileController>().SetWrapper(wrapper);
            instancedGameObjects.Add(tileGameObject);
            wrapper.SetTileGameObject(tileGameObject);
            double[][] wgsCoords = wrapper.GetWgsCoordinates();
            Vector2d latlngA = new Vector2d(wgsCoords[0][0], wgsCoords[0][1]);
            Vector2d latlngB = new Vector2d(wgsCoords[1][0], wgsCoords[1][1]);
            Vector2d latlngC = new Vector2d(wgsCoords[2][0], wgsCoords[2][1]);
            Vector2d latlngD = new Vector2d(wgsCoords[3][0], wgsCoords[3][1]);

            Vector3 a = map.GeoToWorldPosition(latlngA, false);
            Vector3 b = map.GeoToWorldPosition(latlngB, false);
            Vector3 c = map.GeoToWorldPosition(latlngC, false);
            Vector3 d = map.GeoToWorldPosition(latlngD, false);

            //if the distance is too large, the tiles might be over 10,000 units away
            //placing objects this far away causes problems and is unnecessary, so dont draw the tile
            if (Vector3.Magnitude(a - tileHolder.transform.position) > 10000 ||
                Vector3.Magnitude(b - tileHolder.transform.position) > 10000 ||
                Vector3.Magnitude(c - tileHolder.transform.position) > 10000 ||
                Vector3.Magnitude(d - tileHolder.transform.position) > 10000) continue;

            wrapper.GetTileController().DrawTile(a, b, c, d);

            if (previouslySelectedTiles.Contains(wrapper.GetFilename())) wrapper.GetTileController().OnPointerClick(null);
        }

        return allTileWrappers;
    }
    
    private CalibTileWrapper InstantiateWrapperClass(CalibTile calibTile, GameObject tileGameObject)
    {
        CalibTileWrapper calibTileWrapper;
        //tile contains type as raw string! wrapper needs info as enum
        switch (calibTile.type)
        {
            case "cityModel":
                calibTileWrapper = CalibTileWrapper.CreateCalibTileWrapper(tileGameObject, calibTile, TileType.cityModel);
                break;
            case "cityModel_rgb":
                calibTileWrapper = CalibTileWrapper.CreateCalibTileWrapper(tileGameObject, calibTile, TileType.cityModel_rgb);
                break;
            case "dom":
                calibTileWrapper = CalibTileWrapper.CreateCalibTileWrapper(tileGameObject, calibTile, TileType.dom);
                break;
            case "dom_rgb":
                calibTileWrapper = CalibTileWrapper.CreateCalibTileWrapper(tileGameObject, calibTile, TileType.dom_rgb);
                break;
            case "dgm":
                calibTileWrapper = CalibTileWrapper.CreateCalibTileWrapper(tileGameObject, calibTile, TileType.dgm);
                break;
            default:
                return null;
        }
        allTileWrappers.Add(calibTileWrapper);
        return calibTileWrapper;
    }

    public void ShowTilesByTileType(TileType typeToShow)
    {
        Debug.Log("Switching visible tile type to: " + typeToShow.ToString());
        foreach (var calibType in allTileWrappers)
        {
            if (calibType.GetCalibTileType().Equals(typeToShow))
            {
                calibType.GetTileGameObject().SetActive(true);
            }
            else
            {
                calibType.GetTileGameObject().SetActive(false);
            }
        }
    }

    public int GetCountAvailableTiles(TileType typeToShow)
    {
        int count = 0;
        foreach (var tile in allTileWrappers)
        {
            if (tile.GetCalibTileType().Equals(typeToShow))
            {
                count++;
            }
        }
        return count;
    }

    public void ShowTiles()
    {
        foreach (var tile in instancedGameObjects)
        {
            tile.SetActive(true);
        }
    }

    public void HideTiles()
    {
        foreach (var tile in instancedGameObjects)
        {
            tile.SetActive(false);
        }
    }
}
