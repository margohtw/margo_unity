using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FpsCalculator : MonoBehaviour
{
    Text textField;
    public int smoothing;
    private float[] durations;
    private float sum;
    private float avg;
    private float fps;

    void Awake()
    {
        textField = GetComponent<Text>();
        durations = new float[smoothing];
        for (int i = 0; i < durations.Length; i++)
        {
            durations[i] = 0;
        }
    }

    void Update()
    {
        sum = 0;

        for (int i = 1; i < durations.Length; i++)
        {
            durations[i-1] = durations[i];
            sum += durations[i - 1];
        }

        float time = Time.unscaledDeltaTime;
        durations[durations.Length - 1] = time;
        sum += time;
        avg = sum / (float)smoothing;
        fps = (int)(1 / avg);
        textField.text = fps.ToString();
    }
}
