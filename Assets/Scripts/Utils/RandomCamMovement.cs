using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCamMovement : MonoBehaviour
{
    bool isrunning = false;
    [SerializeField] float speed;
    [SerializeField] float totalTime;
    [SerializeField] int angle;
    [SerializeField] GameObject cam;

    private void Start()
    {
        
    }

    void Update()
    {
        if (!isrunning) StartCoroutine(LerpCamera());
    }

    private IEnumerator LerpCamera()
    {
        isrunning = true;
        Vector3 target0 = new Vector3(0, 0, 0);
        Vector3 target1 = new Vector3(0, angle, 0);
        Vector3 target2 = new Vector3(angle, angle, 0);
        Vector3 target3 = new Vector3(angle, -angle, 0);
        Vector3 target4 = new Vector3(0, -angle, 0);

        float progression = 0;
        while (progression < 1)
        {
            progression += Time.fixedDeltaTime / totalTime;
            cam.transform.eulerAngles = Vector3.Lerp(target0, target1, progression);
            yield return null;
        }
        progression = 0;
        while (progression < 1)
        {
            progression += Time.fixedDeltaTime / totalTime;
            cam.transform.eulerAngles = Vector3.Lerp(target1, target2, progression);
            yield return null;
        }
        progression = 0;
        while (progression < 1)
        {
            progression += Time.fixedDeltaTime / totalTime;
            cam.transform.eulerAngles = Vector3.Lerp(target2, target3, progression);
            yield return null;
        }
        progression = 0;
        while (progression < 1)
        {
            progression += Time.fixedDeltaTime / totalTime;
            cam.transform.eulerAngles = Vector3.Lerp(target3, target4, progression);
            yield return null;
        }
        progression = 0;
        while (progression < 1)
        {
            progression += Time.fixedDeltaTime / totalTime;
            cam.transform.eulerAngles = Vector3.Lerp(target4, target0, progression);
            yield return null;
        }


        isrunning = false;
        yield return null;

        /*target1.x = angle;

        Vector3 rot = cam.transform.eulerAngles;
        isrunning = true;
        for (int i = 0; i <= angle; i++)
        {
            rot.x = i;
            Vector3.Lerp(rot, cam.transform.eulerAngles, Time.deltaTime / totalTime);
            cam.transform.eulerAngles = rot;
            yield return new WaitForSeconds(speed);
        }
        for (int j = 0; j <= angle; j++)
        {
            rot.y = j;
            cam.transform.eulerAngles = rot;
            yield return new WaitForSeconds(speed);
        }
        for (int k = angle; k >= 0; k--)
        {
            rot.x = k;
            cam.transform.eulerAngles = rot;
            yield return new WaitForSeconds(speed);
        }
        for (int l = angle; l >= 0; l--)
        {
            rot.y = l;
            cam.transform.eulerAngles = rot;
            yield return new WaitForSeconds(speed);
        }
        isrunning = false;
        yield return null;*/
    }
}
