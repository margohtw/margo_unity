using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderByHeightController : MonoBehaviour
{
    void Start()
    {
        MeshRenderer renderer = GetComponent<MeshRenderer>();
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        Bounds bounds = mesh.bounds;
        Vector3 size = bounds.size;
        MaterialPropertyBlock matBlock = new MaterialPropertyBlock();
        matBlock.SetFloat("Vector1_924bff3813104a939c9cc42c64528449", size.z);
        renderer.SetPropertyBlock(matBlock);

    }
}
