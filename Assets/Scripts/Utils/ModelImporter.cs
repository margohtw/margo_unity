using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using Siccity.GLTFUtility;

public class ModelImporter : MonoBehaviour
{
    private static ModelImporter instance;
    [SerializeField] private ImportSettings importSettings;
    [SerializeField] private Material transparentMat;
    [SerializeField] private MainArUIManager arUiManager;
    [SerializeField] private CalibModelHandler calibModelHandler;
    private List<GameObject> importedObjects;


    private void Start()
    {
        if (instance == null) instance = this;
        if (instance != null == instance != this) this.Destroy();
    }
    
    public static ModelImporter GetInstance()
    {
        return instance;
    }

    public GameObject ImportModel(string filename, string calibTileType)
    {
        if (!CheckPermissions()) return null;

        string path = Application.persistentDataPath + "/CalibTileData/" + calibTileType + "/" + filename;

        GameObject loadedObject;

        loadedObject = Importer.LoadFromFile(path, importSettings);

        MeshRenderer renderer = loadedObject.GetComponent<MeshRenderer>();
        
        if (calibTileType == "dom_rgb")
        {
            Texture texture = renderer.material.mainTexture;
            renderer.material = transparentMat;
            renderer.material.mainTexture = texture;
        }

        loadedObject.name = filename;

        return loadedObject;
    }

    private bool CheckPermissions()
    {
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        }

        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageRead);
        }

        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite) || !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
        {
            //TODO show toast
            return false;
        }
#endif
        return true;
    }
}
