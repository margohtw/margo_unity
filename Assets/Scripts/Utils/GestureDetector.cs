using System.Collections;
using System.Collections.Generic;
using Mapbox.Unity;
using Mapbox.Unity.MeshGeneration.Factories.TerrainStrategies;
using UnityEngine;
using UnityEngine.UIElements;

public enum Gesture
{
    None,
    Tap,
    Drag,
    Zoom,
    ParallelDrag
}

public class GestureDetector : MonoBehaviour
{
    [SerializeField] private int screenSpaceThreshold;
    [SerializeField] private float tapDurationThreshold;
    [SerializeField] private float gestureLockInSeconds;
    [SerializeField] private CanvasManager canvasManager;
    float[] touchDurations = { 0f, 0f };
    private Gesture[] gestureStack;

    private IGestureHandler mapGestureHandler;
    private IGestureHandler arGestureHandler;

    void Start()
    {
        int gestureStackSize = (int)Mathf.Round(1f / gestureLockInSeconds);
        gestureStack = new Gesture[Mathf.Max(gestureStackSize, 2)];
    }

    // Update is called once per frame
    void Update()
    {
        switch (Input.touchCount)
        {
            case 0:
                break;
            case 1:
                touchDurations[0] += Time.deltaTime;
                break;
            case 2:
                touchDurations[0] += Time.deltaTime;
                touchDurations[1] += Time.deltaTime;
                break;
        }

        Gesture detectedGesture = DetectGesture();

        if (detectedGesture != Gesture.None)
        {
            NotifyGestureHandler(detectedGesture);
        }
    }

    private void ResetGestureDetection()
    {
        for (int i = 0; i < gestureStack.Length; i++)
        {
            gestureStack[i] = Gesture.None;
        }
    }

    private bool CheckGestureLock()
    {
        bool locked = true;
        for (int i = 0; i < gestureStack.Length - 1; i++)
        {
            //if any elements of the gesture stack are not a gesture (None)
            //or not all are the same, the gesture shouldnt be locked in
            if (gestureStack[i] == Gesture.None || gestureStack[i] != gestureStack[i + 1]) locked = false;
            gestureStack[i] = gestureStack[i + 1];
        }
        //otherwise the stack is filled with instances of the same gesture
        return locked;
    }

    private Gesture DetectGesture()
    {
        //if the gesture is locked, check _only_ for that type of gesture
        //if its being continued, return that gesture. otherwise return none.
        if (CheckGestureLock())
        {
            return CheckGestureContinuation() ? gestureStack[0] : Gesture.None;
        }

        switch (Input.touchCount)
        {
            case 0:
                ResetGestureDetection();
                return Gesture.None;
            case 1:
                if (IsDragging(Input.touches[0])) return Gesture.Drag;
                if (IsTapped(Input.touches[0])) return Gesture.Tap;
                break;
            case 2:
                if (IsParallelDragging(Input.touches[0], Input.touches[1])) return Gesture.ParallelDrag;
                if (IsZooming(Input.touches[0], Input.touches[1])) return Gesture.Zoom;
                break;
        }

        return Gesture.None;
    }

    private bool CheckGestureContinuation()
    {
        switch (gestureStack[0])
        {
            case Gesture.Drag:
                if (Input.touchCount == 1) return IsDragging(Input.touches[0]);
                break;
            case Gesture.Zoom:
                if (Input.touchCount == 2) return IsZooming(Input.touches[0], Input.touches[1]);
                break;
            case Gesture.ParallelDrag:
                if (Input.touchCount == 2) return IsParallelDragging(Input.touches[0], Input.touches[1]);
                break;
        }
        return false;
    }

    private bool IsTapped(Touch touch)
    {
        return (touch.phase == TouchPhase.Ended && touchDurations[0] < tapDurationThreshold);
    }

    private bool IsDragging(Touch touch)
    {
        return (touch.phase == TouchPhase.Moved && touch.deltaPosition.magnitude > screenSpaceThreshold);
    }

    private bool IsZooming(Touch touch0, Touch touch1)
    {
        Vector2 refVec = (touch0.position - touch1.position).normalized;

        Vector2 dPos0 = touch0.deltaPosition;
        Vector2 dPos1 = touch1.deltaPosition;

        //do not continue if movement is to small
        if (dPos0.magnitude < screenSpaceThreshold && dPos1.magnitude < screenSpaceThreshold) return false;

        if (Vector2.Dot(dPos0.normalized, refVec) < 0 &&
            Vector2.Dot(dPos1.normalized, refVec) > 0)
        {
            return true;
        }

        return false;
    }

    private bool IsParallelDragging(Touch touch0, Touch touch1)
    {
        if (touch0.phase == TouchPhase.Moved || touch1.phase == TouchPhase.Moved)
        {
            Vector2 dPos0 = touch0.deltaPosition;
            Vector2 dPos1 = touch1.deltaPosition;

            //do not continue if movement is to small
            if (dPos0.magnitude < screenSpaceThreshold && dPos1.magnitude < screenSpaceThreshold)
            {
                return false;
            }

            //check if two finger moves parallel to each other
            float dDotProduct = Vector2.Dot(dPos0.normalized, dPos1.normalized);
            if (dDotProduct > 0.8) return true;
        }
        return false;
    }

    public void RegisterGestureHandler(IGestureHandler handler)
    {
        
    }

    private void NotifyGestureHandler(Gesture gesture)
    {
        switch (canvasManager.GetActiveCanvasType())
        {
            case CanvasType.CalibTileScreen:

                break;

        }
    }
}
