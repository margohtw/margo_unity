using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GestureHandlerType
{
    Map,
    AR
}

public interface IGestureHandler
{
    GestureHandlerType handlerType { get; set; }
    public void HandleTap(Touch touch0);
    public void HandleDrag(Touch touch0);
    public void HandleParallelDrag(Touch touch0, Touch touch1);
    public void HandleZoom(Touch touch0, Touch touch1);
}


