using System.Collections;
using System.Collections.Generic;
using Mapbox.Unity.Map;
using UnityEngine;

public enum AppMode
{
    wea,
    pv,
    calibscreen
}

public class AppModeManager : MonoBehaviour
{
    [SerializeField] private MapInteraction mapInteraction;

    private static AppModeManager instance;

    static Stack<AppMode> currentAppMode;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            currentAppMode = new Stack<AppMode>(2);
        }
    }

    public static AppModeManager GetInstance()
    {
        return instance;
    }

    public void SetAppMode(AppMode mode)
    {
        currentAppMode.Push(mode);
        mapInteraction.ChangeMapMode(mode);
    }

    public void PopAppModeStack()
    {
        if (currentAppMode.Count > 0)
        {
            currentAppMode.Pop();
            if (currentAppMode.Count > 0)
            {
                mapInteraction.ChangeMapMode(currentAppMode.Peek());
            }
        }
    }

    public AppMode GetAppMode()
    {
        return currentAppMode.Peek();
    }
}
