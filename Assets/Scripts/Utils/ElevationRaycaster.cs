using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevationRaycaster : MonoBehaviour
{
    private static ElevationRaycaster instance;

    [SerializeField] private LayerMask lmOnlyCalibModels;

    void Start()
    {
        if (instance != null && instance != this) this.Destroy();
        else instance = this;
    }

    public static ElevationRaycaster GetInstance()
    {
        return instance;
    }

    public float GetElevation(Vector3 worldPos)
    {
        Vector3 origin = new Vector3(worldPos.x, 150f, worldPos.z);
        RaycastHit hit;
        if (Physics.Raycast(origin, Vector3.down, out hit, 400f, lmOnlyCalibModels))
        {
            Debug.Log("GetElevation: " + worldPos + ": " + hit.point.y);
            return hit.point.y;
        }
        Debug.Log("GetElevation: 0");
        return 0f;
    }
}
