using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PvModelType
{
    Default,
    Agri
}

public class PvModelManager : MonoBehaviour
{
    private static PvModelManager instance;

    [SerializeField] List<PvModelType> types;
    [SerializeField] List<GameObject> prefabs;
    [SerializeField] List<float> sizesX;
    [SerializeField] List<float> sizesY;
    [SerializeField] List<Texture2D> modelImages;
    [SerializeField] List<float> minDistanceX;
    [SerializeField] List<float> maxDistanceX;
    [SerializeField] List<float> minDistanceY;
    [SerializeField] List<float> maxDistanceY;

    private Dictionary<PvModelType, PvModel> pvModelDict;

    public static PvModelManager GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance != null && instance != this) Destroy(this.gameObject);
        else instance = this;
        pvModelDict = new Dictionary<PvModelType, PvModel>();
        foreach (PvModelType type in types)
        {
            int pos = types.IndexOf(type);
            PvModel newModel = new PvModel(type, sizesX[pos], sizesY[pos], prefabs[pos], modelImages[pos],
                minDistanceX[pos], maxDistanceX[pos], minDistanceY[pos], maxDistanceY[pos]);
            pvModelDict.Add(type, newModel);
        }
    }

    public PvModel GetPvModel(PvModelType type)
    {
        PvModel model;
        pvModelDict.TryGetValue(type, out model);
        return model;
    }
}
