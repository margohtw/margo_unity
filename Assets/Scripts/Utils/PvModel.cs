using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PvModel
{
    private PvModelType type;
    private float sizeX;
    private float sizeY;
    private GameObject modelPrefab;
    private Texture2D modelImage;
    private float minDistanceX;
    private float maxDistanceX;
    private float minDistanceY;
    private float maxDistanceY;

    public PvModel(PvModelType type, float sizeX, float sizeY, GameObject modelPrefab, 
        Texture2D modelImage, float minDistanceX, float maxDistanceX, float minDistanceY, float maxDistanceY)
    {
        this.type = type;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.modelPrefab = modelPrefab;
        this.modelImage = modelImage;
        this.minDistanceX = minDistanceX;
        this.minDistanceY = minDistanceY;
        this.maxDistanceX = maxDistanceX;
        this.maxDistanceY = maxDistanceY;
    }

    public PvModelType GetModelType()
    {
        return type;
    }

    public float GetSizeX()
    {
        return sizeX;
    }

    public float GetSizeY()
    {
        return sizeY;
    }

    public Texture2D GetImage()
    {
        return modelImage;
    }

    public GameObject GetModelPrefab()
    {
        return modelPrefab;
    }

    public float GetMinDistanceY()
    {
        return minDistanceY;
    }

    public float GetMaxDistanceY()
    {
        return maxDistanceY;
    }
    public float GetMinDistanceX()
    {
        return minDistanceX;
    }
    public float GetMaxDistanceX()
    {
        return maxDistanceX;
    }
}
