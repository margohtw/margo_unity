using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RawArCameraOutputController : MonoBehaviour, IPointerClickHandler
{

    [SerializeField] private Camera arCamera;

    private GameObject lastClickedWeaModel;

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        if (!MainArUIManager.IsMouseOverUI)
        {
            CheckClickOnWEAModel();
        }
    }

    private void CheckClickOnWEAModel()
    {
        Debug.Log("ClickTest:" + name + " RawImage clicked!");

        RaycastHit hit;
        Ray ray = arCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log("ClickTest: Raycast performed: " + hit.transform.name);
            if (hit.transform.name.StartsWith("WeaModel"))
            {
                Debug.Log("ClickTest:" + name + " WEA Model clicked!");
                lastClickedWeaModel = hit.transform.gameObject;
                CanvasManager.GetInstance().SwitchToScreen(CanvasType.ChangeWEAModelScreen);

                // Set the RawArCameraOutput gameobject active explicitly when switchting the screen                 
                // to prevent the WEA model animation from stopping
                gameObject.SetActive(true);
            }
        }
    }

    public void RotateLastClickedWeaModel(int degrees)
    {
        this.lastClickedWeaModel.transform.eulerAngles = new Vector3(0, degrees, 0);
        Debug.Log("Model um Y-Achse gedreht ... Gradzahl: " + this.lastClickedWeaModel.transform.eulerAngles.y);

    }

    public void ChangeScaleWeaModel(float size)
    {
        this.lastClickedWeaModel.transform.localScale = new Vector3(size, size, size);
        Debug.Log("Model-Scale verändert ... Größenfaktor: " + size);
    }

}