using System.Collections;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using Mapbox.VectorTile.Geometry;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class WorldToUnitySpaceConverter : MonoBehaviour
{
    private static WorldToUnitySpaceConverter instance;

    private float unityToWorldYOffset;
    private Quaternion calibratedRotationOffset = Quaternion.identity;
    private float calibratedRotationYOffset;
    private Vector3 calibratedLocationOffset;
    private float[] trueHeadingSamples;
    private float[] cameraHeadingSamples;
    private bool isGyroStabilized;
    private bool isGyroOffsetSet;
    private float currentProgress;
    private bool shouldSetOffset;
    private float maxRadDeltaPerSecond;
    //private Vector2d initialLatLng; - Do we need this?

    private AndroidJavaObject sensorPlugin = null;
    private AndroidJavaObject activityContext = null;
    private AndroidJavaClass activityClass = null;    

    [SerializeField] private int offsetSampleSize;
    [SerializeField] private UserLocationProvider userLocationProvider;
    [SerializeField] private Camera arCamera;
    [SerializeField] private MainArUIManager mainArUiManager;
    [SerializeField] private float gyroStabilizationSeconds;
    [SerializeField] private float maxDegDeltaPerSecond;
    [SerializeField] private GameObject arModelHolder;
    [SerializeField] private AbstractMap map;
    [SerializeField] private UserLocationController ulc;
    [SerializeField] private MainArScreenController mainArScreenController;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

        if (Application.platform == RuntimePlatform.Android)
        {
            activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");

            sensorPlugin = new AndroidJavaObject("de.ar4wind.gyroplugin.SensorChangedPlugin");
            sensorPlugin.Call("init", activityContext);
        }

        maxRadDeltaPerSecond = maxDegDeltaPerSecond * Mathf.Deg2Rad;
    }

    void Update()
    {
        if (shouldSetOffset)
        {
            DetermineUnityAngleOffset();
        }
    }

    public void Init()
    {
        shouldSetOffset = true;
        //initialize the arrays with dummy data
        trueHeadingSamples = new float[offsetSampleSize];
        cameraHeadingSamples = new float[offsetSampleSize];
        for (int i = 1; i < offsetSampleSize; i++)
        {
            trueHeadingSamples[i] = 0f;
            cameraHeadingSamples[i] = 0f;
        }
        //reset all variables in case they were previously set
        isGyroStabilized = false;
        isGyroOffsetSet = false;
        currentProgress = 0f;
        unityToWorldYOffset = 0f;
    }

    public void Calibrate()
    {
        //store the new values
        calibratedLocationOffset = arModelHolder.transform.position - arCamera.transform.position;
        calibratedRotationOffset = arModelHolder.transform.rotation;
        calibratedRotationYOffset = arModelHolder.transform.rotation.eulerAngles.y;
    }

    private void DetermineUnityAngleOffset()
    {
        //make sure the gyro is enabled
        if (Input.gyro.enabled == false) Input.gyro.enabled = true;

        //check if the stabilization phase was successful, otherwise get some more values
        if (!isGyroStabilized)
        {
            StartCoroutine(WaitForStabilization());
            float trueHeading = 0f;
            if (sensorPlugin != null)
            {
                trueHeading = sensorPlugin.Call<float>("getAngleToNorth");
            }
            SetNewHeadingPair(arCamera.transform.rotation.eulerAngles.y, trueHeading);
        }

        //once stabilized, set the gyro offset
        if (isGyroStabilized && !isGyroOffsetSet)
        {
            SetOffset();
            isGyroOffsetSet = true;
            shouldSetOffset = false;
            mainArUiManager.SwitchUiMode(MainArUIManager.ArUiMode.LoadingCalibModels);
        }
    }

    public IEnumerator WaitForStabilization()
    {
        currentProgress += (1f / gyroStabilizationSeconds) * Time.deltaTime;
        mainArUiManager.SetLoadingBarProgress(currentProgress);

        //finished! end the stabilization process
        if (currentProgress >= 1)
        {
            isGyroStabilized = true;
        }
        //otherwise check if the gyro moved too much
        else
        {
            if (Input.gyro.rotationRate.x > maxRadDeltaPerSecond ||
                Input.gyro.rotationRate.y > maxRadDeltaPerSecond ||
                Input.gyro.rotationRate.z > maxRadDeltaPerSecond)
            {
                currentProgress = 0;
            }
        }
        yield return null;
    }

    public static WorldToUnitySpaceConverter GetInstance()
    {
        return instance;
    }

    public float GetAndroidAngleToNorth()
    {
        if (sensorPlugin != null)
        {
            return (sensorPlugin.Call<float>("getAngleToNorth") - calibratedRotationYOffset) % 360f;
        }
        return 0f;
    }

    public float[] GetYpr()
    {
        if (sensorPlugin != null)
        {
            return sensorPlugin.Call<float[]>("getYPR");
        }
        return new float[3];
    }

    public float GetUnityToWorldYOffset()
    {
        return unityToWorldYOffset;
    }

    // -----------------------------------------------------------------------------------------------------------------------------------------
    // INITIALIZATION METHODS
    // -----------------------------------------------------------------------------------------------------------------------------------------

    public void SetOffset()
    {
        Quaternion[] trueHeadingQuaternions = new Quaternion[offsetSampleSize];
        Quaternion[] cameraHeadingQuaternions = new Quaternion[offsetSampleSize];
        Quaternion[] diffs = new Quaternion[offsetSampleSize];

        //convert all rotation data to quaternions prior to average calculation!
        for (int i = 1; i < offsetSampleSize; i++)
        {
            trueHeadingQuaternions[i] = Quaternion.Euler(0f, trueHeadingSamples[i], 0f);
            cameraHeadingQuaternions[i] = Quaternion.Euler(0f, cameraHeadingSamples[i], 0f);
        }

        //calculate the differences for each pair
        for (int i = 1; i < offsetSampleSize; i++)
        {
            diffs[i] = trueHeadingQuaternions[i] * Quaternion.Inverse(cameraHeadingQuaternions[i]);
        }

        //calculate the average of the differences
        Quaternion avg = AverageQuaternions(diffs);

        //set the offset
        unityToWorldYOffset = avg.eulerAngles.y;
    }

    public void SetNewHeadingPair(float cameraYrotation, float trueHeading)
    {
        //move up each previously saved pair
        for (int i = 1; i < offsetSampleSize; i++)
        {
            trueHeadingSamples[i - 1] = trueHeadingSamples[i];
            cameraHeadingSamples[i - 1] = cameraHeadingSamples[i];
        }
        //save the latest pair in the last position of the array
        trueHeadingSamples[offsetSampleSize - 1] = trueHeading;
        cameraHeadingSamples[offsetSampleSize - 1] = cameraYrotation;
    }

    // -----------------------------------------------------------------------------------------------------------------------------------------
    // CONVERSION METHODS
    // -----------------------------------------------------------------------------------------------------------------------------------------

    //used to adjust rotations directly
    public Quaternion OffsetUnityRotation(float angle)
    {
        angle = angle - unityToWorldYOffset;
        Quaternion result;
        result = Quaternion.AngleAxis(angle, Vector3.up);
        result *= calibratedRotationOffset;
        return result;
    }

    //used to place calibtiles in unity space
    public Vector3 UtmToUnitySpace(double[] utmPosTarget)
    {
        //method will return a vector3 in unity space
        Vector3 result;

        //coordinate conversion
        LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
        LatLngUTMConverter.UTMResult utmPosUser = converter.convertLatLngToUtm(userLocationProvider.GetCurrentLatitude(), userLocationProvider.GetCurrentLongitude());

        //determine delta between user and target in utm coordinates (z = northing, x = easting)
        float dZ = (float)(utmPosTarget[1] - utmPosUser.Northing);
        float dX = (float)(utmPosTarget[0] - utmPosUser.Easting);

        //calculate magnitude of the vector (dX, 0, dZ)
        float magnitude = Mathf.Sqrt(Mathf.Pow(dX, 2) + Mathf.Pow(dZ, 2));

        //determine offset-adjusted rotation around y axis and normalize
        Quaternion quat = OffsetAdjustedRotationAroundYAxis(dX, dZ, true);
        quat.Normalize();

        //create a unit vector that has the same orientation relative to the z axis
        result = quat * Vector3.forward;

        //scale the length to match the original magnitude
        result *= magnitude;

        //adjust for the current camera position moving away from 0,0,0
        result += arCamera.transform.position;

        return result;
    }

    //used when the map is tapped to create a WEA model
    public Vector3 LatLngToUnitySpace(Vector2d latLng)
    {
        //method will return a vector3 in unity space
        Vector3 result;

        //coordinate conversion
        LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
        LatLngUTMConverter.UTMResult utmPosTarget = converter.convertLatLngToUtm(latLng.x, latLng.y);
        LatLngUTMConverter.UTMResult utmPosUser = converter.convertLatLngToUtm(userLocationProvider.GetCurrentLatitude(), userLocationProvider.GetCurrentLongitude());

        //determine delta between user and target in utm coordinates (z = northing, x = easting)
        float dZ = (float)(utmPosTarget.Northing - utmPosUser.Northing);
        float dX = (float)(utmPosTarget.Easting - utmPosUser.Easting);

        //calculate magnitude of the vector (dX, 0, dZ)
        float magnitude = Mathf.Sqrt(Mathf.Pow(dX, 2) + Mathf.Pow(dZ, 2));

        //determine offset-adjusted rotation around y axis and normalize
        Quaternion quat = OffsetAdjustedRotationAroundYAxis(dX, dZ, true);
        quat.Normalize();

        //create a unit vector that has the same orientation relative to the z axis
        result = quat * Vector3.forward;

        //scale the length to match the original magnitude
        result *= magnitude;

        //adjust for the current camera position moving away from 0,0,0
        result += arCamera.transform.position;

        //adjust for the calibration by the user
        result += calibratedLocationOffset;

        return result;
    }

    public double[] UnitySpaceToUTM(Vector3 unityWorldSpace)
    {
       

        //the Y component of the unityWorldSpace vector must be 0, elevation is accounted for seperately
        unityWorldSpace.y = 0.0f;

        //coordinate conversion
        LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
        LatLngUTMConverter.UTMResult utmPosUser = converter.convertLatLngToUtm(userLocationProvider.GetCurrentLatitude(), userLocationProvider.GetCurrentLongitude());

        //determine delta between user and target in unity world coordinates
        Vector3 delta = unityWorldSpace - arCamera.transform.position;

        //adjust for the calibration by the user
        delta -= calibratedLocationOffset;

        //calculate magnitude of the delta vector
        float magnitude = Mathf.Sqrt(Mathf.Pow(delta.x, 2) + Mathf.Pow(delta.z, 2));

        //determine offset-adjusted rotation around y axis and normalize
        Quaternion quat = OffsetAdjustedRotationAroundYAxis(delta.x, delta.z, false);
        quat.Normalize();

        //create a unit vector that has the same orientation relative to the z axis
        Vector3 adjustedPosition = quat * Vector3.forward;

        //scale the length to match the original magnitude
        adjustedPosition *= magnitude;

        //use adjustedPosition as delta value for users current utmPosition
        float utmTargetEasting = (float)utmPosUser.Easting + adjustedPosition.x;
        float utmTargetNorthing = (float)utmPosUser.Northing + adjustedPosition.z;

        //create the mapbox latlng class to return
        double[] result = {utmTargetEasting, utmTargetNorthing};

        return result;
    }


    //used after the middle button is pressed and a map symbol needs to be generated
    public Vector2d UnitySpaceToLatLng(Vector3 unityWorldSpace)
    {
        double[] utm = UnitySpaceToUTM(unityWorldSpace);

        LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
        //convert the determined utm position of the target to latlng
        LatLngUTMConverter.LatLng latLng = converter.convertUtmToLatLng(utm[0], utm[1], 33, "N");

        //create the mapbox latlng class to return
        Vector2d result = new Vector2d(latLng.Lat, latLng.Lng);
        return result;
    }

    // -----------------------------------------------------------------------------------------------------------------------------------------
    // HELPER METHODS
    // -----------------------------------------------------------------------------------------------------------------------------------------

    private Quaternion OffsetAdjustedRotationAroundYAxis(float xPos, float zPos, bool RealWorldToUnity)
    {
        //calculates a quaternion that rotates around the y axis and points towards the position (x,z) in the x-z plane
        //angle is adjusted by offset, depending on which way the conversion is happening:
        //real world --> unity space or
        //unity space --> real world!
        Quaternion result;
        float angle = Mathf.Atan2(xPos, zPos);
        angle = angle * Mathf.Rad2Deg;
        if (RealWorldToUnity)
        {
            angle = angle - unityToWorldYOffset;
            result = Quaternion.AngleAxis(angle, Vector3.up);
            result *= calibratedRotationOffset;
        }
        else
        {
            angle = angle + unityToWorldYOffset;
            result = Quaternion.AngleAxis(angle, Vector3.up);
            result *= Quaternion.Inverse(calibratedRotationOffset); 
        }

        return result;
    }

    private Quaternion AverageQuaternions(Quaternion[] quats)
    {
        float w = 0.0f;
        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;
        for (int i = 0; i < quats.Length; i++)
        {
            w += quats[i].w;
            x += quats[i].x;
            y += quats[i].y;
            z += quats[i].z;
        }
        w *= (1f / quats.Length);
        x *= (1f / quats.Length);
        y *= (1f / quats.Length);
        z *= (1f / quats.Length);

        return new Quaternion(x, y, z, w);
    }

    public void ManualUserLocationUpdate(Vector2d oldLatLng, Vector2d newLatLng)
    {
        //coordinate conversion
        LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
        LatLngUTMConverter.UTMResult utmPosOld = converter.convertLatLngToUtm(oldLatLng[0], oldLatLng[1]);
        LatLngUTMConverter.UTMResult utmPosNew = converter.convertLatLngToUtm(newLatLng[0], newLatLng[1]);
        
        //determine change in position
        Vector3 delta = new Vector3(
            (float) (utmPosOld.Easting - utmPosNew.Easting),
            arModelHolder.transform.position.y,
            (float) (utmPosOld.Northing - utmPosNew.Northing));
        
        //change the position of the ar models accordingly (since the camera needs to stay unmodified, due to AR constraints!)
        arModelHolder.transform.position = delta;
    }

    public void EndInitialGpsPositioning(Vector2d latLng)
    {
        userLocationProvider.ManualUserLocationUpdate(latLng);
        ulc.gameObject.transform.position =
            map.GeoToWorldPosition(latLng);
        //ManualUserLocationUpdate(initialLatLng, latLng);
        MapInteraction mapInteraction = map.GetComponent<MapInteraction>();
        mapInteraction.RecenterMapOnUserLocation();
        mainArScreenController.InitializeUserElevation();

        //map.gameObject.GetComponent<MapInteraction>().RecenterMapOnUserLocation();

    }

    /* Do we need this?
     * public void StartInitialGpsPositioning(Vector2d latLng)
    {
        initialLatLng = latLng;
    }*/
}
