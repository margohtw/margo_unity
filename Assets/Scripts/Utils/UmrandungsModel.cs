using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UmrandungsModel : MonoBehaviour
{
    private UmrandungsType type;
    private float sizeX;
    private float sizeY;
    private float minHeight;
    private float maxHeight;
    private GameObject modelPrefab;
    private Texture2D modelImage;

    public UmrandungsModel(UmrandungsType type, float sizeX, float sizeY, GameObject modelPrefab, Texture2D modelImage, float minHeight, float maxHeight)
    {
        this.type = type;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
        this.modelPrefab = modelPrefab;
        this.modelImage = modelImage;
    }

    public UmrandungsType GetUmrandungsType()
    {
        return type;
    }

    public float GetSizeX()
    {
        return sizeX;
    }

    public float GetSizeY()
    {
        return sizeY;
    }

    public GameObject GetPrefab()
    {
        return modelPrefab;
    }

    public Texture2D GetModelImage()
    {
        return modelImage;
    }

    public float GetMinHeight()
    {
        return minHeight;
    }

    public float GetMaxHeight()
    {
        return maxHeight;
    }
}
