using System;
using System.IO;
using UnityEngine;

public class CalibLogger : MonoBehaviour
{
    private static CalibLogger instance;
    private string saveFilePath;
    private string logText;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null) instance = this;
        if (instance != null == instance != this) this.Destroy();
        logText = "";
    }

    public static CalibLogger GetInstance()
    {
        return instance;
    }

    public void startCalibLogger()
    {
        DateTime theTime = DateTime.Now;
        string datetime = theTime.ToString("yyyy-MM-dd_HH-mm-ss");
        saveFilePath = "/calibLog_" + datetime + ".txt";
        
        addText("calibStart");
    }


    public void addText(string text)
    {
        DateTime theTime = DateTime.Now;
        string datetime = theTime.ToString("yyyy-MM-dd_HH-mm-ss");
        logText += datetime + ": " + text;
        logText += "\n";
    }


    public void endCalibLogger()
    {
        addText("calibEnd");
        File.WriteAllText(Application.persistentDataPath  + saveFilePath, logText);
        logText = "";
    }

}
