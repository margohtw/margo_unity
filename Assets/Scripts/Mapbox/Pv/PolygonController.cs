using System.Collections;
using System.Collections.Generic;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class PolygonController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    //interaction  handling
    [SerializeField] float timeThreshold;
    float clickDuration = 0;
    bool touchRegistered = false;

    //rendering
    MeshCollider meshCollider;
    LineRenderer lineRenderer;
    Renderer renderer;
    Mesh mesh;
    Vector3[] vertices;

    //positions and status
    bool isActive = false;
    bool isFinished = false;
    List<Vector3> pointList;
    List<Vector3> globalPointPos;
    List<GameObject> cornerList;

    //visuals
    [SerializeField] Material inactiveMat;
    [SerializeField] Material activeMat;
    [SerializeField] GameObject pvCornerPrefab;
    [SerializeField] Material lineMaterial;
    [SerializeField] float lineWidth;
    CustomLineRenderer lr;

    //grid drawing
    private PvGridCalculator pvGridCalculator;

    //map and 3d object instantiating and tracking
    [SerializeField] private List<GameObject> pvModelPrefabList;
    [SerializeField] private GameObject pvTilePrefab;
    [SerializeField] private GameObject pvModelPrefab;
    [SerializeField] private GameObject pvSymbolsPrefab;
    private Dictionary<GameObject, GameObject> pvGoDict = new Dictionary<GameObject, GameObject>();

    //coordinate conversion
    private AbstractMap map;

    private void Awake()
    {
        map = GameObject.Find("Map").GetComponent<AbstractMap>();
        pointList = new List<Vector3>();
        globalPointPos = new List<Vector3>();
        cornerList = new List<GameObject>();
        renderer = GetComponent<Renderer>();
        meshCollider = GetComponent<MeshCollider>();
        lr = GetComponentInChildren<CustomLineRenderer>();
    }

    public void Update()
    {
        if (touchRegistered) clickDuration += Time.deltaTime;
        
        if (clickDuration > timeThreshold && !isActive)
        {
            SetActive();
        }
    }

    public void AddCorner(Vector3 position, float scale)
    {
        var corner = Instantiate(pvCornerPrefab, gameObject.transform, false);
        corner.transform.localScale = corner.gameObject.transform.localScale * scale;
        corner.transform.position = position + DrawLayer.point;
        cornerList.Add(corner);

        corner.GetComponent<PolygonCornerController>().SetCornerIndex(lr.GetPositionCount());

        pointList.Add(corner.transform.localPosition);
        globalPointPos.Add(corner.transform.position);

        Vector3 linePos = corner.transform.localPosition;
        lr.AddCorner(linePos);
    }

    public void Finalize()
    {
        if (isFinished) return;

        //draw polygon
        DrawPolygon();

        //init grid calculator
        pvGridCalculator = gameObject.AddComponent<PvGridCalculator>();
        pvGridCalculator.Init(this, pvSymbolsPrefab, pvTilePrefab);

        //open the settings screen and connect it to this polygon
        CanvasManager.GetInstance().SwitchToScreen(CanvasType.ChangePVModelScreen);
        GameObject settingsScreen = CanvasManager.GetInstance().GetScreenOfType(CanvasType.ChangePVModelScreen);
        settingsScreen.GetComponent<ChangePVSettingsScreenController>().SetPolygonController(this);

        Deactivate();
    }

    private void DrawPolygon()
    {

        lr.CloseLoop();

        //triangulation
        if (pointList.Count < 3) return;
        vertices = pointList.ToArray();
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i].y = DrawLayer.poly.y;
        }
        Triangulator tr = new Triangulator(vertices);
        int[] indices = tr.Triangulate();

        //mesh creation
        var mf = GetComponent<MeshFilter>();
        mesh = new Mesh();
        mf.mesh = mesh;

        mesh.vertices = vertices;

        mesh.triangles = indices;

        var normals = new Vector3[vertices.Length];
        for (int i = 0; i < normals.Length; i++)
        {
            normals[i] = -Vector3.forward;
        }
        mesh.normals = normals;

        renderer.material = inactiveMat;
        meshCollider.sharedMesh = mesh;

        isFinished = true;
    }

    private void Deactivate()
    {
        foreach (var corner in cornerList)
        {
            corner.gameObject.SetActive(false);
        }
    }

    public void AdjustPolygon(int cornerIndex, Vector3 deltaMove)
    {
        Vector3 newPosPoly;

        float scaleFactor = 1 / gameObject.transform.lossyScale.x;
        deltaMove *= scaleFactor;

        if (isFinished)
        {
            newPosPoly = vertices[cornerIndex] + deltaMove;
            newPosPoly.y = DrawLayer.poly.y;
            vertices[cornerIndex] = newPosPoly;
            mesh.vertices = vertices;
            meshCollider.sharedMesh = mesh;
        }
        else
        {
            newPosPoly = pointList[cornerIndex] + deltaMove;
            newPosPoly.y = DrawLayer.poly.y;
            pointList[cornerIndex] = newPosPoly;
        }

        lr.AdjustCornerPosition(cornerIndex, isFinished, deltaMove);
    }

    public List<Vector3> GetPointList()
    {
        return globalPointPos;
    }

    public bool IsFinished()
    {
        return isFinished;
    }

    private void SetActive()
    {
        renderer.material = activeMat;
        isActive = true;
        touchRegistered = false;
        clickDuration = 0;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //nothing yet!
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        touchRegistered = true;
    }

    public void UpdateRotation(float value)
    {
        pvGridCalculator.RotateGrid(value);
    }

    public float UpdateRowDistance(float value)
    {
        return pvGridCalculator.SetRowDistance(value);
    }

    public float UpdateModuleDistance(float value)
    {
        return pvGridCalculator.SetModuleDistance(value);
    }

    public void SwitchPvModels(PvModelType type)
    {
        pvGridCalculator.SwitchPvModels(type);
    }

    public void SwitchUmrandungsModel(UmrandungsType type)
    {
        pvGridCalculator.SwitchUmrandungsModel(type);
    }

    public float UpdateUmrandungsModelHeight(float percentage)
    {
        return pvGridCalculator.UpdateUmrandungsModelHeight(percentage);
    }
}
