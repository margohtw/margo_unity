using Mapbox.Unity.Map;
using Mapbox.Utils;
using Mapbox.VectorTile.Geometry;
using System;
using System.Collections.Generic;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class PvGridCalculator : MonoBehaviour
{
    private float currentRotation;
    private float currentRowDistance;
    private float currentModuleXDistance;

    private List<Point> polygonPoints = new List<Point>();
    private GameObject polyPvSymbolHolder;
    private GameObject polyPvModelHolder;
    private Dictionary<GameObject, GameObject> pvGoDictDefault = new Dictionary<GameObject, GameObject>();
    private Dictionary<GameObject, GameObject> pvGoDictAgri = new Dictionary<GameObject, GameObject>();
    private Dictionary<GameObject, GameObject> pvGoDictC = new Dictionary<GameObject, GameObject>();
    private List<Row> rowList = new List<Row>();

    private PvModel currentlyActiveModel;
    private UmrandungsModel currentlyActiveUmrandung;

    private Polygon polygon;

    private LatLngUTMConverter converter;

    private AbstractMap map;
    private GameObject mapPvIconsHolder;
    private GameObject arModelsHolder;

    private PolygonController polygonController;

    private GameObject pvTilePrefab;
    private GameObject pvSymbolsPrefab;

    private List<GameObject> pvPrefabsList;

    private List<GameObject> umrandungenGameObjects;

    private List<GameObject> pvModelsListTypeDefault;
    private List<GameObject> pvModelsListTypeAgri;
    private List<List<GameObject>> pvModelsListList;

    private List<GameObject> umrandungsModelListTypeZaun;
    private List<GameObject> umrandungsModelListTypeHecke;
    private List<List<GameObject>> umrandungsModelsListList;

    private GameObject umrandungsModelZaunHolder;
    private GameObject umrandungsModelHeckeHolder;

    public void Init(PolygonController polyController, GameObject pvSymbolsPrefab, GameObject pvTilePrefab)
    {
        currentlyActiveModel = PvModelManager.GetInstance().GetPvModel(PvModelType.Default);
        currentlyActiveUmrandung = UmrandungsModelManager.GetInstance().GetUmrandungsModel(UmrandungsType.Zaun);

        this.polygonController = polyController;
        this.pvTilePrefab = pvTilePrefab;
        this.pvSymbolsPrefab = pvSymbolsPrefab;

        pvModelsListTypeDefault = new List<GameObject>();
        pvModelsListTypeAgri = new List<GameObject>();
        pvModelsListList = new List<List<GameObject>>();
        pvModelsListList.Add(pvModelsListTypeDefault);
        pvModelsListList.Add(pvModelsListTypeAgri);

        umrandungsModelListTypeZaun = new List<GameObject>();
        umrandungsModelListTypeHecke = new List<GameObject>();
        umrandungsModelsListList = new List<List<GameObject>>();
        umrandungsModelsListList.Add(umrandungsModelListTypeZaun);
        umrandungsModelsListList.Add(umrandungsModelListTypeHecke);

        converter = new LatLngUTMConverter("ETRS89");

        map = GameObject.Find("Map").GetComponent<AbstractMap>();
        mapPvIconsHolder = GameObject.Find("PvIconHolder");
        arModelsHolder = GameObject.Find("PvModelHolder");

        //TODO SET polygonPoints MEMBER FROM INPUT
        List<Vector3> unityWorldPosList = polyController.GetPointList();
        foreach (var unityWorldPos in unityWorldPosList)
        {
            Vector2d latLng = map.WorldToGeoPosition(unityWorldPos);
            Point newPoint = new Point((float) latLng.x, (float) latLng.y);
            polygonPoints.Add(newPoint);
        }

        //find max axis aligned bounding box around polygon
        float startNorthing = float.MaxValue;
        float startEasting = float.MaxValue;
        float endNorthing = float.MinValue;
        float endEasting = float.MinValue;
        foreach (Point p in polygonPoints)
        {
            startEasting = Mathf.Min((float) startEasting, (float) p.GetEasting());
            startNorthing = Mathf.Min((float) startNorthing, (float) p.GetNorthing());
            endEasting = Mathf.Max((float) endEasting, (float) p.GetEasting());
            endNorthing = Mathf.Max((float) endNorthing, (float) p.GetNorthing());
        }

        polygon = new Polygon(polygonPoints);

        InitPvGrid();
    }

    public void InitPvGrid()
    {
        //calculate the poly's pivot point in map space and make the map symbols holder child pivot there for later rotation
        polyPvSymbolHolder = Instantiate(pvSymbolsPrefab);
        polyPvSymbolHolder.transform.parent = mapPvIconsHolder.transform;
        double centerNorthing = 0d;
        double centerEasting = 0d;
        foreach (Point point in polygon.GetOrderedPoints())
        {
            centerNorthing += point.GetNorthing();
            centerEasting += point.GetEasting();
        }

        centerNorthing /= polygon.GetOrderedPoints().Length;
        centerEasting /= polygon.GetOrderedPoints().Length;
        Point center = new Point(centerEasting, centerNorthing, 33);
        LatLngUTMConverter.LatLng result = converter.convertUtmToLatLng(centerEasting, centerNorthing, 33, "N");
        Vector2d polyLatLngCenter = new Vector2d(result.Lat, result.Lng);
        Vector3 pivot = map.GeoToWorldPosition(polyLatLngCenter);
        polyPvSymbolHolder.transform.position = pivot;

        polyPvModelHolder = new GameObject("polyPvModelHolder");
        polyPvModelHolder.gameObject.transform.parent = arModelsHolder.transform;
        polyPvModelHolder.transform.position =
            WorldToUnitySpaceConverter.GetInstance().LatLngToUnitySpace(polyLatLngCenter);

        //calc maximum distance from any poly point to the pivot
        float radius = float.MinValue;
        foreach (Point point in polygon.GetOrderedPoints())
        {
            float distance = Mathf.Sqrt(Mathf.Pow((float) point.GetEasting() - (float) center.GetEasting(), 2) +
                                        Mathf.Pow((float) point.GetNorthing() - (float) center.GetNorthing(), 2));
            radius = Mathf.Max(distance, radius);
        }

        float sizeY = currentlyActiveModel.GetSizeY();
        float sizeX = currentlyActiveModel.GetSizeX();
        float minRowDistance = currentlyActiveModel.GetMinDistanceY();
        float minModuleDistance = currentlyActiveModel.GetMinDistanceX();

        radius += sizeY + minRowDistance;

        float deltaRadius = 0f;
        float cosAlpha = 1f;
        float alpha = Mathf.Acos(cosAlpha);
        float sinAlpha = Mathf.Sin(alpha);
        int rows = 0;
        float startY = (float) centerNorthing + radius;
        //center row is in center of circle
        startY += startY % (sizeY + minRowDistance);
        int totalRows = 1 + 2 * Mathf.RoundToInt((float) (startY - centerNorthing) / (sizeY + minRowDistance));

        //TODO better check for stopping the loop
        while (alpha > -0.9f)
        {
            float deltaX = sinAlpha * radius;
            deltaX += (sizeX + minModuleDistance) - (deltaX % (sizeX + minModuleDistance));
            float rowCounter = deltaX;
            double northing = startY - (rows * (sizeY + minRowDistance));
            Row row = new Row(new double[] {centerEasting, centerNorthing}, rows, totalRows, polyPvSymbolHolder,
                polyPvModelHolder);
            row.AdjustRowPosition(currentRotation, minRowDistance, converter, map);
            rowList.Add(row);
            while (rowCounter > -deltaX)
            {
                double easting = (float) centerEasting - rowCounter;
                InstantiateAtUtmPos(easting, northing, row);
                rowCounter = rowCounter - sizeX - minRowDistance;
            }

            deltaRadius = deltaRadius - sizeY - minRowDistance;
            cosAlpha = (radius + deltaRadius) / radius;
            alpha = (Mathf.Acos(cosAlpha));
            sinAlpha = Mathf.Sin(alpha);
            rows++;
        }

        InstantiateUmrandungen();

        SwitchPvModels(0);
        SwitchUmrandungsModel(0);
        UpdateVisibility();
    }

    private void InstantiateUmrandungen()
    {
        Point[] points = polygon.GetOrderedPoints();
        foreach (UmrandungsType type in Enum.GetValues(typeof(UmrandungsType)))
        {
            UmrandungsModel model = UmrandungsModelManager.GetInstance().GetUmrandungsModel(type);
            GameObject holder;
            switch (model.GetUmrandungsType())
            {
                case UmrandungsType.Hecke:
                    umrandungsModelHeckeHolder = new GameObject();
                    holder = umrandungsModelHeckeHolder;
                    holder.transform.parent = arModelsHolder.transform;
                    break;
                case UmrandungsType.Zaun:
                    umrandungsModelZaunHolder = new GameObject();
                    holder = umrandungsModelZaunHolder;
                    holder.transform.parent = arModelsHolder.transform;
                    break;
                default:
                    holder = new GameObject();
                    break;
            }
            for (int i = 0; i < points.Length; i++)
            {
                Point p1 = points[i];
                Point p2 = points[(i + 1) % points.Length];
                Point o, d;
                o = p1.GetEasting() > p2.GetEasting() ? p2 : p1;
                d = p1.GetEasting() > p2.GetEasting() ? p1 : p2;
                float dx = (float)o.GetEasting() - (float)d.GetEasting();
                float dy = (float)o.GetNorthing() - (float)d.GetNorthing();
                float l = Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2));
                float alphaRad = 6.28f - Mathf.Acos(dy / l);
                float alphaDeg = alphaRad * Mathf.Rad2Deg;
                float width = model.GetSizeX();
                int instances = (int)(l / width);
                float overhang = (l - (instances * width)) / 2;
                double[] pos = new double[2];
                pos[0] = o.GetEasting() + Mathf.Sin(overhang / l);
                pos[1] = o.GetNorthing() + Mathf.Cos(overhang / l);
                for (int j = 0; j < instances + 1; j++)
                {
                    GameObject instance = Instantiate(model.GetPrefab());
                    instance.transform.parent = holder.transform;
                    Vector3 worldPos = WorldToUnitySpaceConverter.GetInstance().UtmToUnitySpace(pos);
                    worldPos.y = ElevationRaycaster.GetInstance().GetElevation(worldPos);
                                 //+model.GetSizeY();
                    instance.transform.position = worldPos;
                    Vector3 rotation = instance.transform.rotation.eulerAngles;
                    rotation.y = alphaDeg;
                    instance.transform.rotation = WorldToUnitySpaceConverter.GetInstance().OffsetUnityRotation(alphaDeg);
                    pos[0] -= Mathf.Sin(alphaRad) * model.GetSizeX();
                    pos[1] -= Mathf.Cos(alphaRad) * model.GetSizeX();
                    switch (type)
                    {
                        case UmrandungsType.Hecke:
                            umrandungsModelListTypeHecke.Add(instance);
                            break;
                        case UmrandungsType.Zaun:
                            umrandungsModelListTypeZaun.Add(instance);
                            break;
                    }
                }
            }
            float minScaleFactor = model.GetMinHeight() / model.GetMaxHeight();
            float scalefactor = (1 - minScaleFactor) + minScaleFactor;
            Vector3 scaleVector = holder.transform.localScale;
            Vector3 positionVector = holder.transform.position;
            positionVector.y = scalefactor * model.GetSizeY();
            holder.transform.localScale = new Vector3(scaleVector.x, scalefactor, scaleVector.z);
            holder.transform.position = positionVector;
        }
    }

    public float UpdateUmrandungsModelHeight(float percentage)
    {
        float minScaleFactor = currentlyActiveUmrandung.GetMinHeight() / currentlyActiveUmrandung.GetMaxHeight();
        float scalefactor = (percentage / 100f) * (1 - minScaleFactor) + minScaleFactor;
        GameObject holder;
        switch (currentlyActiveUmrandung.GetUmrandungsType())
        {
            case UmrandungsType.Hecke:
                holder = umrandungsModelHeckeHolder;
                break;
            case UmrandungsType.Zaun:
                holder = umrandungsModelZaunHolder;
                break;
            default:
                holder = umrandungsModelZaunHolder;
                break;
        }
        Vector3 scaleVector = holder.transform.localScale;
        Vector3 positionVector = holder.transform.position;
        positionVector.y = scalefactor * currentlyActiveUmrandung.GetSizeY();
        holder.transform.localScale = new Vector3(scaleVector.x, scalefactor, scaleVector.z);
        holder.transform.position = positionVector;
        return scalefactor * currentlyActiveUmrandung.GetSizeY();
    }

    public void SwitchUmrandungsModel(UmrandungsType type)
    {
        currentlyActiveUmrandung = UmrandungsModelManager.GetInstance().GetUmrandungsModel(type);
        List<GameObject> activeUmrandungen;
        switch (currentlyActiveUmrandung.GetUmrandungsType())
        {
            case UmrandungsType.Hecke:
                activeUmrandungen = umrandungsModelListTypeHecke;
                break;
            case UmrandungsType.Zaun:
                activeUmrandungen = umrandungsModelListTypeZaun;
                break;
            default:
                activeUmrandungen = umrandungsModelListTypeZaun;
                break;
        }
        foreach (List<GameObject> modelList in umrandungsModelsListList)
        {
            if (modelList != activeUmrandungen)
            {
                foreach (GameObject umrandungsModel in modelList)
                {
                    umrandungsModel.SetActive(false);
                }
            }
            else
            {
                foreach (GameObject umrandungsModel in modelList)
                {
                    umrandungsModel.SetActive(true);
                }
            }
        }
    }

    public float SetRowDistance(float percentRowDistance)
    {
        float rowDistance = currentlyActiveModel.GetMinDistanceY() + percentRowDistance * (currentlyActiveModel.GetMaxDistanceY() - currentlyActiveModel.GetMinDistanceY());
        foreach (var row in rowList)
        {
            row.AdjustRowPosition(currentRotation, rowDistance, converter, map);
        }
        UpdateVisibility();
        currentRowDistance = percentRowDistance;
        return rowDistance;
    }

    public float SetModuleDistance(float percentModuleDistance)
    {
        foreach (var row in rowList)
        {
            row.AdjustModuleDistance(percentModuleDistance, converter, map);
            //row.AdjustRowPosition(currentRotation, moduleDistance, converter, map);
        }
        UpdateVisibility();
        currentModuleXDistance = percentModuleDistance;
        float scale = rowList[0].getMapRowGameObject().transform.localScale.x;
        float distance = currentlyActiveModel.GetMinDistanceX() * scale + currentlyActiveModel.GetSizeX() * scale -
                         currentlyActiveModel.GetSizeX();
        return distance;
    }

    public void RotateGrid(float rotation)
    {
        currentRotation = rotation;
        polyPvSymbolHolder.transform.rotation = Quaternion.Euler(0f, currentRotation, 0f);
        polyPvModelHolder.transform.rotation = Quaternion.Euler(0f, currentRotation, 0f);
        UpdateVisibility();
    }

    private void UpdateVisibility()
    {
        Dictionary<GameObject, GameObject> pvGoDict;
        switch (currentlyActiveModel.GetModelType())
        {
            case PvModelType.Default:
                pvGoDict = pvGoDictDefault;
                break;
            case PvModelType.Agri:
                pvGoDict = pvGoDictAgri;
                break;
            default:
                pvGoDict = pvGoDictDefault;
                break;
        }
        foreach (var goPair in pvGoDict)
        {
            Vector2d latLng = map.WorldToGeoPosition(goPair.Key.transform.position);
            Point point = new Point(latLng.x, latLng.y);
            Point[] points = new Point[4];
            float dx = currentlyActiveModel.GetSizeX();
            float dy = currentlyActiveModel.GetSizeY();
            points[0] = new Point(point.GetEasting() + dx, point.GetNorthing() + dy, 32);
            points[1] = new Point(point.GetEasting() + dx, point.GetNorthing() - dy, 32);
            points[2] = new Point(point.GetEasting() - dx, point.GetNorthing() + dy, 32);
            points[3] = new Point(point.GetEasting() - dx, point.GetNorthing() - dy, 32);
            bool inside = true;
            foreach (var pt in points)
            {
                if (!isPointInPolygon(pt))
                {
                    inside = false;
                    break;
                }
            }
            if (inside)
            {
                goPair.Key.SetActive(true);
                goPair.Value.SetActive(true);
            }
            else
            {
                goPair.Key.SetActive(false);
                goPair.Value.SetActive(false);
            }
        }
    }

    public void InstantiateAtUtmPos(double easting, double northing, Row currentRow)
    {
        LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
        LatLngUTMConverter.LatLng latLng =
            converter.convertUtmToLatLng(easting,
                northing, 33,
                "N");
        Vector3 p1 = map.GeoToWorldPosition(new Vector2d(latLng.Lat, latLng.Lng));
        GameObject pvMapSymbol = Instantiate(pvTilePrefab);

        Vector3 scalefactor = map.gameObject.transform.localScale;
        //the factor was empirically determined by placing points on the map and measuring x-distance in unity space...
        pvMapSymbol.transform.localScale =
            (new Vector3(0.2677851f * scalefactor.x, 0.2677851f * scalefactor.x,
                0.2677851f * scalefactor.x));
        pvMapSymbol.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        pvMapSymbol.transform.parent = currentRow.getMapRowGameObject().transform;
        pvMapSymbol.transform.position = p1;

        foreach(PvModelType type in Enum.GetValues(typeof(PvModelType)))
        {
            PvModel model = PvModelManager.GetInstance().GetPvModel(type);
            GameObject pvModelInstance = Instantiate(model.GetModelPrefab());
            pvModelInstance.gameObject.transform.parent = currentRow.getArRowGameObject().transform;
            Vector3 worldPos = WorldToUnitySpaceConverter.GetInstance()
                .UtmToUnitySpace(new double[] { easting, northing });
            float elevation = ElevationRaycaster.GetInstance().GetElevation(worldPos);
            worldPos.y = elevation;
            pvModelInstance.transform.position = worldPos;
            switch (model.GetModelType())
            {
                case PvModelType.Default:
                    pvModelsListTypeDefault.Add(pvModelInstance);
                    pvGoDictDefault.Add(pvMapSymbol, pvModelInstance);
                    break;
                case PvModelType.Agri:
                    pvModelsListTypeAgri.Add(pvModelInstance);
                    pvGoDictAgri.Add(pvMapSymbol, pvModelInstance);
                    break;
            } 
        }      
    }

    public void SwitchPvModels(PvModelType type)
    {
        currentlyActiveModel = PvModelManager.GetInstance().GetPvModel(type);
        List<GameObject> activePvModels;
        Dictionary<GameObject, GameObject> pvGoDict;
        switch (currentlyActiveModel.GetModelType())
        {
            case PvModelType.Default:
                activePvModels = pvModelsListTypeDefault;
                pvGoDict = pvGoDictDefault;
                break;
            case PvModelType.Agri:
                activePvModels = pvModelsListTypeAgri;
                pvGoDict = pvGoDictAgri;
                break;
            default:
                activePvModels = pvModelsListTypeDefault;
                pvGoDict = pvGoDictDefault;
                break;
        }
        foreach(List<GameObject> modelList in pvModelsListList)
        {
            if (modelList != activePvModels)
            {
                foreach (GameObject pvModel in modelList)
                {
                    pvModel.SetActive(false);
                }
            }
            else
            {
                foreach (GameObject pvModel in modelList)
                {
                    pvModel.SetActive(true);
                }
            }
        }
        Vector3 scale = new Vector3(currentlyActiveModel.GetSizeX(), currentlyActiveModel.GetSizeY(), 1f);
        foreach(GameObject go in pvGoDict.Keys)
        {
            go.transform.GetChild(0).transform.localScale = scale;
        }
        SetRowDistance(currentRowDistance);
        UpdateVisibility();
    }

    private bool isPointInPolygon(Point point)
    {
        Point2d<float> p = new Point2d<float>((float) point.GetEasting(), (float) point.GetNorthing());
        return PolygonUtils.PointInPolygon(p, polygon.GetOrderedPointsList());
    }

    private class Polygon
    {
        private Point[] orderedPoints;
        private List<List<Point2d<float>>> orderedPointsList;

        public Polygon(List<Point> points)
        {
            Point[] orderedPoints = new Point[points.Count];
            //find leftmost point and add to the result array
            Point leftmost = points[0];
            foreach (var point in points)
            {
                if (point.GetEasting() < leftmost.GetEasting()) leftmost = point;
            }

            orderedPoints[0] = leftmost;
            points.Remove(leftmost);

            //iterate through all remaining points
            int count = points.Count;
            for (int i = 0; i < count; i++)
            {
                Point next = points[0];
                double angle = Mathf.Atan2((float) (next.GetEasting() - orderedPoints[i].GetEasting()),
                    (float) (next.GetNorthing() - orderedPoints[i].GetNorthing()));
                foreach (var point in points)
                {
                    //find the point with the lowest value angle (around the previous point, clockwise, from +north axis)
                    double newAngle = Mathf.Atan2((float) (point.GetEasting() - orderedPoints[i].GetEasting()),
                        (float) ((point.GetNorthing() - orderedPoints[i].GetNorthing())));
                    if (newAngle < angle)
                    {
                        next = point;
                        angle = newAngle;
                    }
                }

                //remove that point and add it to the result array
                orderedPoints[i + 1] = next;
                points.Remove(next);
            }

            this.orderedPoints = orderedPoints;

            //create typed list for mapbox point-in-polygon-test
            orderedPointsList = new List<List<Point2d<float>>>();
            List<Point2d<float>> pts = new List<Point2d<float>>();
            foreach (var pt in this.orderedPoints)
            {
                pts.Add(new Point2d<float>((float) pt.GetEasting(), (float) pt.GetNorthing()));
            }

            orderedPointsList.Add(pts);
        }

        public Point[] GetOrderedPoints()
        {
            return orderedPoints;
        }

        public List<List<Point2d<float>>> GetOrderedPointsList()
        {
            return orderedPointsList;
        }
    }

    private class Point
    {
        private double easting;
        private double northing;
        private double lat;
        private double lng;
        private int utmZoneNumber;
        private string utmZoneLetter;

        public Point(double easting, double northing, int utmZoneNumber)
        {
            this.easting = easting;
            this.northing = northing;
            this.utmZoneNumber = utmZoneNumber;
            LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
            LatLngUTMConverter.LatLng latLng =
                converter.convertUtmToLatLng(easting, northing, utmZoneNumber, "N");
            this.lat = latLng.Lat;
            this.lng = latLng.Lng;
            this.utmZoneLetter = "N";
        }

        public Point(double lat, double lng)
        {
            this.lat = lat;
            this.lng = lng;
            LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
            LatLngUTMConverter.UTMResult utm =
                converter.convertLatLngToUtm(lat, lng);
            this.easting = (float) utm.Easting;
            this.northing = (float) utm.Northing;
            this.utmZoneNumber = utm.ZoneNumber;
            this.utmZoneLetter = utm.ZoneLetter;
        }

        public double GetEasting()
        {
            return easting;
        }

        public double GetNorthing()
        {
            return northing;
        }

        public double GetLat()
        {
            return lat;
        }

        public double GetLng()
        {
            return lng;
        }

        public int GetUtmZoneNumber()
        {
            return utmZoneNumber;
        }

        public string GetUtmZoneLetter()
        {
            return utmZoneLetter;
        }
    }

    public class Row
    {
        private int number;
        private double[] circleCenterUtmPos;
        private double[] currentRowUtmPos = new double[2];
        private int centerRowNumber;
        private GameObject rowMapGO;
        private GameObject rowArGO;
        private int sign;
        private List<GameObject> moduleList = new List<GameObject>();
        private float currentAngle;
        private float currentRowDistance;

        public Row(double[] circleCenterUtmPos, int number, int totalRowNumber, GameObject polyPvSymbolHolder,
            GameObject polyPvModelHolder)
        {
            //row in map space (map symbols)
            rowMapGO = new GameObject("map_row_" + number);
            rowMapGO.transform.parent = polyPvSymbolHolder.transform;
            //row in AR space (3d models)
            rowArGO = new GameObject("3d_row_" + number);
            rowArGO.transform.parent = polyPvModelHolder.transform;
            this.number = number;
            this.circleCenterUtmPos = circleCenterUtmPos;
            centerRowNumber = (totalRowNumber - 1) / 2;
            if (number < centerRowNumber) sign = 1;
            else if (number == centerRowNumber) sign = 0;
            else sign = -1;
        }

        public void AdjustRowPosition(float angle, float rowDistance, LatLngUTMConverter converter, AbstractMap map)
        {
            double distance = (Mathf.Abs(number - centerRowNumber) * sign * rowDistance);
            double northing = circleCenterUtmPos[1] + Mathf.Cos(Mathf.Deg2Rad * angle) * distance;
            double easting = circleCenterUtmPos[0] + Mathf.Sin(Mathf.Deg2Rad * angle) * distance;
            //move map row
            //double newNorthing = circleCenterUtmPos[1] + (Mathf.Abs(number - centerRowNumber) * sign * rowDistance);
            LatLngUTMConverter.LatLng latLng =
                converter.convertUtmToLatLng(easting,
                    northing, 33,
                    "N");
            Vector3 p1 = map.GeoToWorldPosition(new Vector2d(latLng.Lat, latLng.Lng));
            rowMapGO.transform.position = p1;
            //move AR row
            Vector3 worldPos = WorldToUnitySpaceConverter.GetInstance()
                .LatLngToUnitySpace(new Vector2d(latLng.Lat, latLng.Lng));
            rowArGO.transform.position = worldPos;
            currentRowUtmPos[0] = easting;
            currentRowUtmPos[1] = northing;
            currentAngle = angle;
            currentRowDistance = rowDistance;
        }

        public GameObject getMapRowGameObject()
        {
            return rowMapGO;
        }

        public GameObject getArRowGameObject()
        {
            return rowArGO;
        }

        public void AddModule(GameObject module)
        {
            moduleList.Add(module);
        }

        public void AdjustModuleDistance(float percentage, LatLngUTMConverter converter, AbstractMap map)
        {
            float maxscale = 1;
            float scaleFactor = 1 + (maxscale * percentage);
            rowMapGO.transform.localScale = new Vector3(scaleFactor, 1f, 1f);
            rowArGO.transform.localScale = new Vector3(scaleFactor, 1f, 1f);
            Vector3 mapChildScale = new Vector3(0.2677851f * (1/scaleFactor), 0.2677851f, 0.2677851f);
            Vector3 arChildScale = new Vector3(1 / scaleFactor, 1, 1);
            for (int i = 0; i < rowMapGO.transform.childCount; i++)
            {
                rowMapGO.transform.GetChild(i).localScale = mapChildScale;
            }

            for (int j = 0; j < rowArGO.transform.childCount; j++)
            {
                rowArGO.transform.GetChild(j).localScale = arChildScale;
            }
            /*int relativePos = rowMapGO.transform.childCount / 2;
            int startpos = -relativePos;
            int endpos = relativePos;
            if (rowMapGO.transform.childCount % 2 == 0) endpos -= 1;
            int j = 0;
            for (int i = startpos; i <= endpos; i++)
            {
                float deltaX = i * moduleDistance;
                double[] newUtmPos = new double[2] { currentRowUtmPos[0] + deltaX, currentRowUtmPos[1]};
                LatLngUTMConverter.LatLng latLng = converter.convertUtmToLatLng(newUtmPos[0], newUtmPos[1], 33, "N");
                Vector3 worldPos = map.GeoToWorldPosition(new Vector2d(latLng.Lat, latLng.Lng));
                rowMapGO.transform.GetChild(j).localPosition = worldPos - rowMapGO.transform.position;
                j++;
            }*/
        }
    }

    public class PvRowElement
    {
        private int sign;
        
        public PvRowElement(int number, int totalElementNumber)
        {
            
        }
    }

}
