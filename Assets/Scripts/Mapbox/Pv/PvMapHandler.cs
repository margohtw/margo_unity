using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PvMapHandler : MonoBehaviour
{
	[SerializeField] GameObject symbolHolder;
	[SerializeField] GameObject polyPrefab;

	bool polygonInitialized = false;
	PolygonController polyController;

	public void PlaceCorner(Vector3 position, float scale)
	{
		if (!polygonInitialized)
		{
			polygonInitialized = true;
			var polygon = Instantiate(polyPrefab, symbolHolder.transform, false);
			polyController = polygon.GetComponent<PolygonController>();
		}
		if (!polyController.IsFinished())
        {
			polyController.AddCorner(position, scale);
		}
        
	}

	
}
