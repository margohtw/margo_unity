using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PolygonCornerController : DraggableMapSymbol
{
    int cornerIndex;
    PolygonController polygon;

    void Start()
    {
        base.Start();
    }

    public int GetCornerIndex()
    {
        return cornerIndex;
    }

    public void SetCornerIndex(int index)
    {
        cornerIndex = index;
    }

    protected override void AdjustPosition()
    {
        
        base.AdjustPosition();
        if (polygon == null) polygon = GetComponentInParent<PolygonController>();
        polygon.AdjustPolygon(cornerIndex, deltaMove);
    }

    protected override void HandleClick()
    {
        if (cornerIndex == 0)
        {
            if (polygon == null) polygon = GetComponentInParent<PolygonController>();
            polygon.Finalize();
        }
    }
}
