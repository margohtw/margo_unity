using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaIconController : DraggableMapSymbol
{
    private IOnMapSymbolMovedListener mapSymbolMovedListener;

    void Start()
    {
        base.Start();
    }

    protected override void HandleClick()
    {
        //clicks shouldnt do anything for WEA symbols, currently!
        return;
    }

    protected override void AdjustPosition()
    {
        base.AdjustPosition();
        mapSymbolMovedListener.OnMapSymbolMoved(this.gameObject);
    }

    public void RegisterOnMovedListener(IOnMapSymbolMovedListener listener)
    {
        mapSymbolMovedListener = listener;
    }
}
