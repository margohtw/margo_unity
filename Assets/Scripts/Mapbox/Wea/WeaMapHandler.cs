using Mapbox.Unity.Map;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Constants = Mapbox.Unity.Constants;



public class WeaMapHandler : MonoBehaviour
{
    [SerializeField] GameObject weaPrefab;
    [SerializeField] GameObject symbolHolder;
    [SerializeField] private float iconScaleFactor;
    [SerializeField] private ArInteractionWeaModels arInteractionWea;

    private UserLocationProvider locationProvider;
    Vector3 iconScale;
    AbstractMap map;

    private List<WEAModel> placedWeaSymbols = new List<WEAModel>();

    void Start()
    {
        map = GetComponent<AbstractMap>();
        locationProvider = GetComponent<UserLocationProvider>();
        iconScale = new Vector3(iconScaleFactor, iconScaleFactor, iconScaleFactor);
    }

    // Adds the symbol and initantiates the 3d model
    public void PlaceWea(Vector3 unityWorldPositionMapSymbol, float scale)
    {
        //add symbol on map
        var mapSymbol = Instantiate(weaPrefab, symbolHolder.transform, false);
        mapSymbol.transform.localScale = mapSymbol.gameObject.transform.localScale * gameObject.transform.localScale.x;
        mapSymbol.transform.position = unityWorldPositionMapSymbol + DrawLayer.point;
        AddToPlacedModelsList(unityWorldPositionMapSymbol);
        mapSymbol.GetComponent<WeaIconController>().RegisterOnMovedListener(arInteractionWea);

        //add model in unity space // conversion of map symbol geo pos to unity space geo pos
        Vector2d latlng = map.WorldToGeoPosition(unityWorldPositionMapSymbol);
        Debug.Log("TestWEA: PlaceWea - latlng: " + latlng);
        Vector3 unityPosWeaModel = WorldToUnitySpaceConverter.GetInstance().LatLngToUnitySpace(latlng);
        //Vector3 unityPos = WorldToUnitySpaceConverter.GetInstance().UtmToUnitySpace(map.WorldToGeoPosition(unityWorldPosition).ToArray());
        Debug.Log("TestWEA: PlaceWea - unityPosWeaModel: " + unityPosWeaModel);
        arInteractionWea.InstantiateWea(unityPosWeaModel, mapSymbol);
    }

    public GameObject AddMapSymbol(Vector3 worldPosition)
    {
        Vector2d latLng = WorldToUnitySpaceConverter.GetInstance().UnitySpaceToLatLng(worldPosition);
        Vector3 position = map.GeoToWorldPosition(latLng);

        //add symbol on map
        var mapSymbol = Instantiate(weaPrefab, symbolHolder.transform, false);
        mapSymbol.transform.localScale = mapSymbol.gameObject.transform.localScale * gameObject.transform.localScale.x;
        mapSymbol.transform.position = position + DrawLayer.point;
        AddToPlacedModelsList(position);
        mapSymbol.GetComponent<WeaIconController>().RegisterOnMovedListener(arInteractionWea);
        return mapSymbol;
    }

    public void PlaceWeaLatLng(Vector2d latLngPos, float scale)
    {
        Vector3 worldPosition = map.GeoToWorldPosition(latLngPos);

        var spawnedObject = Instantiate(weaPrefab, symbolHolder.transform, false);
        spawnedObject.transform.localScale = spawnedObject.gameObject.transform.localScale * scale;
        spawnedObject.transform.position = worldPosition + DrawLayer.point;

        AddToPlacedModelsList(worldPosition);

    }

    public void ClearAllSpawnedWeaSymbols()
    {
        foreach (GameObject go in FindObjectsOfType(typeof(GameObject)))
        {
            if (go.transform.parent.name.Equals("MapSymbolHolder") && go.name.Contains("WeaMapIcon"))
            {
                go.Destroy();
            }
        }
    }

    private void AddToPlacedModelsList(Vector3 position)
    {
        // Add to userproject
        Vector2d latLng = map.WorldToGeoPosition(position);
        WEAModel model = new WEAModel();
        model.position = latLng;

        placedWeaSymbols.Add(model);
    }

    /*
     * We use this method, if a project (with already placed wea models) is loaded
     * Wea 3D Models will be placed and matching map symbols added
     */
    public void PlaceWeaModels(List<WEAModel> placedWeaModels)
    {
        foreach (var placedWeaModel in placedWeaModels)
        {

            Debug.Log("SaveDebug: WEA loaded, geopos map symbol - lat lng:" + placedWeaModel.position);

            Debug.Log("TestWEA: load wea: WEA Symbol " + placedWeaModel.position + " wird platziert ...");
            //Vector3 vec = new Vector3((float)placedWeaModel.position.x, (float)placedWeaModel.position.y, 0.1f);

            Vector3 unityWorldPosMapSymbol = map.GeoToWorldPosition(placedWeaModel.position);

            //Vector3 unityPos3 = WorldToUnitySpaceConverter.GetInstance().LatLngToUnitySpace(placedWeaModel.position);
            /*
            //Vector3 unityWorldPos1 = map.GeoToWorldPosition(new Vector2d((float)placedWeaModel.position.y, (float)placedWeaModel.position.y));
            Vector3 unityWorldPos2 = WorldToUnitySpaceConverter.GetInstance().LatLngToUnitySpace(placedWeaModel.position);
            //Vector3 unityPos3 = WorldToUnitySpaceConverter.GetInstance().LatLngToUnitySpace(map.WorldToGeoPosition(unityWorldPosition).ToArray());
            // Umwandlung falsch
            Debug.Log("SaveDebug: WEA loaded, worldpos1:" + unityWorldPos1);
            Debug.Log("SaveDebug: WEA loaded, worldpos2:" + unityWorldPos2);

            Debug.Log("SaveDebug: Test1:" + map.GeoToWorldPosition(new Vector2d(19.04821f, 52.52416f)));
            Debug.Log("SaveDebug: Test2:" + map.GeoToWorldPosition(new Vector2d(52.52416f, 19.04821f)));
            Debug.Log("SaveDebug: Test3:" + WorldToUnitySpaceConverter.GetInstance().LatLngToUnitySpace(new Vector2d(19.04821f, 52.52416f)));
            Debug.Log("SaveDebug: Test4:" + WorldToUnitySpaceConverter.GetInstance().LatLngToUnitySpace(new Vector2d(52.52416f, 19.04821f)));*/

            PlaceWea(unityWorldPosMapSymbol, 1 / map.gameObject.transform.localScale.x);
        }
    }

    // Before saving, we need to update the geoposition if user has moved the model via map symbol interaction
    // If done so, only the geo position of the wea models has to be changed.
    public void UpdateGeoPositionWeaSymbols()
    {
        /*var weaModels = arInteractionWea.GetAllWeaModels();
        Debug.Log("Gr??e platzierte MapSymbols: " + placedWeaSymbols.Count);
        Debug.Log("Gr??e platzierte 3D-Models: " + weaModels.Count);
        for (int i = 0; i < placedWeaSymbols.Count; i++)
        {
            //Vector2d newGeoPos = map.WorldToGeoPosition(weaModels[i].transform.position);
            Vector2d newGeoPosLatLng = WorldToUnitySpaceConverter.GetInstance().UnitySpaceToLatLng(weaModels[i].transform.position);
            Debug.Log("SaveDebug: Symbol moved; correct geo position to lat lng " + newGeoPosLatLng);
            placedWeaSymbols[i].position = newGeoPosLatLng;
        }*/
      var weaTupleList = arInteractionWea.GetWeaSymbolTupleList();
      Debug.Log("Gr??e platzierte MapSymbols: " + placedWeaSymbols.Count);
      Debug.Log("Gr??e platzierte Tuplelist: " + weaTupleList.Count);
      for (int i = 0; i < placedWeaSymbols.Count; i++)
      {
          Vector2d newGeoPosMapSymbol = map.WorldToGeoPosition(weaTupleList[i].Item1.transform.position);
          //Vector2d newGeoPosLatLng = WorldToUnitySpaceConverter.GetInstance().UnitySpaceToLatLng(weaTupleList[i].Item1.transform.position);
          Debug.Log("SaveDebug: Symbol moved; correct geo position to lat lng " + newGeoPosMapSymbol);
          placedWeaSymbols[i].position = newGeoPosMapSymbol;
      }
    }

    public Vector3 TestGetWorldPos(Vector2d geoPos)
    {
        return WorldToUnitySpaceConverter.GetInstance().LatLngToUnitySpace(geoPos);
    }

    public List<WEAModel> GetCurrentlyPlacedWeaModels()
    {
        UpdateGeoPositionWeaSymbols();
        return placedWeaSymbols;
    }

    public AbstractMap getMap()
    {
        return map;
    }

}
