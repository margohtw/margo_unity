using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IConstantScreenSize
{

    public void SetInitialScale(); //call once when initialized to account for previous scale changes!
    public void RescaleToMaintainScreenSize(float scaleFactor); //call whenever the scale needs to change (the map is zoomed)!
}
