using DG.Tweening;

namespace Mapbox.Examples
{
	using Mapbox.Unity.Location;
	using UnityEngine;

    public class RotateWithLocationProvider : MonoBehaviour
    {
        private Transform transform;
        private Vector3 initialScale;
        private Quaternion targetQuaternion;

        [SerializeField] int bufferSize;
        [SerializeField] private float maxRotSpeed;

        private float[] angleToNorthBuffer;
        private float totalWeight;
        private bool initialized;

        void Start()
        {
            if (!initialized) Init();
        }

        void Init()
        {
            initialScale = gameObject.transform.localScale;
            transform = GetComponent<Transform>();
            angleToNorthBuffer = new float[bufferSize];
            totalWeight = 0;
            for (int i = 0; i < (bufferSize); i++)
            {
                float weight = (1 / (float)(i + 1));
                totalWeight += weight;
            }

            initialized = true;
        }

        void Update()
        {
            if (!Input.gyro.enabled) Input.gyro.enabled = true;
            UpdateBuffer();
            transform.localRotation =
                Quaternion.RotateTowards(transform.localRotation, targetQuaternion, 360);
        }

        private void UpdateBuffer()
        {
            float avgRotation = 0;
            for (int i = 0; i < (bufferSize - 1); i++)
            {
                angleToNorthBuffer[i] = angleToNorthBuffer[i + 1];
                float weight = (1f / ((float)bufferSize - (float)i));
                avgRotation += angleToNorthBuffer[i] * weight;
            }
            angleToNorthBuffer[bufferSize - 1] = WorldToUnitySpaceConverter.GetInstance().GetAndroidAngleToNorth();
            avgRotation += angleToNorthBuffer[bufferSize - 1];
            avgRotation /= totalWeight;
            targetQuaternion = Quaternion.Euler(0f, avgRotation, 0f);
        }

    }
}
