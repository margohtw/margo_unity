using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Camera))]
public class MapRaycaster : MonoBehaviour
{
	//layer mask specifies a layer that should be ignored while raycasting
	//only positions on the maps surface are returned
	[SerializeField] LayerMask layerMask;
	[SerializeField] static Camera mapCam;

	static int layer;

	private void Awake()
    {
		mapCam = GetComponent<Camera>();
		//must get static int value from specified layer mask
		layer = layerMask;
    }

	public static bool MapCamRaycast(out Vector3 position)
	{
		position = Vector3.zero;

		RaycastHit hit;
		Ray ray = new Ray(mapCam.ScreenToWorldPoint(Input.mousePosition), Vector3.down);

		//ignores the specified layer with the "~" operator
		if (Physics.Raycast(ray, out hit, 100f, ~layer))
		{
			position = hit.point;
			return true;
		}
		else return false;
	}
}
