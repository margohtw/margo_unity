using System.Collections;
using System.Collections.Generic;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using UnityEngine;

public class UserLocationController : DraggableMapSymbol
{
    [SerializeField] private AbstractMap map;
    [SerializeField] private MainArScreenController arScreenController;
    
    private bool manualAdjustmentEnabled = false;

    public void SetManualAdjustment(bool manualAdjustmentEnabled)
    {
        this.manualAdjustmentEnabled = manualAdjustmentEnabled;
    }

    protected override void HandleClick()
    {
        //tapping shouldnt do anything, currently!
    }

    protected override void AdjustPosition()
    {
        if (manualAdjustmentEnabled)
        {
            base.AdjustPosition();
        }
    }

    protected override void PostPointerUp()
    {
        UserLocationProvider ulp = UserLocationProvider.GetInstance();
        Vector2d newLatLng = map.WorldToGeoPosition(transform.position);
        Vector2d oldLatLng = new Vector2d(ulp.GetCurrentLatitude(), ulp.GetCurrentLongitude());
        ulp.ManualUserLocationUpdate(newLatLng);
        WorldToUnitySpaceConverter.GetInstance().ManualUserLocationUpdate(oldLatLng, newLatLng);
        arScreenController.InitializeUserElevation();
    }
}
