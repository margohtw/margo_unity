using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//created to avoid issues with z-fighting
//could be replaced with https://www.youtube.com/watch?v=ZzcyREamMUo
//this was quicker...

public class DrawLayer : MonoBehaviour
{
    public static Vector3 poly = new Vector3(0, 0.1f, 0);
    public static Vector3 line = new Vector3(0, 0.2f, 0);
    public static Vector3 point = new Vector3(0, 0.3f, 0);
    public static Vector3 user = new Vector3(0, 0.4f, 0);
}
