using Mapbox.Unity.Map;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Draws calibration tiles for the mapbox map
 * Use the Method in AbstractMap called GeoToWorldPosition to convert Lat/Lng values to world position
 * Mapbox uses Vector2d as a class to handle Lat/Lng values
 * Use the TileController in each Tile prefab to draw the tile
*/

[RequireComponent(typeof(AbstractMap))]
public class CalibTileDrawer : MonoBehaviour
{
	[SerializeField] Vector2d startingLatLng;
	[SerializeField] GameObject tilePrefab;
	[SerializeField] GameObject symbolHolder;
	[SerializeField] int size;
	AbstractMap map;

	//CustomStart ensures the map is fully initialized when the CalibTileDrawer tries to access it
    public void CustomStart()
    {
		map = GetComponent<AbstractMap>();
	}

	//draws 100 tiles, starting with the center of the map as lat lng values
	public void InitializeTiles()
	{
		float deltaLat = 0.001f;
		float deltaLng = 0.001f;
		List<Vector2d> latLngList = new List<Vector2d>();
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				double x = startingLatLng.x + i * deltaLat;
				double y = startingLatLng.y + j * deltaLng;
				latLngList.Add(new Vector2d(x, y));
			}
		}

		for (int i = 0; i < size - 1; i++)
		{
			for (int j = 0; j < size - 1; j++)
			{
				GameObject tile = (GameObject)Instantiate(tilePrefab, symbolHolder.transform, true);
				TileController tc = tile.GetComponent<TileController>();
				Vector3 a = map.GeoToWorldPosition(latLngList[j + i * size], false);
				Vector3 b = map.GeoToWorldPosition(latLngList[j + 1 + i * size], false);
				Vector3 c = map.GeoToWorldPosition(latLngList[j + size + i * size], false);
				Vector3 d = map.GeoToWorldPosition(latLngList[j + size + 1 + i * size], false);
				tc.DrawTile(a, b, c, d);
			}
		}
	}
}
