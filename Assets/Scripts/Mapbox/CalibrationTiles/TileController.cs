﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/*
 * Handles drawing of calibration tiles on the map
 * Tile is drawn with the DrawTile method
 * --> Requires 4 Vector3 that are the 4 world positions of the tile corners
*/

public class TileController : MonoBehaviour
{
    [SerializeField] Material inactiveMat;
    [SerializeField] Material activeMat;
    [SerializeField] Material lineMat;
    [SerializeField] float lineWidth;

    private CalibTileWrapper tileWrapper;

    Mesh mesh;
    MeshCollider meshCollider;
    Renderer renderer;
    Renderer[] lineRenderers;
    bool active = false;

    //make sure to call the method so the vectors for the tile corners are like so:
    //  c --- d
    //  |     |
    //  |     |
    //  a --- b

    // with
    //    N
    //    |
    // W - - E
    //    |
    //    S

    // the points end up in the unity zx-plane ordered aligned to the compass like:
    //     z
    //     ^
    //     |
    // -x ---> x
    //     |
    //    -z


    public void DrawTile(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
    {
        Vector3 centerPosition = (a + b + c + d) / 4;

        renderer = GetComponent<MeshRenderer>();
        meshCollider = GetComponent<MeshCollider>();
        lineRenderers = new Renderer[4];
        lineRenderers[0] = gameObject.transform.GetChild(0).GetComponent<Renderer>();
        lineRenderers[1] = gameObject.transform.GetChild(1).GetComponent<Renderer>();
        lineRenderers[2] = gameObject.transform.GetChild(2).GetComponent<Renderer>();
        lineRenderers[3] = gameObject.transform.GetChild(3).GetComponent<Renderer>();

        var mf = GetComponent<MeshFilter>();

        mesh = new Mesh();
        mf.mesh = mesh;

        var vertices = new Vector3[4];
        vertices[0] = a + DrawLayer.poly;
        vertices[1] = b + DrawLayer.poly;
        vertices[2] = c + DrawLayer.poly;
        vertices[3] = d + DrawLayer.poly;

        mesh.vertices = vertices;

        var tris = new int[6];
        tris[0] = 0;
        tris[1] = 2;
        tris[2] = 1;

        tris[3] = 2;
        tris[4] = 3;
        tris[5] = 1;

        mesh.triangles = tris;

        var normals = new Vector3[4];

        normals[0] = -Vector3.forward;
        normals[1] = -Vector3.forward;
        normals[2] = -Vector3.forward;
        normals[3] = -Vector3.forward;

        mesh.normals = normals;

        var uv = new Vector2[4];

        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(1, 0);
        uv[2] = new Vector2(0, 1);
        uv[3] = new Vector2(1, 1);

        mesh.uv = uv;

        renderer.material = inactiveMat;
        meshCollider.sharedMesh = mesh;

        DrawLines(vertices);
    }

    private void DrawLines(Vector3[] vertices)
    {
        Renderer renderer = lineRenderers[0];
        GameObject line = renderer.gameObject;
        MeshFilter mf = line.GetComponent<MeshFilter>();
        Vector3 centerPosition;

        mesh = new Mesh();
        mf.mesh = mesh;

        Vector3[] verts = new Vector3[4];

        verts[0] = vertices[0];
        verts[0].x -= lineWidth;
        verts[0].z += lineWidth;
        verts[0].y = DrawLayer.line.y;

        verts[1] = vertices[1];
        verts[1].x += lineWidth;
        verts[1].z += lineWidth;
        verts[1].y = DrawLayer.line.y;

        verts[2] = vertices[1];
        verts[2].x += lineWidth;
        verts[2].z -= lineWidth;
        verts[2].y = DrawLayer.line.y;

        verts[3] = vertices[0];
        verts[3].x -= lineWidth;
        verts[3].z -= lineWidth;
        verts[3].y = DrawLayer.line.y;

        mesh.vertices = verts;

        var tris = new int[6];
        tris[0] = 0;
        tris[1] = 1;
        tris[2] = 2;

        tris[3] = 2;
        tris[4] = 3;
        tris[5] = 0;

        mesh.triangles = tris;

        var uv = new Vector2[4];

        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(1, 0);
        uv[2] = new Vector2(0, 1);
        uv[3] = new Vector2(1, 1);

        mesh.uv = uv;

        renderer.material = lineMat;

        //centerPosition = (verts[0] + verts[1] + verts[2] + verts[3]) / 4;
        //line.transform.position = centerPosition;

        // ---------------------------------------------------------------

        renderer = lineRenderers[1];
        line = renderer.gameObject;
        mf = line.GetComponent<MeshFilter>();

        mesh = new Mesh();
        mf.mesh = mesh;

        verts = new Vector3[4];

        verts[0] = vertices[1];
        verts[0].x -= lineWidth;
        verts[0].z -= lineWidth;
        verts[0].y = DrawLayer.line.y;

        verts[1] = vertices[3];
        verts[1].x -= lineWidth;
        verts[1].z += lineWidth;
        verts[1].y = DrawLayer.line.y;

        verts[2] = vertices[3];
        verts[2].x += lineWidth;
        verts[2].z += lineWidth;
        verts[2].y = DrawLayer.line.y;

        verts[3] = vertices[1];
        verts[3].x += lineWidth;
        verts[3].z -= lineWidth;
        verts[3].y = DrawLayer.line.y;

        mesh.vertices = verts;
        mesh.triangles = tris;
        mesh.uv = uv;
        renderer.material = lineMat;

        //centerPosition = (verts[0] + verts[1] + verts[2] + verts[3]) / 4;
        //line.transform.position = centerPosition;

        // ---------------------------------------------------------------

        renderer = lineRenderers[2];
        line = renderer.gameObject;
        mf = line.GetComponent<MeshFilter>();

        mesh = new Mesh();
        mf.mesh = mesh;

        verts = new Vector3[4];

        verts[0] = vertices[3];
        verts[0].x += lineWidth;
        verts[0].z -= lineWidth;
        verts[0].y = DrawLayer.line.y;

        verts[1] = vertices[2];
        verts[1].x -= lineWidth;
        verts[1].z -= lineWidth;
        verts[1].y = DrawLayer.line.y;

        verts[2] = vertices[2];
        verts[2].x -= lineWidth;
        verts[2].z += lineWidth;
        verts[2].y = DrawLayer.line.y;

        verts[3] = vertices[3];
        verts[3].x += lineWidth;
        verts[3].z += lineWidth;
        verts[3].y = DrawLayer.line.y;

        mesh.vertices = verts;
        mesh.triangles = tris;
        mesh.uv = uv;
        renderer.material = lineMat;

        //centerPosition = (verts[0] + verts[1] + verts[2] + verts[3]) / 4;
        //line.transform.position = centerPosition;

        // ---------------------------------------------------------------

        renderer = lineRenderers[3];
        line = renderer.gameObject;
        mf = line.GetComponent<MeshFilter>();

        mesh = new Mesh();
        mf.mesh = mesh;

        verts = new Vector3[4];

        verts[0] = vertices[2];
        verts[0].x += lineWidth;
        verts[0].z += lineWidth;
        verts[0].y = DrawLayer.line.y;

        verts[1] = vertices[0];
        verts[1].x += lineWidth;
        verts[1].z -= lineWidth;
        verts[1].y = DrawLayer.line.y;

        verts[2] = vertices[0];
        verts[2].x -= lineWidth;
        verts[2].z -= lineWidth;
        verts[2].y = DrawLayer.line.y;

        verts[3] = vertices[2];
        verts[3].x -= lineWidth;
        verts[3].z += lineWidth;
        verts[3].y = DrawLayer.line.y;

        mesh.vertices = verts;
        mesh.triangles = tris;
        mesh.uv = uv;
        renderer.material = lineMat;

        //centerPosition = (verts[0] + verts[1] + verts[2] + verts[3]) / 4;
        //line.transform.position = centerPosition;
    }

    //sorts vectors by world position, returns an array with the leftmost one at position 0 and the rightmost one at position 1
    //if both have the same x value, the vector with lower z value is first
    private Vector3[] SortTwoVectors(Vector3 a, Vector3 b)
    {
        Vector3[] vecs = new Vector3[2];
        if (a.x < b.x)
        {
            vecs[0] = a;
            vecs[1] = b;
        }
        else if (a.x > b.x)
        {
            vecs[0] = b;
            vecs[1] = a;
        }
        else
        {
            if (a.z < b.z)
            {
                vecs[0] = a;
                vecs[1] = b;
            }
            else
            {
                vecs[0] = b;
                vecs[1] = a;
            }
        }
        return vecs;
    }
       

    public void SwapColor()
    {
        if (!active) renderer.material = inactiveMat;
        else renderer.material = activeMat;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("OnPointerClick");
        active = !active;
        SwapColor();
        tileWrapper.SetSelected(active);
        
    }

    public void SetEnabled(bool enabled)
    {
        Debug.Log("SetEnabled: " + enabled);
        active = enabled;
        SwapColor();
        tileWrapper.SetSelected(enabled);
        
    }

    public void ToggleEnabled()
    {
        SetEnabled(!active);
    }

    public void SetWrapper(CalibTileWrapper tileWrapper)
    {
        this.tileWrapper = tileWrapper;
    }
}
