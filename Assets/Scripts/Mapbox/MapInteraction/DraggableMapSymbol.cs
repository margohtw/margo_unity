using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/*
 * Base abstract class for any symbol displayed on the map that should be draggable
 * Override HandleClick method to implement clicking function
 * 
 * If touch is short (less than timeThreshold), a click is handled
 * Otherwise, the movement of the touch gesture is detected 
 * and used to drag the object in the AdjustPosition method
*/

public abstract class DraggableMapSymbol : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IConstantScreenSize
{
    [SerializeField] float moveThreshold;
    [SerializeField] float timeThreshold;
    [SerializeField] Vector3 initialScale;
    bool touchRegistered = false;
    bool dragInitialized = false;
    Vector3 prevPos;
    Vector3 currPos;
    float clickDuration = 0;
    protected Vector3 deltaMove = Vector3.zero;

    protected void Start()
    {
        SetInitialScale();
    }

    public void SetInitialScale()
    {
        gameObject.transform.localScale = initialScale * (1 / gameObject.transform.parent.transform.lossyScale.x);
    }

    public void RescaleToMaintainScreenSize(float scaleFactor)
    {
        gameObject.transform.localScale = initialScale * scaleFactor;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        touchRegistered = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (clickDuration < timeThreshold)
        {
            HandleClick();
        }
        touchRegistered = false;
        prevPos = Vector3.zero;
        currPos = Vector3.zero;
        deltaMove = Vector3.zero;
        dragInitialized = false;
        clickDuration = 0;
        PostPointerUp();
    }

    private void Update()
    {
        if (touchRegistered)
        {
            clickDuration += Time.deltaTime;
            AdjustPosition();
        }
    }

    protected virtual void PostPointerUp() {}

    protected virtual void AdjustPosition()
    {
        Vector3 newPos = Vector3.zero;
        MapRaycaster.MapCamRaycast(out newPos);
        if (!dragInitialized)
        {
            currPos = newPos;
            dragInitialized = true;
        }
        else
        {
            prevPos = currPos;
            currPos = newPos;
            deltaMove = currPos - prevPos;
            if ((deltaMove).magnitude > moveThreshold)
            {
                Vector3 transformPos = gameObject.transform.position;
                transformPos += deltaMove;
                transformPos.y = DrawLayer.point.y;
                gameObject.transform.position = transformPos;
            }
        }
    }
    protected abstract void HandleClick();


}
