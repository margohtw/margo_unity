﻿using UnityEngine;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class MapInteraction : MonoBehaviour
{
    [SerializeField] Camera mapCam;
    [SerializeField] AbstractMap map;

    [SerializeField] float panSpeed;
    [SerializeField] float threshold;
    [SerializeField] float tapTimeThreshold;
    [SerializeField] float zoomSpeed;
    [SerializeField] float maxZoomLevel = 16;
    [SerializeField] float minZoomLevel = 6;
    [SerializeField] int initZoomLevel;
    [SerializeField] Vector2d initLatLngCenter;

    [SerializeField] GameObject userLocationMarker;
    [SerializeField] GameObject symbolHolder;
    [SerializeField] GameObject weaSymbolHolder;
    [SerializeField] GameObject pvSymbolHolder;
    [SerializeField] GameObject calibTileSymbolHolder;
    [SerializeField] CalibTileScreenController calibTileScreen;
    [SerializeField] MainArUIManager mainArUiManager;

    private CalibTileHandlerFacade calibTileManager;
    private WeaMapHandler weaHandler;
    private PvMapHandler pvHandler;
    private UserLocationProvider userLocationProvider;
    private MapViewSizeController mapSizeController;

    private int invertedMapLayerMask;
    private bool isPointerOverUnityUIChecked;
    private bool zoomInitialized;
    private bool mapInitializedWithUserLocation;
    private bool areCalibTilesAlreadyRequested=false;
    private bool isPointerOverUnityUI;

    private float touchDuration;
    private Vector2 lastTouchPosition;
    private int touchLength;
    private Vector2 touchPos0;
    private Vector2 touchPos1;

    //variables for testing
    [SerializeField] float mouseScrollScale;

    //position where touch started (finger down on screen) as world position on the surface of the map
    private Vector3 touchDownPos;
    //position where touch ended (finger up from screen) as world position on the surface of the map
    private Vector3 mouseUpPos;

    //variables to store data when map is being dragged with one finger; stores positions that are on the surface of the map in world space
    private bool initialDragPosRegistered;
    private Vector3 previousDragPos = new Vector3();
    private Vector3 currentDragPos = new Vector3();

    //arrays to store data when map is being zoomed via pinches
    //screen pixel positions of current frames touches
    private Vector2[] pinchPositions = new Vector2[2];
    //screen pixel positions of previous frames touches
    private Vector2[] previousPinchPositions = new Vector2[2];

    //used for dpi-independent zooming
    private float screenSizeFactor;

    private void Awake()
    {
        screenSizeFactor = 1f / ((Screen.width + Screen.height) / 2f);
        weaHandler = GetComponent<WeaMapHandler>();
        pvHandler = GetComponent<PvMapHandler>();
        calibTileManager = GetComponent<CalibTileHandlerFacade>();
        userLocationProvider = GetComponent<UserLocationProvider>();
        mapSizeController = GetComponent<MapViewSizeController>();
    }

    private void Start()
    {
        map.Initialize(initLatLngCenter, initZoomLevel);
    }

    private void Update()
    {
        InitMapWithCurrentUserLocationIfNeeded();

        if (!mapSizeController.IsMapOpen()) return;
        
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                touchLength = Input.touches.Length;
                switch (touchLength)
                {
                    case 0:
                        touchPos0 = Vector2.zero;
                        touchPos1 = Vector2.zero;
                        break;
                    case 1:
                        touchPos0 = Input.touches[0].position;
                        touchPos1 = Vector2.zero;
                        break;
                    case 2:
                        touchPos0 = Input.touches[0].position;
                        touchPos1 = Input.touches[1].position;
                        break;
                }
                break;
            default:
                if (Input.GetMouseButton(0))
                {
                    touchLength = 1;
                    touchPos0 = Input.mousePosition;
                }
                else
                {
                    touchLength = 0;
                    touchPos0 = Vector2.zero;
                    touchPos1 = Vector2.zero;
                }
                break;
        }

        if ((touchLength > 0) && mapSizeController.IsMapOpen())
        {
            //this checks if the map is hit (click is in map area)
            if (PerformMapCamRaycast(touchPos0, out touchDownPos, out GameObject hitObject))
            {
                //this checks if either the native unity ui or a map symbol was clicked
                if (!isPointerOverUnityUIChecked)
                {
                    CheckIfPointerOverUnityUI(touchPos0);
                }
                //if the touch is over the map and isnt on ui or map symbols, handle the touch!
                if (!isPointerOverUnityUI)
                {
                    //quick fix for stupid UI elements
                    if (CanvasManager.GetInstance().GetActiveCanvasType() == CanvasType.ChangePVModelScreen
                        && touchPos0.y < (Screen.height / 2))
                    {
                        //do nothing
                    }
                    else
                    {
                        touchDuration += Time.deltaTime;
                        lastTouchPosition = touchPos0;
                        HandleTouchInput();
                    }
                }
            }
        }
        else if(touchLength == 0)
        {
            if (lastTouchPosition != Vector2.zero && !isPointerOverUnityUI && touchDuration > 0.0f && touchDuration < tapTimeThreshold && !MainArUIManager.IsMouseOverUI)
            {
                HandleTap(lastTouchPosition);
            }
        }

        if (touchLength == 0)
        {
            //Debug.Log("Touch: Length==0 V2");
            ResetAllInputs();
        }
        Test_ImplementZoomFunctionForUnityEditor();
    }

    //-----------------------------------------------------------------------------------------------------------------------------
    // Interaction Methods
    //-----------------------------------------------------------------------------------------------------------------------------

    private void HandleTouchInput()
    {
        // Zooming and panning the map is handled here
        switch (touchLength)
        {
            case 1:
                HandleSingleTouchInput();
                break;
            case 2:
                HandleDoubleTouchInput();
                break;
        }
    }

    private void Test_ImplementZoomFunctionForUnityEditor()
    {
        if (Input.mouseScrollDelta.y != 0)
        {
            float desiredZoomLevel = map.Zoom + Input.mouseScrollDelta.y * mouseScrollScale;
            if (desiredZoomLevel < minZoomLevel) desiredZoomLevel = minZoomLevel;
            if (desiredZoomLevel > maxZoomLevel) desiredZoomLevel = maxZoomLevel;
            map.UpdateMap(map.CenterLatitudeLongitude, desiredZoomLevel);
            AdjustSymbols();
        }
    }

    private void HandleTap(Vector2 screenPos)
    {
        if (mainArUiManager.IsInGpsInitMode()) return;
        Debug.Log("Touch: HandleTap");
        if (PerformMapCamRaycast(screenPos, out mouseUpPos, out GameObject hitObject))
        {
            //this checks if either the native unity ui or a map symbol was clicked (does NOT check if pointer is over toolkit UI elements!)
            if (!isPointerOverUnityUIChecked)
            {
                CheckIfPointerOverUnityUI(screenPos);
            }

            //handle touch only if not over unity UI element (native unity UI click events are handled separately!)
            if (!isPointerOverUnityUI)
            {
                switch (AppModeManager.GetInstance().GetAppMode())
                {
                    case AppMode.calibscreen:
                        TileController tileController = hitObject.GetComponent<TileController>();
                        bool check = calibTileScreen.IsPointerOverUI(screenPos);
                        //finally check if pointer is over toolkit UI element (register tile click only if no UI button is touched!)
                        if (tileController != null && !check)
                        {
                            tileController.ToggleEnabled();
                        }
                        break;
                    case AppMode.pv:
                        pvHandler.PlaceCorner(mouseUpPos, 1 / gameObject.transform.localScale.x);
                        break;
                    case AppMode.wea:
                        weaHandler.PlaceWea(mouseUpPos, 1 / gameObject.transform.localScale.x);
                        break;
                }
            }
        }

    }

    private void HandleSingleTouchInput()
    {
        if (zoomInitialized)
        {
            //dont pan if zooming has already happened
            return;
        }

        //save positions of touch
        previousDragPos = currentDragPos;
        currentDragPos = touchDownPos;
        //only pan once two positions with sufficient distance between the two have been registered
        float magnitude = (currentDragPos - previousDragPos).magnitude;
        if (initialDragPosRegistered && magnitude > threshold)
        {
            PanMap();
        }

        initialDragPosRegistered = true;
    }

    private void HandleDoubleTouchInput()
    {
        //store the pixel positions of the previous two touches
        previousPinchPositions[0] = pinchPositions[0];
        previousPinchPositions[1] = pinchPositions[1];
        //store the pixel positions of the current two touches
        pinchPositions[0] = touchPos0;
        pinchPositions[1] = touchPos1;
        //only drag once two sets of positions have been registered
        if (zoomInitialized)
        {
            //calculate the length of the vector between the two points
            //roughly normalize the deltaX and deltaY depending on screen pixel size (screensizefactor)
            //calculate the difference in previous and current vector length and execute zoom
            float magCurrent = (pinchPositions[0] - pinchPositions[1]).magnitude * screenSizeFactor;
            float magPrev = (previousPinchPositions[0] - previousPinchPositions[1]).magnitude * screenSizeFactor;
            PinchZoom(magCurrent - magPrev);
        }
        else zoomInitialized = true;
    }

    //-----------------------------------------------------------------------------------------------------------------------------
    // Utility Methods
    //-----------------------------------------------------------------------------------------------------------------------------

    private void CheckIfPointerOverUnityUI(Vector2 pos)
    {
        PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = pos;

        List<RaycastResult> results = new List<RaycastResult>();
        
        EventSystem.current.RaycastAll(pointerEventData, results);
        foreach (RaycastResult result in results)
        {
            //Debug.Log("Pointer: CheckShouldBlockInteraction: " + result.gameObject.name + " " + result.GetType() + " " + result.ToString());
            //layer 5 is ui
            //layer 6 is map symbols 
            if (result.gameObject.layer == 5 || result.gameObject.layer == 6)
            {
                isPointerOverUnityUI = true;
            }
        }

        if ((MainArUIManager.IsMouseOverUI)) isPointerOverUnityUI = true;
        /*VisualElement navbar = CanvasManager.GetInstance().GetGameObjectOfActiveCanvas().GetComponent<UIDocument>().rootVisualElement.Q<VisualElement>("BottomNavBar");
        foreach (var child in navbar.Children())
        {
            if (child.ContainsPoint(Input.mousePosition)) isPointerOverUnityUI = true;
        }*/

        isPointerOverUnityUIChecked = true;
    }

    private bool PerformMapCamRaycast(Vector2 pixelCoords, out Vector3 position, out GameObject hitObject)
    {
        position = Vector3.zero;
        hitObject = null;

        RaycastHit hit;
        Ray ray = new Ray(mapCam.ScreenToWorldPoint(pixelCoords), Vector3.down);

        if (Physics.Raycast(ray, out hit, 20000f))
        {
            hitObject = hit.transform.gameObject;
            position = hit.point;
            return true;
        }
        return false;
    }

    public void AdjustSymbols()
    {
        if (mapSizeController.IsMapFullscreen()) symbolHolder.transform.localScale = map.transform.localScale;
        else symbolHolder.transform.localScale = map.transform.localScale;

        symbolHolder.transform.position = map.GeoToWorldPosition(initLatLngCenter, false);
        float invertedScale = 1 / map.transform.localScale.x;
        foreach (var screenResizer in symbolHolder.GetComponentsInChildren<IConstantScreenSize>())
        {
            screenResizer.RescaleToMaintainScreenSize(invertedScale);
        }
    }

    public void ResetAllInputs()
    {
        isPointerOverUnityUIChecked = false;
        isPointerOverUnityUI = false;
        currentDragPos = new Vector3();
        previousDragPos = new Vector3();
        pinchPositions = new Vector2[2];
        previousPinchPositions = new Vector2[2];
        touchDownPos = new Vector3();
        zoomInitialized = false;
        initialDragPosRegistered = false;
        lastTouchPosition = Vector2.zero;
        touchDuration = 0.0f;
    }

    public void ChangeMapMode(AppMode mode)
    {
        switch (mode)
        {
            case AppMode.wea:
                weaSymbolHolder.SetActive(true);
                pvSymbolHolder.SetActive(false);
                calibTileSymbolHolder.SetActive(false);
                break;
            case AppMode.pv:
                weaSymbolHolder.SetActive(false);
                pvSymbolHolder.SetActive(true);
                calibTileSymbolHolder.SetActive(false);
                break;
            case AppMode.calibscreen:
                weaSymbolHolder.SetActive(false);
                pvSymbolHolder.SetActive(false);
                calibTileSymbolHolder.SetActive(true);
                break;
        }
    }

    private void InitMapWithCurrentUserLocationIfNeeded()
    {
        if (userLocationProvider.IsInitialUpdateComplete() && !mapInitializedWithUserLocation && !areCalibTilesAlreadyRequested)
        {
            areCalibTilesAlreadyRequested = true;
            Debug.Log("Map wird mit der aktuellen UserLocation initialisiert...");
            initLatLngCenter = new Vector2d(userLocationProvider.GetCurrentLatitude(),
                userLocationProvider.GetCurrentLongitude());
            RecenterMapOnUserLocation();
            calibTileManager.Initialize();
            mapInitializedWithUserLocation = true;
        }
    }

    public void RecenterMapOnUserLocation()
    {
        Vector2d latLng = new Vector2d(userLocationProvider.GetCurrentLatitude(),
            userLocationProvider.GetCurrentLongitude());
        map.UpdateMap(latLng, initZoomLevel);
        AdjustSymbols();
    }

    private void PinchZoom(float deltaMagnitude)
    {
        //zoom levels in mapbox range between 0 and 21
        float desiredZoomLevel = map.Zoom + deltaMagnitude * zoomSpeed;
        if (desiredZoomLevel < minZoomLevel) desiredZoomLevel = minZoomLevel;
        if (desiredZoomLevel > maxZoomLevel) desiredZoomLevel = maxZoomLevel;
        map.UpdateMap(map.CenterLatitudeLongitude, desiredZoomLevel);
        AdjustSymbols();
    }

    private void PanMap()
    {
        Vector3 deltaMove = previousDragPos - currentDragPos;
        Vector3 origin = map.GeoToWorldPosition(map.CenterLatitudeLongitude, false);
        Vector2d latLng = map.WorldToGeoPosition(origin + deltaMove);
        map.UpdateMap(latLng, map.Zoom);
        AdjustSymbols();
    }

    public void SetVisibilityUserLocationMarker(bool isVisible)
    {
        userLocationMarker.SetActive(isVisible);
    }
}