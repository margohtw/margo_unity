using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class ProjectDataSaver
{
    public static bool SaveJsonData(ISaveable saveable)
    {
        SaveData saveData = new SaveData();

        // populate the data to be saved
        saveable.PopulateSaveData(saveData);

        string filename = RemoveInvalidChars(saveData.userProject.title);

        if (!("".Equals(filename)) && FileManager.WriteToFile(filename, saveData.ToJson()))
        {
            Debug.Log("Save successful");
            return true;
        }else
        {
            return false;
        }
    }

    public static void LoadJsonData(ISaveable saveable)
    {
        if (FileManager.LoadFromFile("SaveData01.dat", out var json))
        {
            SaveData saveData = new SaveData();
            saveData.LoadFromJson(json);

            // do action after loading
            saveable.LoadFromSaveData(saveData);

            Debug.Log("Load complete");
        }
        else
        {
            Debug.Log("Fehler!!");
        }
    }

    public static List<UserProject> getAllSavedProjects()
    {
        return FileManager.getAllSavedProjects();
    }

    private static string RemoveInvalidChars(string filename)
    {
        return string.Concat(filename.Split(Path.GetInvalidFileNameChars()));
    }

}