using System.Collections.Generic;
using UnityEngine;

/*
 * This class is used to save and load persisted user projects.
 */
[System.Serializable]
public class SaveData
{
    public UserProject userProject = new UserProject();

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadFromJson(string json)
    {
        JsonUtility.FromJsonOverwrite(json, this);
    }

}

public interface ISaveable
{
    void PopulateSaveData(SaveData projectData);
    void LoadFromSaveData(SaveData projectData);
}