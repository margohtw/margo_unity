using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;
using System.Threading.Tasks;

public static class FileManager
{
    public static bool WriteToFile(string a_FileName, string a_FileContents)
    {
        var fullPath = Path.Combine(Application.persistentDataPath, a_FileName);
        Debug.Log("FileManager: �berpr�fe, ob Datei existiert: " + fullPath);
        if (File.Exists(fullPath + ".margo"))
        {
            Debug.Log("Datei " + fullPath + " existiert bereits.");
            return false;
        }

        try
        {
            File.WriteAllText(fullPath + ".margo", a_FileContents);
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to write to {fullPath} with exception {e}");
            return false;
        }
    }


    public static bool LoadFromFile(string a_FileName, out string result)
    {
        var fullPath = Path.Combine(Application.persistentDataPath, a_FileName);

        try
        {
            result = File.ReadAllText(fullPath);
            return true;
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to read from {fullPath} with exception {e}");
            result = "";
            return false;
        }
    }

    public static List<UserProject> getAllSavedProjects()
    {
        List<UserProject> allUserProjects = new List<UserProject>();
        string[] projectFilePaths = System.IO.Directory.GetFiles(Application.persistentDataPath, "*.margo");
        Debug.Log("Es wurden " + projectFilePaths.Length + " Projektfiles gefunden.");
        foreach (var projectFilePath in projectFilePaths)
        {
            Debug.Log("Gefundene Projektfile: " + projectFilePath);

            LoadFromFile(projectFilePath, out var json);

            SaveData saveData = new SaveData();
            saveData.LoadFromJson(json);

            allUserProjects.Add(saveData.userProject);

        }
        return allUserProjects;
    }

}