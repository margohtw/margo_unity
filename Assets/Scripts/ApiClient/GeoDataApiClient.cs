using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/*
 * This class handles the communication with the GeoDataAPI
 * - GET-Request to retrieve the available geo-calibration tiles within a fixed radius of the current location
*/
public class GeoDataApiClient : MonoBehaviour
{

    // Values are for testing; adjust here when api is ready
    private readonly string API_BASE_URL = "http://drive.google.com/";
    private readonly string API_GET_CALIB_TILES_ENDPOINT = "uc?export=download&id=1RJItn6mD4R9BZgmI8fJpWKqBmQi7wv2f";

    public void RequestCalibTiles(Action<List<CalibTile>> responseCallback)
    {
        StartCoroutine(DoCalibTilesHTTPRequest(responseCallback));
    }

    IEnumerator DoCalibTilesHTTPRequest(System.Action<List<CalibTile>> responseCallback)
    {
        UnityWebRequest www = UnityWebRequest.Get(API_BASE_URL + API_GET_CALIB_TILES_ENDPOINT);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("HTTP-Request successfull! URL: " + API_BASE_URL + API_GET_CALIB_TILES_ENDPOINT);
            string httpResponse = www.downloadHandler.text;
            List<CalibTile> calibTilesList = ProcessHttpResponseData(httpResponse);
            responseCallback(calibTilesList);
        }
    }

    private List<CalibTile> ProcessHttpResponseData(string httpResponse)
    {
        List<CalibTile> calibTileList = new List<CalibTile>();
        CalibTile[] listCalibTiles = JsonUtilityHelper.FromJson<CalibTile>(httpResponse);
        foreach (var calibTile in listCalibTiles)
        {
            calibTileList.Add(calibTile);
        }
        return calibTileList;
    }
}
