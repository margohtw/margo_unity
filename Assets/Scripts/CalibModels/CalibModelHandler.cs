using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Siccity.GLTFUtility;
using UnityEngine;
using UnityEngine.Serialization;

public class CalibModelHandler : MonoBehaviour
{
    private static CalibModelHandler instance;

    [SerializeField] private MainArUIManager arUiManager;
    
    [SerializeField] private GameObject calibModelHolder;
    [SerializeField] private GameObject cityModelHolder;
    [SerializeField] private GameObject dgmModelHolder;
    [SerializeField] private GameObject domModelHolder;
    [SerializeField] private GameObject domRgbModelHolder;
    [SerializeField] private GameObject cityRgbModelHolder;
    
    [SerializeField] private Material invisibleOcclusionMat; 
    [SerializeField] private Material transparentTexMat;
    [SerializeField] private List<Material> materials;

    [SerializeField] private HomeWEAScreenController homeWEAScreenController;

    [SerializeField] private CalibTileHandlerFacade calibTilehandlerFacade;
    
    private List<MeshRenderer> dgmRendererList = new List<MeshRenderer>();
    private List<MeshRenderer> domRendererList = new List<MeshRenderer>();
    private List<MeshRenderer> cityModelRendererList = new List<MeshRenderer>();
    private List<MeshRenderer> domRgbRendererList = new List<MeshRenderer>();
    private List<MeshRenderer> cityModelRgbRendererList = new List<MeshRenderer>();

    private List<CalibTileWrapper> loadedTiles = new List<CalibTileWrapper>();

    private Dictionary<MeshRenderer, Texture> rgbTexRendererDict;
    private bool rgbTexStored;

    private bool tilesLoaded;
    

    private bool isLoadingTile;
    private GameObject loadedTile;


    private void Start()
    {
        if (instance == null) instance = this;
        if (instance != null == instance != this) this.Destroy();
    }

    public static CalibModelHandler GetInstance()
    {
        return instance;
    }


    public void MakeCalibModelsInvisible()
    {
        cityModelHolder.SetActive(false);
        domModelHolder.SetActive(false);
        dgmModelHolder.SetActive(false);
        domRgbModelHolder.SetActive(false);
        cityRgbModelHolder.SetActive(false);
    }

    
    public void ChangeCalibModelVisibility(TileType tileType, bool visibility)
    {
        switch (tileType)
        {
            case TileType.cityModel:
                cityModelHolder.SetActive(visibility);
                break;
            case TileType.dom:
                domModelHolder.SetActive(visibility);
                break;
            case TileType.dgm:
                dgmModelHolder.SetActive(visibility);
                break;
            case TileType.dom_rgb:
                domRgbModelHolder.SetActive(visibility);
                break;
            case TileType.cityModel_rgb:
                cityRgbModelHolder.SetActive(visibility);
                break;
        }
    }
    

    public void AdjustRgbModelTransparency(float transparency, TileType tileType)
    {
        switch (tileType)
        {
            case TileType.dom_rgb:
                foreach (var renderer in domRgbRendererList)
                {
                    renderer.material.SetFloat("_transparency", transparency);
                }
                break;
            case TileType.cityModel_rgb:
                foreach (var renderer in cityModelRendererList)
                {
                    renderer.material.SetFloat("_transparency", transparency);
                }
                break;
        }
    }
    
    public void InitializeTypeVisibility()
    {
        //homeWEAScreenController.InitController();
        StoreRgbTextures();
        SetCalibModelsVisible();
        SetHolderObjectStatus();
    }

    private void StoreRgbTextures()
    {
        if (!rgbTexStored)
        {
            rgbTexRendererDict = new Dictionary<MeshRenderer, Texture>();
            foreach (var renderer in GetRenderersByTileType(TileType.cityModel_rgb))
            {
                rgbTexRendererDict.Add(renderer, renderer.material.mainTexture);
            }
            foreach (var renderer in GetRenderersByTileType(TileType.dom_rgb))
            {
                rgbTexRendererDict.Add(renderer, renderer.material.mainTexture);
            }
        }
        rgbTexStored = true;
    }

    private void SetHolderObjectStatus()
    {
        cityModelHolder.SetActive(IsCalibTileTypeSelected(TileType.cityModel));
        dgmModelHolder.SetActive(IsCalibTileTypeSelected(TileType.dgm));
        domModelHolder.SetActive(IsCalibTileTypeSelected(TileType.dom));
        cityRgbModelHolder.SetActive(IsCalibTileTypeSelected(TileType.cityModel_rgb));
        domRgbModelHolder.SetActive(IsCalibTileTypeSelected(TileType.dom_rgb));
    }

    private bool IsCalibTileTypeSelected(TileType type)
    {
        return homeWEAScreenController.IsCalibTypeSelected(type);
    }
    
    public void EndCalibration()
    {

        WorldToUnitySpaceConverter.GetInstance().Calibrate();
        SetHolderObjectStatus();
        SetCalibModelsInvisible();
    }
    
    private void SetCalibModelsInvisible()
    {
        TileType[] types = {TileType.dgm, TileType.dom, TileType.cityModel, TileType.dom_rgb, TileType.cityModel_rgb};
        foreach (var tileType in types)
        {
            if (IsCalibTileTypeSelected(tileType))
            {
                foreach (var renderer in GetRenderersByTileType(tileType))
                {
                    renderer.material = invisibleOcclusionMat;
                }
            }
        }
    }
    
    private void SetCalibModelsVisible()
    {
        cityModelHolder.SetActive(true);
        dgmModelHolder.SetActive(true);
        domModelHolder.SetActive(true);
        domRgbModelHolder.SetActive(true);
        cityRgbModelHolder.SetActive(true);
        TileType[] types = {TileType.dgm, TileType.dom, TileType.cityModel};
        foreach (var tileType in types)
        {
            foreach (var renderer in GetRenderersByTileType(tileType))
            {
                renderer.material = materials[homeWEAScreenController.GetDataRow(tileType).selectedShaderPos];
            }
        }
        foreach (var renderer in GetRenderersByTileType(TileType.cityModel_rgb))
        {
            renderer.material = transparentTexMat;
            rgbTexRendererDict.TryGetValue(renderer, out Texture texture);
            renderer.material.mainTexture = texture;
        }
        foreach (var renderer in GetRenderersByTileType(TileType.dom_rgb))
        {
            renderer.material = transparentTexMat;
            rgbTexRendererDict.TryGetValue(renderer, out Texture texture);
            renderer.material.mainTexture = texture;
        }
    }
    
    public void SetInitialCalibTileRotation()
    {
        calibModelHolder.transform.rotation = Quaternion.Euler(0f, - WorldToUnitySpaceConverter.GetInstance().GetUnityToWorldYOffset(), 0f);
    }

    public void EnableDgmForElevation()
    {
        dgmModelHolder.SetActive(true);
    }

    public void SetShader(int listPosition, TileType type)
    {
        foreach (var renderer in GetRenderersByTileType(type))
        {
            renderer.material = materials[listPosition];
        }
    }

    private List<MeshRenderer> GetRenderersByTileType(TileType tileType)
    {
        switch (tileType)
        {
            case TileType.dom:
                return domRendererList;
            case TileType.dgm:
                return dgmRendererList;
            case TileType.cityModel:
                return cityModelRendererList;
            case TileType.dom_rgb:
                return domRgbRendererList;
            case TileType.cityModel_rgb:
                return cityModelRgbRendererList;
            default:
                return null;
        }
    }

    public void BeginLoadingTiles()
    {
        if (ShouldLoadTiles(out List<CalibTileWrapper> tilesToLoad))
        {
            StartCoroutine(LoadSelectedCalibTiles(tilesToLoad));
        }
        else
        {
            arUiManager.SwitchUiMode(MainArUIManager.ArUiMode.Calibration);
        }
    }

    private bool ShouldLoadTiles(out List<CalibTileWrapper> tilesToLoad)
    {
        tilesToLoad = new List<CalibTileWrapper>();
        List<CalibTileWrapper> selectedCalibTileList = calibTilehandlerFacade.GetSelectedTiles();
        foreach (var tileWrapper in selectedCalibTileList)
        {
            if (!loadedTiles.Contains(tileWrapper)) tilesToLoad.Add(tileWrapper);
        }

        if (tilesToLoad.Count != 0) return true; 
        return false;
    }

    public IEnumerator LoadSelectedCalibTiles(List<CalibTileWrapper> tilesToLoad)
    {
        ModelImporter modelImporter = ModelImporter.GetInstance(); 
        float[] utmPos = loadCurrentUserPositionUTM();

        int modelsToLoad = tilesToLoad.Count;
        int modelsLoaded = 0;

        if (tilesToLoad.Count != 0)
        {
           foreach (var calibTileWrapper in tilesToLoad)
           {
                GameObject loadedTile = modelImporter.ImportModel(calibTileWrapper.GetFilename(),
                calibTileWrapper.GetCalibTileType().ToString());

                MeshRenderer renderer = loadedTile.GetComponent<MeshRenderer>();
        
                if (calibTileWrapper.GetCalibTileType() == TileType.dom_rgb)
                {
                    Texture texture = renderer.material.mainTexture;
                    renderer.material = transparentTexMat;
                    renderer.material.mainTexture = texture;
                }

                loadedTile.name = calibTileWrapper.GetFilename();

                //determine correct utm pos to convert (offset + SW coords)
                double easting = -(utmPos[0] + calibTileWrapper.GetOffsetX());
                double northing = -(utmPos[1] + calibTileWrapper.GetOffsetY());

                //set the objects layer
                loadedTile.layer = LayerMask.NameToLayer("CalibModels");

                //add mesh collider for raycasting
                if (calibTileWrapper.GetCalibTileType() == TileType.dgm) loadedTile.AddComponent<MeshCollider>();

                //save reference to the meshrenderer for switching materials later on
                //add to proper gameobject parent for model management
                switch (calibTileWrapper.GetCalibTileType())
                {
                    case TileType.cityModel:
                        cityModelRendererList.Add(loadedTile.GetComponent<MeshRenderer>());
                        loadedTile.transform.parent = cityModelHolder.transform;
                        break;
                    case TileType.dgm:
                        dgmRendererList.Add(loadedTile.GetComponent<MeshRenderer>());
                        loadedTile.transform.parent = dgmModelHolder.transform;
                        break;
                    case TileType.dom:
                        domRendererList.Add(loadedTile.GetComponent<MeshRenderer>());
                        loadedTile.transform.parent = domModelHolder.transform;
                        break;
                    case TileType.dom_rgb:
                        domRgbRendererList.Add(loadedTile.GetComponent<MeshRenderer>());
                        loadedTile.transform.parent = domRgbModelHolder.transform;
                        break;
                    case TileType.cityModel_rgb:
                        cityModelRgbRendererList.Add(loadedTile.GetComponent<MeshRenderer>());
                        loadedTile.transform.parent = cityRgbModelHolder.transform;
                        break;
                }
                
                //rotate to account for the models standard orientation
                loadedTile.transform.localRotation = Quaternion.Euler(-90, -180, 0);

                //set the models position in unity space
                loadedTile.transform.localPosition = new Vector3((float) easting, 0, (float) northing);
                
                modelsLoaded++;
                arUiManager.SetLoadingBarProgress((float)modelsLoaded/(float)modelsToLoad);
                arUiManager.SetLoadingBarProgress((float)modelsLoaded/(float)modelsToLoad);
                loadedTile = null;
                yield return null; 
           }
        }
        loadedTiles = tilesToLoad;
        arUiManager.SwitchUiMode(MainArUIManager.ArUiMode.Calibration);
        tilesLoaded = true;
    }

    private void OnFinished(GameObject arg1, AnimationClip[] arg2)
    {
        isLoadingTile = false;
        loadedTile = arg1;
    }

    public bool AreTilesLoaded()
    {
        return tilesLoaded;
    }
    
    private float[] loadCurrentUserPositionUTM()
    {
        UserLocationProvider locationProvider = UserLocationProvider.GetInstance();
        
        LatLngUTMConverter converter = new LatLngUTMConverter("ETRS89");
        LatLngUTMConverter.UTMResult utmResult = converter.convertLatLngToUtm((double)locationProvider.GetCurrentLatitude(), (double)locationProvider.GetCurrentLongitude());

        Debug.Log("UTM Result: " + utmResult.Easting + " " + utmResult.Northing + " " + utmResult.ZoneNumber);
        return new[] { (float)utmResult.Easting, (float)utmResult.Northing };
    }

    public void RecieveLoadedModels(List<GameObject> loadedModels)
    {
        
    }

    public void SetMatDistance(float value)
    {
        // set the distance in the appropriate material
        //calibModelMaterial.SetFloat("Vector1_d4adbae12b854f45aa247967a05331fa", value.newValue);
    }
}
