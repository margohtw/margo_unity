using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UmrandungsType
{
    Zaun,
    Hecke
}

public class UmrandungsModelManager : MonoBehaviour
{
    private static UmrandungsModelManager instance;

    [SerializeField] List<UmrandungsType> types;
    [SerializeField] List<GameObject> prefabs;
    [SerializeField] List<float> sizesX;
    [SerializeField] List<float> sizesY;
    [SerializeField] List<float> minHeights;
    [SerializeField] List<float> maxHeights;
    [SerializeField] List<Texture2D> modelImages;

    private Dictionary<UmrandungsType, UmrandungsModel> pvModelDict;

    public static UmrandungsModelManager GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance != null && instance != this) Destroy(this.gameObject);
        else instance = this;
        pvModelDict = new Dictionary<UmrandungsType, UmrandungsModel>();
        foreach (UmrandungsType type in types)
        {
            int pos = types.IndexOf(type);
            UmrandungsModel newModel = new UmrandungsModel(types[pos], sizesX[pos], sizesY[pos], prefabs[pos], modelImages[pos], minHeights[pos], maxHeights[pos]);
            pvModelDict.Add(type, newModel);
        }
    }

    public UmrandungsModel GetUmrandungsModel(UmrandungsType type)
    {
        UmrandungsModel model;
        pvModelDict.TryGetValue(type, out model);
        return model;
    }
}
