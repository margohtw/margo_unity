#ifndef SOBELOUTLINES_INCLUDED
#define SOBELOUTLINES_INCLUDED
static float2 sobelSamplePoints[9] = {
	float2(-1, 1), float2(0, 1), float2(1, 1),
	float2(-1, 0), float2(0, 0), float2(1, 1),
	float2(-1, -1), float2(0, -1), float2(1, -1)
};

static SamplerState MeshTextureSampler
{
	Filter = SAMPLER_POINT_REPEAT;
	AddressU = Wrap;
	AddressV = Wrap;
};

static float sx[9] = {
	-1, 0, 1,
	-1, 0, 1,
	-1, 0, 1
};

static float sy[9] = {
	-1, -1, 1,
	0, 0, 0,
	1, 0, 1
};

void Sobel_float(float2 UV, Texture2D renderedtex, SamplerState SS, float Thickness, float Threshold, float4 OutlineColor, out float4 Color, out float Alpha) {
	//float2 adjustedUV = UV + sobelSamplePoints[0] * Thickness;
	float2 result = 0;
	//Color = OutlineColor;
	//Alpha = 0.5f;

	[unroll]
	for (int i = 0; i < 9; i++) {
		//float depth = SHADERGRAPH_SAMPLE_SCENE_DEPTH(UV + sobelSamplePoints[i] * Thickness);
		float4 color = SAMPLE_TEXTURE2D(renderedtex, SS, UV + sobelSamplePoints[i] * Thickness);
		//sobel += depth * float2(sobelXMatrix[i], sobelYMatrix[i]);
		float grey = (color.r + color.g + color.b) / 3.0f;
		result[0] += sx[i] * grey;
		result[1] += sy[i] * grey;
	}
	float4 outputcolor = SAMPLE_TEXTURE2D(renderedtex, SS, UV);
	if (result[0] > Threshold || result[1] > Threshold) {
		Color = OutlineColor;
		Alpha = 1.0f;
	}
	else {
		Color = outputcolor;
		if (outputcolor.a == 0.0f) Alpha = 0.0f;
		else Alpha = 0.5f;
	}
}
#endif