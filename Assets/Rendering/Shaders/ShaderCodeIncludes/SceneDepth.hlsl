#ifndef CUSTOMSCENEDEPTH_INCLUDED
#define CUSTOMSCENEDEPTH_INCLUDED
void CustomSceneDepth_float(float4 UV, out float Out) 
{
	Out = Unity_SceneDepth_Raw_float(UV, Out);
}
#endif