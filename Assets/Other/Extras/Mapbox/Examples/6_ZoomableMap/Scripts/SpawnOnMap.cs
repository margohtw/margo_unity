﻿namespace Mapbox.Examples
{
	using UnityEngine;
	using Mapbox.Utils;
	using Mapbox.Unity.Map;
	using System.Collections.Generic;
    using UnityEngine.EventSystems;
    using Mapbox.Unity.Utilities;

    public class SpawnOnMap : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
	{
		[SerializeField]
		Camera mapCam;

		[SerializeField]
		Vector3 iconScale;

		[SerializeField]
		AbstractMap map;

		[SerializeField]
		GameObject _markerPrefab;

		[SerializeField]
		float panSpeed;

		[SerializeField]
		float threshold;

		[SerializeField]
		float zoomSpeed;

		[SerializeField]
		GameObject symbolHolder;

		//lists used for updating the icons positions in <AdjustSymbols()>
		List<GameObject> spawnedObjects = new List<GameObject>();
		List<Vector2d> locations = new List<Vector2d>();

		//position where touch started (finger down on screen) as world position on the surface of the map
		Vector3 touchDownPos;
		//position where touch ended (finger up from screen) as world position on the surface of the map
		Vector3 mouseUpPos;

		//array to store data when map is being dragged with one finger
		//[0] is previous frame, [1] is current frame
		//stores positions that are on the surface of the map in world space
		//Vector3[] dragPositions = new Vector3[2];
		Vector3 previousDragPos = new Vector3();
		Vector3 currentDragPos = new Vector3();

		//arrays to store data when map is being zoomed via pinches
		//screen pixel positions of current frames touches
		Vector2[] pinchPositions = new Vector2[2];
		//screen pixel positions of previous frames touches
		Vector2[] previousPinchPositions = new Vector2[2];

		//bools to determine drag/zoom state
		bool dragInitialized = false;
		bool zoomInitialized = false;
		bool touchRegistered = false;

		//int to keep track of how many touches are currently registered
		private int currentTouchCount = 0;

		private void Update()
		{
			if (touchRegistered)
			{
				if (MapRaycast(out touchDownPos))
				{
					switch (Input.touches.Length)
					{
						case 1:
							HandleSingleInput();
							break;
						case 2:
							HandleDoubleInput();
							break;

					}
				}
			}
		}

		//-----------------------------------------------------------------------------------------------------------------------------
		// Event System Interface Methods
		//-----------------------------------------------------------------------------------------------------------------------------
		public void OnPointerDown(PointerEventData eventData)
		{
			touchRegistered = true;
			currentTouchCount++;
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			currentTouchCount--;
			if (currentTouchCount == 0)
			{
				if (MapRaycast(out mouseUpPos) && !(dragInitialized || zoomInitialized))
				{
					placeWea();
				}
				ResetAllInputs();
			}
		}

		//-----------------------------------------------------------------------------------------------------------------------------
		// Single Input
		//-----------------------------------------------------------------------------------------------------------------------------

		private void HandleSingleInput()
		{
			previousDragPos = currentDragPos;
			currentDragPos = touchDownPos;

			//only drag once two positions with sufficient distance between the two have been registered
			float magnitude = (currentDragPos - previousDragPos).magnitude;
			if (previousDragPos != new Vector3() && magnitude > threshold)
			{
				PanMap();
				dragInitialized = true;
			}
		}

		private void placeWea()
		{
			var spawnedObject = Instantiate(_markerPrefab, symbolHolder.transform, true);
			Vector3 spawnPos = mouseUpPos;
			Vector2d latLng = map.WorldToGeoPosition(spawnPos);
			//increase the symbols height by 1 unit to avoid clipping
			spawnPos.y += 1;
			spawnedObject.transform.position = spawnPos;
			spawnedObject.transform.localScale = iconScale;
			spawnedObjects.Add(spawnedObject);
			locations.Add(latLng);
		}

		//-----------------------------------------------------------------------------------------------------------------------------
		// Double Input
		//-----------------------------------------------------------------------------------------------------------------------------

		private void HandleDoubleInput()
		{
			//store the pixel positions of the previous two touches
			previousPinchPositions[0] = pinchPositions[0];
			previousPinchPositions[1] = pinchPositions[1];
			//store the pixel positions of the current two touches
			pinchPositions[0] = Input.touches[0].position;
			pinchPositions[1] = Input.touches[1].position;
			//only drag once two sets of positions have been registered
			if (zoomInitialized)
			{
				//normalize the deltaX and deltaY depending on screen pixel size (result between 0 and 1)
				//calculate the length of the vector between the two points
				//calculate the difference in previous and current vector length

				float normalizedDeltaX = (pinchPositions[0].x - pinchPositions[1].x) / Screen.width;
				float normalizedDeltaY = (pinchPositions[0].y - pinchPositions[1].y) / Screen.height;

				float magnitudeCurrent = Mathf.Sqrt(normalizedDeltaX * normalizedDeltaX + normalizedDeltaY * normalizedDeltaY);

				normalizedDeltaX = (previousPinchPositions[0].x - previousPinchPositions[1].x) / Screen.width;
				normalizedDeltaY = (previousPinchPositions[0].y - previousPinchPositions[1].y) / Screen.height;

				float magnitudePrev = Mathf.Sqrt(normalizedDeltaX * normalizedDeltaX + normalizedDeltaY * normalizedDeltaY);

				PinchZoom(magnitudeCurrent - magnitudePrev);
			}
			else zoomInitialized = true;
		}

		private void PinchZoom(float deltaMagnitude)
		{
			float desiredZoomLevel = map.Zoom + deltaMagnitude * zoomSpeed;
			if (desiredZoomLevel < 0f) desiredZoomLevel = 0f;
			if (desiredZoomLevel > 21f) desiredZoomLevel = 21f;
			map.UpdateMap(map.CenterLatitudeLongitude, desiredZoomLevel);
			AdjustSymbols();
		}


		private void PanMap()
        {
			Vector3 deltaMove = previousDragPos - currentDragPos;
			Vector3 origin = map.GeoToWorldPosition(map.CenterLatitudeLongitude);
			Vector2d latLng = map.WorldToGeoPosition(origin + deltaMove);
			map.UpdateMap(latLng, map.Zoom);
			AdjustSymbols();
		}

		//-----------------------------------------------------------------------------------------------------------------------------
		// Utility Methods
		//-----------------------------------------------------------------------------------------------------------------------------

		private bool MapRaycast(out Vector3 position)
		{
			position = new Vector3(0, 0, 0);
			RaycastHit hit;
			Ray ray = new Ray(mapCam.ScreenToWorldPoint(Input.mousePosition), Vector3.down);
			if (Physics.Raycast(ray, out hit))
			{
				string name = hit.collider.gameObject.name;
				position = hit.point;
				Debug.DrawRay(mapCam.ScreenToWorldPoint(Input.mousePosition), Vector3.down);
				return true;
			}
			else return false;
		}


		private void AdjustSymbols()
        {
			int count = spawnedObjects.Count;
			for (int i = 0; i < count; i++)
			{
				var spawnedObject = spawnedObjects[i];
				var location = locations[i];
				Vector3 geoToWorldPosition = map.GeoToWorldPosition(location, true);
				geoToWorldPosition.y += 1;
				spawnedObject.transform.position = geoToWorldPosition;
				spawnedObject.transform.localScale = iconScale;
			}
		}

		private void ResetAllInputs()
		{
			currentDragPos = new Vector3();
			previousDragPos = new Vector3();
			pinchPositions = new Vector2[2];
			previousPinchPositions = new Vector2[2];
			touchDownPos = new Vector3();
			touchRegistered = false;
			dragInitialized = false;
			zoomInitialized = false;
		}
	}
}