using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BtnReCenterInitGpsController : MonoBehaviour, IPointerClickHandler
{

    [SerializeField] private MainArUIManager mainArUiManager;

    public void OnPointerClick(PointerEventData eventData)
    {
        mainArUiManager.RecenterMapOnInitGpsPosition();
    }
}
